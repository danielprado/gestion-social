<?php

namespace IDRDApp\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('citizen_attention_exist', 'IDRDApp\Rules\CustomRules@validateCitizenAttentionSchedule');
        Validator::extend('is_require_which', 'IDRDApp\Rules\CustomRules@validateIsRequireWhichSummons');
        Validator::extend('is_require_process', 'IDRDApp\Rules\CustomRules@validateIsRequireProcess');
        Validator::extend('schedule_programming', 'IDRDApp\Rules\CustomRules@validateProgrammingSchedules');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
