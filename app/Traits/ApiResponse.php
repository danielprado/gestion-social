<?php


namespace IDRDApp\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Schema;

trait ApiResponse
{

    /**
     * The model of the table to filter.
     *
     * @var string
     */
    protected $model;

    /**
     * The transformer of the model.
     *
     * @var string
     */
    protected $transformer;


    /**
     * The column of the table to filter.
     *
     * @var string
     */
    protected $column;

    /**
     * The order of the data must have returned.
     *
     * @var string
     */
    protected $order;

    /**
     * The quantity of resources shown per query.
     *
     * @var string
     */
    protected $per_page;

    /**
     * The value to filter in the table.
     *
     * @var string
     */
    protected $query;


    public function __construct()
    {
        $this->column   =  \request()->has( 'column' ) ? \request()->get('column')[0] : 'id';
        $this->order    =  \request()->has( 'order' ) ? (boolean) \request()->get('order')[0] : false;
        $this->per_page =  \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $this->query    =  \request()->has( 'query' ) ? \request()->get('query') : null;
        $this->order    =  ( $this->order ) ? 'asc' : 'desc';
    }

    /**
     * Return formatted data for query search
     *
     * @param Model $model
     * @param $transformer
     * @param bool $paginated
     * @return LengthAwarePaginator
     */
    public function table(Model $model, $transformer, $paginated = true )
    {
        $data = $model::query();

        if ( $this->modelHasColumn( $this->column, $model->getFillable() ) ) {
            $data = $data->where( $this->column, 'LIKE', "%{$this->query}%");
        } else {
            $this->column = $model->getKeyName();
            $data = $data->where('id', 'LIKE', "%{$this->query}%");
        }

        $data = $data->when( $this->query, function ($query) use($model) {
            return $query->orWhere(function ( $query ) use($model) {
                foreach ( $model->getFillable() as $column ) {
                    $query->orWhere( $column, 'LIKE', '%' . $this->query . '%' );
                }
            });
        });

        return  $paginated ? $this->paginateCollection($data, $transformer ) : $data;
    }

    /**
     * Get paginated collection
     *
     * @param $data
     * @param $transformer
     * @return LengthAwarePaginator
     */
    protected function paginateCollection ($data, $transformer ) {

        $data = $data->orderBy( $this->column, $this->order )->paginate( $this->per_page );

        $resource = $data->getCollection()
            ->map(function($model) use ( $transformer ) {
                return ( new $transformer() )->transform( $model );
            })->toArray();

        return new LengthAwarePaginator(
            $resource,
            (int) $data->total(),
            (int) $data->perPage(),
            (int) $data->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $data->currentPage()
            ]
        ]);
    }

    /**
     * @param $column
     * @param array $columns
     * @return mixed
     */
    protected function modelHasColumn($column, array $columns = [] )
    {
        return in_array( $column, $columns );
    }


    /**
     * Return formatted errors
     *
     * @param $message
     * @param int $code
     * @param null $details
     * @return \Illuminate\Http\JsonResponse
     */
    protected function error_response($message, $code = 422, $details = null )
    {
        return response()->json([
            'message' =>  $message,
            'details' => $details,
            'code'  =>  $code
        ], $code);
    }
}