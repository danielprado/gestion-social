<?php


namespace IDRDApp\Entities\Schedule;


use Illuminate\Database\Eloquent\Model;

class ProgrammingCommitmentTracing extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programmings_commitments_tracings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commitment_id',
        'date',
        'description'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'commitment_id'      =>  'int',
        'date'               =>  'date',
    ];

    /*
   * ---------------------------------------------------------
   * Accessors and Mutator Attributes
   * ---------------------------------------------------------
   */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = toUpper($value);
    }
}