<?php


namespace IDRDApp\Entities\Schedule;


use Illuminate\Database\Eloquent\Model;

class ProgrammingCommitment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programmings_commitments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'programming_id',
        'responsable',
        'date',
        'description'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'programming_id'      =>  'int',
        'date'      =>  'date',
    ];

    /*
    * ---------------------------------------------------------
    * Accessors and Mutator Attributes
    * ---------------------------------------------------------
    */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setResponsableAttribute($value)
    {
        $this->attributes['responsable'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = toUpper($value);
    }

    /*
    * ---------------------------------------------------------
    * Eloquent Relationships
    * ---------------------------------------------------------
    */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tracings()
    {
        return $this->hasMany( ProgrammingCommitmentTracing::class, 'commitment_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function programing()
    {
        return $this->belongsTo( ProgrammingView::class, 'programming_id', 'id' );
    }
}