<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;

class ProgrammingActivities extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programmings_activities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activity_id',
        'programming_id',
        'description'
    ];
}
