<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;

class CommunityFile extends Model
{
    const FILE_PATH = '/storage/community_files';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'community_files';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'community_id',
        'file_name',
        'path'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'community_id'  =>  'int',
        'file_name'     =>  'string',
        'path'          =>  'string',
    ];


    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * @param $path
     * @return string
     */
    public function getPathAttribute( $path )
    {
        return file_exists( public_path(self::FILE_PATH.'/'.$path ) )
            ? asset( 'public'.self::FILE_PATH.'/'.$path )
            : null;
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function citizens()
    {
        return $this->belongsTo( CommunityAttention::class, 'community_id' ,'id' );
    }
}
