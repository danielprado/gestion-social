<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;

class ProgrammingThemes extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programmings_themes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'theme_id',
        'programming_id',
        'other'
    ];
}
