<?php

namespace IDRDApp\Entities\Schedule;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CitizenAttentionView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "citizen_attention_view";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'execution_date',
        'citizen_attention_schedule_id',
        'citizen_attention_schedule',
        'user_id',
        'who_cancel',
        'reason_for_cancellation',
        'created_at',
        'updated_at',
        'deleted_at',
        'full_name',
        'name',
        'middle_name',
        'last_name',
        'second_last_name',
        'document',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'execution_date'    =>  'date',
        'deleted_at'        =>  'date',
        'citizen_attention_schedule_id' =>  'int',
        'user_id' =>  'int',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['day_month', 'day_of_week', 'part_name'];


    public function getPartNameAttribute()
    {
        return isset( $this->name, $this->last_name ) ? toUpper( "{$this->name} {$this->last_name}" ) : null;
    }

    public function getDayMonthAttribute()
    {
        return isset( $this->execution_date ) ? Carbon::parse( $this->execution_date )->format('m-d') : null;
    }


    public function getDayOfWeekAttribute()
    {
        return isset( $this->execution_date ) ? Carbon::parse( $this->execution_date )->dayOfWeek: null;
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reprogramming()
    {
        return $this->hasMany(CitizenAttentionReprogramming::class, 'citizen_attention_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany( CitizenFile::class,  'citizen_id', 'id');
    }
}
