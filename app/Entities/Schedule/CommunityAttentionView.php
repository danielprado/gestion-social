<?php

namespace IDRDApp\Entities\Schedule;

use Carbon\Carbon;
use IDRDApp\Entities\Security\Profile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommunityAttentionView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'community_attentions_view';


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'execution_date'    =>  'date',
        'user_id'           =>  'int',
    ];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['day_month', 'day_of_week', 'part_name'];


    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */
    public function getPartNameAttribute()
    {
        return isset( $this->name, $this->last_name ) ? toUpper( "{$this->name} {$this->last_name}" ) : null;
    }

    public function getDayMonthAttribute()
    {
        return isset( $this->execution_date ) ? Carbon::parse( $this->execution_date )->format('m-d') : null;
    }


    public function getDayOfWeekAttribute()
    {
        return isset( $this->execution_date ) ? Carbon::parse( $this->execution_date )->dayOfWeek: null;
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany( CommunityFile::class,  'community_id', 'id');
    }
}
