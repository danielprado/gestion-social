<?php

namespace IDRDApp\Entities\Schedule;

use IDRDApp\Entities\Parks\Park;
use IDRDApp\Entities\Security\Profile;
use Illuminate\Database\Eloquent\Model;

class FollowUpOnCommitment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'follow_up_on_commitments';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'park_id',
        'date',
        'responsable',
        'description',
        'user_id',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'park_id'   =>  'int',
        'user_id'   =>  'int',
        'date'      =>  'date',
    ];

    /*
    * ---------------------------------------------------------
    * Accessors and Mutator Attributes
    * ---------------------------------------------------------
    */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setResponsableAttribute($value)
    {
        $this->attributes['responsable'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = toUpper($value);
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function professional()
    {
        return $this->belongsTo( Profile::class, 'user_id', 'Id_Persona' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function park()
    {
        return $this->belongsTo( Park::class, 'park_id', 'Id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tracings()
    {
        return $this->hasMany(FollowUpOnCommitmentTracing::class, 'follow_commitment_id', 'id');
    }
}
