<?php

namespace IDRDApp\Entities\Schedule;

use IDRDApp\Entities\Security\Profile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Programming extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programmings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'execution_date',
        'initial_hour',
        'final_hour',
        'reunion_type_id',
        'park',
        'upz',
        'upz_id',
        'place',
        'process_id',
        'park_id',
        'who_summons_id',
        'which',
        'request_type_id',
        'objective',
        'who_cancel',
        'reason_for_cancellation',
        'include_attendance',
        'include_files',
        'include_images',
        'include_commitments',
        'user_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'execution_date'    =>  'date',
        'reunion_type_id'   =>  'int',
        'upz_id'            =>  'int',
        'process_id'        =>  'int',
        'park_id'           =>  'int',
        'who_summons_id'    =>  'int',
        'user_id'           =>  'int',
        'request_type_id'   =>  'int',
        'include_attendance'=>  'bool',
        'include_files'     =>  'bool',
        'include_images'    =>  'bool',
        'include_commitments'=>  'bool',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setWhoCancelAttribute($value)
    {
        $this->attributes['who_cancel'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setReasonForCancellationAttribute($value)
    {
        $this->attributes['reason_for_cancellation'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setObjectiveAttribute($value)
    {
        $this->attributes['objective'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setWhichAttribute($value)
    {
        $this->attributes['which'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setParkAttribute($value)
    {
        $this->attributes['park'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setPlaceAttribute($value)
    {
        $this->attributes['place'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setUpzAttribute($value)
    {
        $this->attributes['upz'] = toUpper($value);
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function professional()
    {
        return $this->belongsTo( Profile::class, 'user_id', 'Id_Persona' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function summoner()
    {
        return $this->belongsTo( Summoner::class, 'who_summons_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function request_type()
    {
        return $this->belongsTo( RequestType::class, 'request_type_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function process()
    {
        return $this->belongsTo( Process::class, 'process_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reunion_type()
    {
        return $this->belongsTo( ReunionType::class, 'reunion_type_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function programming_themes()
    {
        return $this->belongsToMany( Theme::class, 'programmings_themes', 'programming_id')
                     ->withPivot('other', 'theme_id', 'id')
                     ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function programming_activities()
    {
        return $this->belongsToMany( Activity::class, 'programmings_activities', 'programming_id')
                    ->withPivot('activity_id', 'description', 'id')
                    ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function programming_files()
    {
        return $this->belongsToMany( FileType::class, 'programming_files', 'programming_id')
                    ->withPivot('file_type_id', 'file_name', 'path', 'id')
                    ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function programming_images()
    {
        return $this->hasMany( ProgrammingImage::class,  'programming_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function programming_commitments()
    {
        return $this->hasMany( ProgrammingCommitment::class,  'programming_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function execution()
    {
        return $this->hasMany(Execution::class, 'programming_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reprogrammings()
    {
        return $this->hasMany(Reprogramming::class, 'programming_id', 'id' );
    }
}
