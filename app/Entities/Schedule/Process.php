<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Process extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'processes';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'process'
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setProcessAttribute($value)
    {
        $this->attributes['process'] = toUpper($value);
    }
}
