<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;

class CitizenAttentionSchedule extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'citizen_attention_schedules';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'schedule'
    ];
}
