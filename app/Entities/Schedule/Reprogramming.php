<?php


namespace IDRDApp\Entities\Schedule;


use Illuminate\Database\Eloquent\Model;

class Reprogramming extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reprogrammings';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'programming_ig',
        'reason',
        'initial_hour',
        'final_hour',
        'date'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'programming_ig'                =>  'int',
        'date'                          =>  'date'
    ];

    /*
     *
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setReasonAttribute($value)
    {
        $this->attributes['reason'] = toUpper($value);
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */


}