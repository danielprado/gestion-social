<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;

class CitizenFile extends Model
{
    const FILE_PATH = '/storage/citizen_files';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'citizen_files';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'citizen_id',
        'file_name',
        'path'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'citizen_id'    =>  'int',
        'file_name'     =>  'string',
        'path'          =>  'string',
    ];


    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * @param $path
     * @return string
     */
    public function getPathAttribute( $path )
    {
        return file_exists( public_path(self::FILE_PATH.'/'.$path ) )
            ? asset( 'public'.self::FILE_PATH.'/'.$path )
            : null;
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function citizens()
    {
        return $this->belongsTo( CitizenAttention::class, 'citizen_id' ,'id' );
    }
}
