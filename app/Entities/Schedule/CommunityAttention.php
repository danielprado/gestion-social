<?php

namespace IDRDApp\Entities\Schedule;

use IDRDApp\Entities\Security\Profile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommunityAttention extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'community_attentions';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'execution_date',
        'initial_hour',
        'final_hour',
        'user_id'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'who_cancel',
        'reason_for_cancellation',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'execution_date'    =>  'date',
        'user_id'           =>  'int',
    ];

    /*
     *
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */


    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setWhoCancelAttribute($value)
    {
        $this->attributes['who_cancel'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setReasonForCancellationAttribute($value)
    {
        $this->attributes['reason_for_cancellation'] = toUpper($value);
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo( Profile::class, 'user_id', 'Id_Persona' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany( CommunityFile::class,  'community_id', 'id');
    }
}
