<?php


namespace IDRDApp\Entities\Schedule;


use Illuminate\Database\Eloquent\Model;

class CitizenAttentionReprogramming extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reprogramming_citizen_attentions';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'citizen_attention_id',
        'schedule_id',
        'reason',
        'date'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'citizen_attention_id'          =>  'int',
        'schedule_id' =>  'int',
        'date'                          =>  'date'
    ];

    /*
     *
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setReasonAttribute($value)
    {
        $this->attributes['reason'] = toUpper($value);
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schedule()
    {
        return $this->belongsTo(CitizenAttentionSchedule::class, 'schedule_id', 'id' );
    }
}