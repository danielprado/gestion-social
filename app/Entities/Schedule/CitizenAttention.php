<?php

namespace IDRDApp\Entities\Schedule;

use IDRDApp\Entities\Security\Profile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CitizenAttention extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'citizen_attentions';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'execution_date',
        'citizen_attention_schedule_id',
        'user_id'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'who_cancel',
        'reason_for_cancellation',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'execution_date'    =>  'date',
        'citizen_attention_schedule_id' =>  'int',
        'user_id' =>  'int',
    ];

    /*
     *
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */


    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setWhoCancelAttribute($value)
    {
        $this->attributes['who_cancel'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setReasonForCancellationAttribute($value)
    {
        $this->attributes['reason_for_cancellation'] = toUpper($value);
    }

    /*
     * ---------------------------------------------------------
     * Query scopes
     * ---------------------------------------------------------
     */
    public function scopeUserHasNotProgrammed( $query, $date )
    {
        return $query->where([
            ['execution_date', $date],
            ['user_id', auth()->user()->id]
        ]);
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function schedule()
    {
        return $this->hasOne( CitizenAttentionSchedule::class, 'id', 'citizen_attention_schedule_id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo( Profile::class, 'user_id', 'Id_Persona' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reprogramming()
    {
        return $this->hasMany(CitizenAttentionReprogramming::class, 'citizen_attention_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany( CitizenFile::class,  'citizen_id', 'id');
    }
}
