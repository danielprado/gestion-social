<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;

class Execution extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'executions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'programming_id',
        'entity_id',
        'type_of_population_id',
        'condition_id',
        'situation_id',
        'f_0_5',
        'm_0_5',
        'f_6_12',
        'm_6_12',
        'f_13_17',
        'm_13_17',
        'f_18_26',
        'm_18_26',
        'f_27_59',
        'm_27_59',
        'f_60_more',
        'm_60_more',
        'observation'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'programming_id'        =>  'int',
        'entity_id'             =>  'int',
        'type_of_population_id' =>  'int',
        'condition_id'          =>  'int',
        'situation_id'          =>  'int',
        'f_0_5'                 =>  'int',
        'm_0_5'                 =>  'int',
        'f_6_12'                =>  'int',
        'm_6_12'                =>  'int',
        'f_13_17'               =>  'int',
        'm_13_17'               =>  'int',
        'f_18_26'               =>  'int',
        'm_18_26'               =>  'int',
        'f_27_59'               =>  'int',
        'm_27_59'               =>  'int',
        'f_60_more'             =>  'int',
        'm_60_more'             =>  'int',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setObservationAttribute($value)
    {
        $this->attributes['observation'] = toUpper($value);
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function entity()
    {
        return $this->belongsTo( Entity::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function population()
    {
        return $this->belongsTo( TypeOfPopulation::class, 'type_of_population_id', 'id' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function condition()
    {
        return $this->belongsTo( Condition::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function situation()
    {
        return $this->belongsTo( Situation::class );
    }
}
