<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequestType extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'request_types';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'request_type'
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setRequestTypeAttribute($value)
    {
        $this->attributes['request_type'] = toUpper($value);
    }
}
