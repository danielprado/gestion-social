<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReunionType extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reunion_types';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'reunion_type', 'require_process'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'require_process'   =>  'boolean'
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setReunionTypeAttribute($value)
    {
        $this->attributes['reunion_type'] = toUpper($value);
    }
}
