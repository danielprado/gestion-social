<?php


namespace IDRDApp\Entities\Schedule;


use Illuminate\Database\Eloquent\Model;

class ProgrammingImage extends Model
{
    const IMAGE_PATH = '/storage/images';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "programming_images";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'programming_id',
        'file_name',
        'confirmation',
        'description',
        'path',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'programming_id'    =>  'int',
        'confirmation'      =>  'boolean',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * @param $path
     * @return string
     */
    public function getPathAttribute( $path )
    {
        return $path && file_exists( public_path(self::IMAGE_PATH.'/'.$path ) )
            ? asset( 'public'.self::IMAGE_PATH.'/'.$path )
            : asset('public/img/img_404.png');
    }
}