<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;

class ProgrammingFile extends Model
{
    const FILE_PATH = '/storage/files';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programming_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_type_id',
        'programming_id',
        'file_name',
        'path',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'file_type_id'      =>  'int',
        'programming_id'    =>  'int',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * @param $path
     * @return string
     */
    public function getPathAttribute( $path )
    {
        return file_exists( public_path(self::FILE_PATH.'/'.$path ) )
                ? asset( 'public'.self::FILE_PATH.'/'.$path )
                : asset('public/img/img_404.png');
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file_type()
    {
        return $this->belongsTo(FileType::class, 'file_type_id');
    }
}
