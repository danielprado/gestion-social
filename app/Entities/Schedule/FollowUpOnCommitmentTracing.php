<?php

namespace IDRDApp\Entities\Schedule;

use Illuminate\Database\Eloquent\Model;

class FollowUpOnCommitmentTracing extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'follow_up_on_commitments_tracing';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected  $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $fillable = [
        'date',
        'description',
        'follow_commitment_id',
    ];

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date'                  =>  'date',
        'follow_commitment_id'  =>  'int',
    ];

    /*
    * ---------------------------------------------------------
    * Accessors and Mutator Attributes
    * ---------------------------------------------------------
    */

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setResponsableAttribute($value)
    {
        $this->attributes['responsable'] = toUpper($value);
    }

    /**
     * Set the text to uppercase.
     *
     * @param  string  $value
     * @return string
     */
    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = toUpper($value);
    }
}
