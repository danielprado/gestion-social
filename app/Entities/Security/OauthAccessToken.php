<?php

namespace IDRDApp\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class OauthAccessToken extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oauth_access_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'session_id', 'expire_time'];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Refresh Token
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function refresh_token()
    {
        return $this->hasOne( OauthRefreshToken::class, 'access_token_id');
    }
}
