<?php

namespace IDRDApp\Entities\Security;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_users';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'acceso';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Persona';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Usuario', 'Contrasena'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['Contrasena', 'password'];


    /*
     * ---------------------------------------------------------
     * Accessors and Mutator Attributes
     * ---------------------------------------------------------
     */

    /**
     * Get the user's id.
     *
     * @return int
     */
    public function getIdAttribute()
    {
        return (int) $this->Id_Persona;
    }

    /**
     * Get the user's username.
     *
     * @return int
     */
    public function getUsernameAttribute()
    {
        return $this->Usuario;
    }

    /**
     * Get the user's password.
     *
     * @return int
     */
    public function getPasswordAttribute()
    {
        return $this->Contrasena;
    }

    /**
     * Set the user's username.
     *
     * @param  string  $value
     * @return string
     */
    public function setUsernameAttribute($value)
    {
        $this->attributes['Usuario'] = strtolower($value);
    }

    /**
     * Set the user's password.
     *
     * @param  string  $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['Contrasena'] = $this->hash( $value );
    }


    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->Contrasena;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // do nothing
    }


    /**
     * Hash the password
     *
     * @param $password
     * @return string
     */
    public function hash( $password )
    {
        $k = 18;
        $C = '';
        for( $i=0; $i < strlen( $password ) ; $i++ ) $C.=chr((ord( $password [$i])+$k)%255);
        return sha1( $C );
    }


    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Get the user profile.
     *
     * @return HasOne
     */
    public function profile()
    {
        return $this->hasOne( Profile::class, 'Id_Persona' );
    }

    /**
     * Get the user permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions()
    {
        return $this->hasMany( Permissions::class, 'Id_Persona' );
    }
}
