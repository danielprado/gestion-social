<?php

namespace IDRDApp\Entities\Security;

use Illuminate\Database\Eloquent\Model;

class OauthRefreshToken extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'oauth_refresh_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'access_token_id', 'expire_time'];

    /*
     * ---------------------------------------------------------
     * Eloquent Relationships
     * ---------------------------------------------------------
     */

    /**
     * Access Token
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function access_token()
    {
        return $this->belongsTo( OauthAccessToken::class, 'id');
    }
}
