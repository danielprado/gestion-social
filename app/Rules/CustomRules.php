<?php


namespace IDRDApp\Rules;


use IDRDApp\Entities\Schedule\CitizenAttention;
use IDRDApp\Entities\Schedule\Programming;
use IDRDApp\Entities\Schedule\ReunionType;
use IDRDApp\Entities\Schedule\Summoner;

class CustomRules
{
    /**
     * Validate if the process field is require
     * 
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validateIsRequireProcess($attribute, $value, $parameters)
    {
        if ( is_null($value) ) {
            $values = array_slice($parameters, 1);
            $reunion_type = ReunionType::query()->where('id', $values)->first();
            return isset( $reunion_type->require_process ) ? !$reunion_type->require_process : true;
        }
        return true;
    }

    /**
     * Validate if a schedule exists between the given date and hours 
     * or have some conflict
     * 
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validateProgrammingSchedules($attribute, $value, $parameters)
    {
        if ( is_null($value) ) {
            return false;
        } else {
            $start = isset( $parameters[0] ) ? $parameters[0] : null;
            $end   = isset( $parameters[1] ) ? $parameters[1] : null;
            if ( is_null( $start ) || is_null( $end ) ) {
                return false;
            }
            $except_id = isset( $parameters[2] ) ? $parameters[2] : null;
            $exist = Programming::where([
                ['execution_date', $value],
                ['user_id', auth()->user()->id]
            ])->where(function ($query) use ( $start, $end ) {
                $query->whereBetween('initial_hour', [ $start, $end ])
                    ->orWhereBetween('final_hour', [ $start, $end ])
                    ->orWhere(function ($query) use ($start, $end){
                        $query->where([
                            ['initial_hour', '<', $start],
                            ['final_hour', '>', $start]
                        ])->orWhere([
                            ['initial_hour', '<', $end],
                            ['final_hour', '>', $end]
                        ]);
                    });
            })->when( $except_id, function ($query) use ($except_id) {
                return $query->where('id', "!=", $except_id);
            })->count();

            return $exist > 0 ? false : true;
        }
    }

    /**
     * Validate if which summons filed is required
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validateIsRequireWhichSummons($attribute, $value, $parameters)
    {
        $values = isset( $parameters[0] ) ? $parameters[0] : null;

        if ( is_null( $values ) ) {
            return false;
        } else if ( is_null( $value ) && ! is_null( $values ) ) {
            $summoner = Summoner::query()->where('id', $values)->first();
            return isset( $summoner->additional_input ) ? !$summoner->additional_input : true;
        } else {
            return true;
        }
    }

    /**
     * Validate if citizen attention exists as duplicate
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @return bool
     */
    public function validateCitizenAttentionSchedule($attribute, $value, $parameters)
    {

        if ( is_null( $value ) ) {
            return false;
        } else {
            $except_id   = isset( $parameters[0] ) ? $parameters[0] : null;

            $wheres_update = [
                ['id', "!=", $except_id],
                ['execution_date', $value],
                ['user_id', auth()->user()->id]
            ];

            $wheres = [
                ['execution_date', $value],
                ['user_id', auth()->user()->id]
            ];

            $array_wheres = isset( $except_id ) ? $wheres_update : $wheres;
            $exist = CitizenAttention::where( $array_wheres )->count();
            return $exist > 0 ? false : true;
        }
    }
}