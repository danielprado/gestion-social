<?php

use Illuminate\Support\Facades\DB;

if ( !function_exists('toUpper') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toUpper( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
        }

        return null;
    }
}

if ( !function_exists('isAValidDate') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param $date
     * @param string $format
     * @return bool
     */
    function isAValidDate( $date, $format = 'Y-m-d' )
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

if ( !function_exists('isCovenant') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param null $park_code
     * @param array $activities
     * @return boolean
     */
    function isCovenant($park_code = null, $activities = [] )
    {
        if ( count( $activities ) > 0 ) {
            $count = DB::table('covenant_2018')
                         ->where( 'park_code', $park_code )
                         ->whereIn('activity_id', collect( $activities )->pluck('id')->toArray() )->count();

            return ( $count > 0 ) ? true : false;
        }

        return false;
    }
}

if ( !function_exists('isAgreement') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param null $park_code
     * @param array $activities
     * @return boolean
     */
    function isAgreement($park_code = null, $activities = [] )
    {
        if ( count( $activities ) > 0 ) {
            $count = DB::table('agreements_2018')
                ->where( 'park_code', $park_code )
                ->whereIn('activity_id', collect( $activities )->pluck('id')->toArray() )->count();

            return ( $count > 0 ) ? true : false;
        }

        return false;
    }
}

if ( !function_exists('isWorkTable') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param null $park_code
     * @param array $activities
     * @return boolean
     */
    function isWorkTable($park_code = null, $activities = [] )
    {
        if ( count( $activities ) > 0 ) {
            $count = DB::table('work_table_2018')
                ->where( 'park_code', $park_code )
                ->whereIn('activity_id', collect( $activities )->pluck('id')->toArray() )->count();

            return ( $count > 0 ) ? true : false;
        }

        return false;
    }
}