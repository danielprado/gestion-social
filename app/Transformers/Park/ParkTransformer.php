<?php


namespace IDRDApp\Transformers\Park;


use IDRDApp\Entities\Parks\Park;
use IDRDApp\Entities\Parks\Upz;
use League\Fractal\TransformerAbstract;

class ParkTransformer extends TransformerAbstract
{
    public function transform(Park $data)
    {
        $name       = isset( $data->Nombre ) ? toUpper($data->Nombre) : '';
        $address    = isset( $data->Direccion ) ? toUpper( $data->Direccion ) : '';
        $code       = isset( $data->Id_IDRD ) ? $data->Id_IDRD : null;
        return [
            'id'        =>  isset( $data->Id )      ? (int) $data->Id : 0,
            'code'      =>  $code,
            'name'      =>  "{$code} - {$name} / {$address}",
            'upz'       =>  isset( $data->Upz ) ? $data->Upz : null,
            'upz_name'  =>  isset( $data->Upz ) ? toUpper( "{$data->Upz} - {$this->includeUpz( $data->Upz )}" ) : null,
        ];
    }

    public function includeUpz( $upz = null )
    {
        if ( isset( $upz ) ) {
            $data = Upz::query()->where('cod_upz', $upz)->first();
            return isset( $data->Upz ) ? $data->Upz : '';
        } else {
            return '';
        }
    }
}