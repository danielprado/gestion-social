<?php


namespace IDRDApp\Transformers\Security;


use IDRDApp\Entities\Security\Permissions;
use League\Fractal\TransformerAbstract;

class PermissionsTransformer extends TransformerAbstract
{
    public function transform( Permissions $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'name'      =>  isset( $data->activity ) ? $data->activity : null,
            'activity_id'   =>  isset( $data->activity_id ) ? (int) $data->activity_id : null,
            'status'        =>  isset( $data->status ) ? (int) $data->status : null,
        ];
    }
}