<?php


namespace IDRDApp\Transformers\Security;


use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;
use IDRDApp\Entities\Security\Profile;

class ProfileTransformer extends TransformerAbstract
{
    public function transform(Profile $data)
    {
        return [
            'id'                =>  isset( $data->Id_Persona ) ? (int) $data->Id_Persona : null,
            'document'          =>  isset( $data->Cedula ) ? $data->Cedula : '',
            'document_type_id'  =>  isset( $data->Id_TipoDocumento ) ? (int) $data->Id_TipoDocumento : null,
            'document_type'     =>  isset( $data->Nombre_TipoDocumento ) ? $data->Nombre_TipoDocumento : '',
            'document_type_description'    =>  isset( $data->Descripcion_TipoDocumento ) ? $data->Descripcion_TipoDocumento : '',
            'full_name'         => isset( $data->full_name ) ? $data->full_name : '',
            'part_name'         => isset( $data->part_name ) ? $data->part_name : '',
            'lastname'          =>  isset( $data->Primer_Apellido ) ? $data->Primer_Apellido : '',
            'second_lastname'   =>  isset( $data->Segundo_Apellido ) ? $data->Segundo_Apellido : '',
            'first_name'        =>  isset( $data->Primer_Nombre ) ? $data->Primer_Nombre : '',
            'second_name'       =>  isset( $data->Segundo_Nombre ) ? $data->Segundo_Nombre : '',
            'birthday'          =>  isset( $data->Fecha_Nacimiento ) ? $data->Fecha_Nacimiento : '',
            'country_id'        =>  isset( $data->Id_Pais ) ? (int) $data->Id_Pais : null,
            'city'              =>  isset( $data->Nombre_Ciudad ) ? $data->Nombre_Ciudad : '',
            'gender_id'         =>  isset( $data->Id_Genero ) ? (int) $data->Id_Genero : null,
            'ethnicity_id'      =>  isset( $data->Id_Etnia ) ? (int) $data->Id_Etnia : null,
            'department_id'     =>  isset( $data->Id_Departamento ) ? (int) $data->Id_Departamento : null,
            'gender'            =>  isset( $data->Nombre_Genero ) ? $data->Nombre_Genero : '',
            'country'           =>  isset( $data->Nombre_Pais ) ? $data->Nombre_Pais : '',
            'ethnicity'         =>  isset( $data->Nombre_Etnia ) ? $data->Nombre_Etnia : '',
            'permissions'       =>  isset( $data->permissions ) ? $this->includePermissions( $data ) : []
        ];
    }

    public function includePermissions( Profile $data )
    {
        $permissions = $data->permissions()->inThisModule()->get();
        $manager = new Manager();
        $resource = $this->collection( $permissions, new PermissionsTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}