<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\ProgrammingCommitment;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class CommitmentTransformer extends TransformerAbstract
{
    public function transform(ProgrammingCommitment $data)
    {

        $title = '';
        if ( isset( $data->programing->park ) ) {
            $title = $data->programing->park;
        }

        if ( isset( $data->programing->title ) ) {
            $title = $data->programing->title;
        }

        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'programming_id'=>  isset( $data->programming_id ) ? (int) $data->programming_id : null,
            'programming'   =>  $title,
            'responsable'   =>  isset( $data->responsable ) ? $data->responsable : null,
            'date'          =>  isset( $data->date ) ? $data->date->format('Y-m-d') : null,
            'professional_name' => isset( $data->programing->full_name) ? $data->programing->full_name : null,
            'description'   =>  isset( $data->description ) ? $data->description : null,
            'tracings'      =>  isset( $data->tracings )   ? $this->includeTracing( $data ) : [],
            'traced'        =>  (boolean)  ( $data->tracings()->count() > 0 ),
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function includeTracing( ProgrammingCommitment $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->tracings, new TracingTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}