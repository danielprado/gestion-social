<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Security\Profile;
use League\Fractal\TransformerAbstract;

class ProfessionalTransformer extends TransformerAbstract
{
    public function transform( Profile $data )
    {
        return [
            'id'    =>  isset( $data->id )        ? $data->id : null,
            'name'  =>  isset( $data->full_name ) ? toUpper( $data->full_name ) : null,
        ];
    }
}