<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\CitizenAttentionReprogramming;
use IDRDApp\Entities\Schedule\Reprogramming;
use League\Fractal\TransformerAbstract;

class ReprogrammingTransformer extends TransformerAbstract
{
    public function transform( Reprogramming $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'programming_id'=>  isset( $data->programming_id ) ? (int) $data->programming_id : null,
            'date'          =>  isset( $data->date ) ? $data->date->format('Y-m-d') : null,
            'initial_hour'  =>  isset( $data->initial_hour ) ? substr($data->initial_hour, 0, 5) : null,
            'final_hour'    =>  isset( $data->final_hour ) ? substr($data->final_hour, 0, 5) : null,
            'reason'        =>  isset( $data->reason ) ? $data->reason : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}