<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\CitizenAttentionView;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class CitizenAttentionViewTransformer extends TransformerAbstract
{
    /**
     * @param CitizenAttentionView $data
     * @return array
     */
    public function transform(CitizenAttentionView $data )
    {
        return [
            'id'                             =>  isset( $data->id ) ? (int) $data->id : null,
            'color'                          => 'success',
            'place'                          => 'IDRD - Calle 63 # 59A-06',
            'execution_date'                 =>  isset( $data->execution_date ) ? $data->execution_date->format('Y-m-d') : null,
            'citizen_attention_schedule_id'  =>  isset( $data->citizen_attention_schedule_id ) ? (int) $data->citizen_attention_schedule_id : null,
            'citizen_attention_schedule'     =>  isset( $data->citizen_attention_schedule ) ? $data->citizen_attention_schedule : null,
            'schedule'                       =>  isset( $data->citizen_attention_schedule ) ? $data->citizen_attention_schedule : null,
            'professional'                   =>  isset( $data->user_id ) ? (int) $data->user_id : null,
            'professional_name'              =>  isset( $data->full_name ) ? $data->full_name : null,
            'who_cancel'                     =>  isset( $data->who_cancel ) ? $data->who_cancel : null,
            'reason_for_cancellation'        =>  isset( $data->reason_for_cancellation ) ? $data->reason_for_cancellation : null,
            'has_reprogramming'              =>  isset( $data->reprogramming ) ? $data->reprogramming->count() : 0,
            "reprogramming_reason"           =>  null, //KEY ONLY FOR REPROGRAMMING FORM
            'reprogramming'                  =>  isset( $data->reprogramming ) ? $this->includeTracing($data) : [],
            'files'                          =>  isset( $data->files ) ? $this->includeFiles($data) : [],
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'deleted_at'    =>  isset( $data->deleted_at ) ? $data->deleted_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function includeTracing( CitizenAttentionView $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->reprogramming, new CitizenAttentionReprogrammingTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }

    public function includeFiles( CitizenAttentionView $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->files, new CitizenFilesTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}