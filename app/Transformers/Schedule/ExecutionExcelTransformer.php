<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\Execution;
use League\Fractal\TransformerAbstract;

class ExecutionExcelTransformer extends TransformerAbstract
{
    public function transform(Execution $data)
    {
        return [
            'id'                        =>      isset($data->id) ? (int) $data->id : null,
            'entity'                    =>      isset($data->entity->entity) ? $data->entity->entity : null,
            'type_of_population'        =>      isset($data->population->type_of_population) ? $data->population->type_of_population : null,
            'condition'                 =>      isset($data->condition->condition) ? $data->condition->condition : null,
            'situation'                 =>      isset($data->situation->situation) ? $data->situation->situation : null,
            'f_0_5'                     =>      isset($data->f_0_5) ? (int) $data->f_0_5 : 0,
            'm_0_5'                     =>      isset($data->m_0_5) ? (int) $data->m_0_5 : 0,
            'f_6_12'                    =>      isset($data->f_6_12) ? (int) $data->f_6_12 : 0,
            'm_6_12'                    =>      isset($data->m_6_12) ? (int) $data->m_6_12 : 0,
            'f_13_17'                   =>      isset($data->f_13_17) ? (int) $data->f_13_17 : 0,
            'm_13_17'                   =>      isset($data->m_13_17) ? (int) $data->m_13_17 : 0,
            'f_18_26'                   =>      isset($data->f_18_26) ? (int) $data->f_18_26 : 0,
            'm_18_26'                   =>      isset($data->m_18_26) ? (int) $data->m_18_26 : 0,
            'f_27_59'                   =>      isset($data->f_27_59) ? (int) $data->f_27_59 : 0,
            'm_27_59'                   =>      isset($data->m_27_59) ? (int) $data->m_27_59 : 0,
            'f_60_more'                 =>      isset($data->f_60_more) ? (int) $data->f_60_more : 0,
            'm_60_more'                 =>      isset($data->m_60_more) ? (int) $data->m_60_more : 0,
            'subtotal'                  =>      isset($data->subtotal) ? (int) $data->subtotal : 0,
            'observation'               =>      isset($data->observation) ? $data->observation : null,
            'created_at'                =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null
        ];
    }
}