<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\ProgrammingFile;
use IDRDApp\Entities\Schedule\ProgrammingImage;
use League\Fractal\TransformerAbstract;

class ProgrammingImageTransformer extends TransformerAbstract
{
    public function transform( ProgrammingImage $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'programming_id'=>  isset( $data->programming_id ) ? (int) $data->programming_id : null,
            'file_name'     =>  isset( $data->file_name ) ? $data->file_name : null,
            'path'          =>  isset( $data->path ) ? $data->path : null,
            'confirmation'  =>  isset( $data->confirmation ) ? (boolean) $data->confirmation : false,
            'description'   =>  isset( $data->description ) ? $data->description : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}