<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Parks\Park;
use IDRDApp\Entities\Schedule\Process;
use IDRDApp\Entities\Schedule\Programming;
use IDRDApp\Entities\Schedule\ProgrammingView;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class ProgrammingTransformer extends TransformerAbstract
{
    public function transform(Programming $data)
    {
        $initial_hour = isset( $data->initial_hour ) ? substr($data->initial_hour, 0, 5) : null;
        $final_hour   = isset( $data->final_hour ) ? substr($data->final_hour, 0, 5) : null;
        $date         = isset( $data->execution_date ) ? $data->execution_date->format('Y-m-d') : null;
        $start        = trim( "{$date} {$initial_hour}" );
        $final        = trim( "{$date} {$final_hour}" );

        $park_id = isset( $data->park_id ) ? (int) $data->park_id : null;
        $park = Park::query()->select('Id, Id_IDRD')->where( 'Id', $park_id )->first();
        $park_code = isset( $park->Id_IDRD ) ?  $park->Id_IDRD : null;
        $activities = isset( $data->programming_activities ) ? $data->programming_activities : [];
        $title = isset( $data->title ) ? $data->title : null;

        if ( is_null( $title ) || !$title ) {
            $title = isset( $data->park ) ?  $data->park : null;
        }

        return [
            'id'                             =>  isset( $data->id ) ? (int) $data->id : null,
            'color'                          =>  $this->isExecuted( $data ) ? 'grey-light' : 'warning',
            'is_executed'                    =>  $this->isExecuted( $data ),
            'name'                           =>  $title,
            'execution_date'                 =>  $date,
            'start'                          =>  $start,
            'end'                            =>  $final,
            'professional'                   =>  isset( $data->user_id ) ? (int) $data->user_id : null,
            'professional_name'              =>  isset( $data->professional->part_name ) ? $data->professional->part_name : null,
            'initial_hour'                   =>  $initial_hour,
            'final_hour'                     =>  $final_hour,
            'place'                          =>  isset( $data->place ) ? $data->place : null,
            'reunion_type_id'                =>  isset( $data->reunion_type_id ) ? (int) $data->reunion_type_id : null,
            'reunion_type'                   =>  isset( $data->reunion_type->reunion_type ) ? $data->reunion_type->reunion_type : null,
            'park'                           =>  isset( $data->park ) ?  $data->park : null,
            'upz'                            =>  isset( $data->upz ) ?  $data->upz : null,
            'upz_id'                         =>  isset( $data->upz_id ) ? (int) $data->upz_id : null,
            'process_id'                     =>  isset( $data->process_id ) ? (int) $data->process_id : null,
            'has_themes'                     =>  isset( $data->process_id ) ?  $this->includeHasThemes( $data->process_id ) : false,
            'process'                        =>  isset( $data->process->process ) ? $data->process->process : null,
            'park_id'                        =>  $park_id,
            'who_summons_id'                 =>  isset( $data->who_summons_id ) ? (int) $data->who_summons_id : null,
            'who_summons'                    =>  isset( $data->summoner->summoner ) ? $data->summoner->summoner : null,
            'which'                          =>  isset( $data->which ) ? $data->which : null,
            'request_type_id'                =>  isset( $data->request_type_id ) ? (int) $data->request_type_id : null,
            'request'                        =>  isset( $data->request_type->request_type ) ? $data->request_type->request_type : null,
            'objective'                      =>  isset( $data->objective ) ?  $data->objective : null,
            'who_cancel'                     =>  isset( $data->who_cancel ) ? $data->who_cancel : null,
            'reason_for_cancellation'        =>  isset( $data->reason_for_cancellation ) ? $data->reason_for_cancellation : null,

            'has_reprogramming'              =>  isset( $data->reprogrammings ) ? $data->reprogrammings->count() : 0,
            "reprogramming_reason"           =>  null, //KEY ONLY FOR REPROGRAMMING FORM
            'reprogramming'                  =>  isset( $data->reprogrammings ) ? $this->includeTracing($data) : [],

            'activities'                     =>  $activities,
            'is_covenant'                    =>  isCovenant($park_code, $activities)  ? 'PACTOS 2018' : null,
            'is_agreement'                   =>  isAgreement($park_code, $activities) ? 'ACUERDOS 2018' : null,
            'is_work_table'                  =>  isWorkTable($park_code, $activities) ? 'MESAS 2018' : null,

            'include_attendance'             =>  isset( $data->include_attendance ) ?  (boolean) $data->include_attendance : true,
            'include_files'                  =>  isset( $data->include_files ) ?  (boolean) $data->include_files : true,
            'include_images'                 =>  isset( $data->include_images ) ?  (boolean) $data->include_images : true,
            'include_commitments'            =>  isset( $data->include_commitments ) ?  (boolean) $data->include_commitments : true,

            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'deleted_at'    =>  isset( $data->deleted_at ) ? $data->deleted_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function includeHasThemes( $id = null )
    {
        if ( isset( $d ) ) {
            $process = Process::query()->where('process', 'SOSTENIBILIDAD SOCIAL')
                                       ->where('id', $id)->first();
            return isset( $process->id ) ? true : false;
        }
        return false;
    }

    public function includeTracing( Programming $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->reprogrammings, new ReprogrammingTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }

    public function isExecuted( ProgrammingView $programming )
    {
        if ( ($programming->execution()->count() > 0 || ! (boolean) $programming->include_attendance ) &&
            ($programming->programming_files()->count() > 0  || ! (boolean) $programming->include_files )  &&
            ($programming->programming_images()->count() > 0 || ! (boolean) $programming->include_images)  &&
            ($programming->programming_commitments()->count() > 0 || ! (boolean) $programming->include_commitments)  ) {

            /*
            if ( $this->includeHasThemes( $programming->process_id ) ) {
                if ( $programming->programming_themes()->count() > 0 ) {
                    return true;
                }
                return false;
            }
            */

            return true;
        } else {
            return false;
        }
    }
}