<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\TypeOfPopulation;
use League\Fractal\TransformerAbstract;

class TypeOfPopulationTransformer extends TransformerAbstract
{
    public function transform( TypeOfPopulation $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'name'          =>  isset( $data->type_of_population ) ? $data->type_of_population : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'deleted_at'    =>  isset( $data->deleted_at ) ? $data->deleted_at->format('Y-m-d H:i:s') : null,
        ];
    }
}