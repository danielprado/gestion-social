<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\ProgrammingFile;
use League\Fractal\TransformerAbstract;

class ProgrammingFileTransformer extends TransformerAbstract
{
    public function transform( ProgrammingFile $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'file_type_id'  =>  isset( $data->file_type_id ) ? (int) $data->file_type_id : null,
            'file_type'     =>  isset( $data->file_type->file_type ) ? $data->file_type->file_type : null,
            'programming_id'=>  isset( $data->programming_id ) ? (int) $data->programming_id : null,
            'file_name'     =>  isset( $data->file_name ) ? $data->file_name : null,
            'path'          =>  isset( $data->path ) ? $data->path : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}