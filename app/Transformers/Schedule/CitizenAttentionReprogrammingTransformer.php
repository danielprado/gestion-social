<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\CitizenAttentionReprogramming;
use League\Fractal\TransformerAbstract;

class CitizenAttentionReprogrammingTransformer extends TransformerAbstract
{
    public function transform( CitizenAttentionReprogramming $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'citizen_attention_id'  =>  isset( $data->citizen_attention_id ) ? (int) $data->citizen_attention_id : null,
            'schedule_id'   =>  isset( $data->schedule_id ) ? (int) $data->schedule_id : null,
            'date'          =>  isset( $data->date ) ? $data->date->format('Y-m-d') : null,
            'reason'        =>  isset( $data->reason ) ? $data->reason : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}