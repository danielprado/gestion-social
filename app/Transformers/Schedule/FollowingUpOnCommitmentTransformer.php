<?php


namespace IDRDApp\Transformers\Schedule;


use Carbon\Carbon;
use IDRDApp\Entities\Parks\Location;
use IDRDApp\Entities\Parks\Upz;
use IDRDApp\Entities\Schedule\FollowUpOnCommitment;
use IDRDApp\Entities\Schedule\FollowUpOnCommitmentTracing;
use IDRDApp\Entities\Schedule\ProgrammingCommitment;
use Illuminate\Support\Str;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class FollowingUpOnCommitmentTransformer extends TransformerAbstract
{
    public function transform(FollowUpOnCommitment $data)
    {
        $array = [];
        $array[] = [
            'id'            =>  toUpper(Str::random(3)),
            'park_id'       =>  isset( $data->park_id ) ? (int) $data->park_id : null,
            'park'          =>  isset( $data->park->Nombre ) ? toUpper($data->park->Nombre) : null,
            'park_code'     =>  isset( $data->park->Id_IDRD ) ? toUpper($data->park->Id_IDRD) : null,
            'location_id'   =>  isset( $data->park->Id_Localidad ) ? (int) $data->park->Id_Localidad : null,
            'location'      =>  isset( $data->park->Id_Localidad ) ? toUpper($this->getLocation( $data->park->Id_Localidad )) : null,
            'upz_id'        =>  isset( $data->park->Upz ) ? $data->park->Upz : null,
            'upz'           =>  isset( $data->park->Upz ) ? toUpper($this->getUpz( $data->park->Upz )) : null,
            'programming_date'  =>  isset( $data->date ) ? $data->date->format('Y-m-d') : null,
            'programming_desc'  =>  isset( $data->description ) ? toUpper($data->description) : null,
            'programming_act'   =>  '',
            'date'          =>  isset( $data->date ) ? $data->date->format('Y-m-d') : null,
            'responsable'   =>  isset( $data->responsable ) ? toUpper($data->responsable) : null,
            'description'   =>  isset( $data->description ) ? toUpper($data->description) : null,
            'professional'  => isset( $data->user_id) ? $data->user_id : null,
            'professional_name' => isset( $data->professional->full_name) ? toUpper($data->professional->full_name) : null,
            'status'        => isset( $data->date) ? $this->greaterThanOneYear( $data->date ) : null,
            'tracings'      =>  isset( $data->tracings )   ? $this->includeTracing( $data ) : [],
            'commitment_id' =>  isset( $data->id ) ? (int) $data->id : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'files'             =>  '',
            'images'            =>  '',
        ];

        return $array;
    }

    public function includeTracing( FollowUpOnCommitment $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->tracings, new FollowingUpOnCommitmentTracingTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }

    public function getUpz($upz_code = null)
    {
        if (isset($upz_code)) {
            $upz = Upz::query()->where('cod_upz', $upz_code)->first();
            return isset($upz->Upz) ? $upz->Upz : null;
        }
        return null;
    }

    public function getLocation($id = null)
    {
        if (isset($id)) {
            $location = Location::query()->where('Id_Localidad', $id)->first();
            return isset($location->Localidad) ? $location->Localidad : null;
        }
        return null;
    }

    public function greaterThanOneYear($date)
    {
        return Carbon::parse($date)->age >= 1 ? 'VENCIDO' : 'VIGENTE';
    }
}