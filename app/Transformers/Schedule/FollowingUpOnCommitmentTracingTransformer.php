<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\FollowUpOnCommitmentTracing;
use League\Fractal\TransformerAbstract;

class FollowingUpOnCommitmentTracingTransformer extends TransformerAbstract
{
    public function transform(FollowUpOnCommitmentTracing $data)
    {
        return [
            'id'                         =>  isset( $data->id ) ? (int) $data->id : null,
            'follow_commitment_id'       =>  isset( $data->follow_commitment_id ) ? (int) $data->follow_commitment_id : null,
            'date'          =>  isset( $data->date ) ? $data->date->format('Y-m-d') : null,
            'description'   =>  isset( $data->description ) ? $data->description : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}