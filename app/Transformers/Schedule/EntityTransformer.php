<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\Entity;
use League\Fractal\TransformerAbstract;

class EntityTransformer extends TransformerAbstract
{
    public function transform( Entity $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'name'          =>  isset( $data->entity ) ? $data->entity : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'deleted_at'    =>  isset( $data->deleted_at ) ? $data->deleted_at->format('Y-m-d H:i:s') : null,
        ];
    }
}