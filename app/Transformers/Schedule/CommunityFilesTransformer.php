<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\CitizenFile;
use IDRDApp\Entities\Schedule\CommunityFile;
use League\Fractal\TransformerAbstract;

class CommunityFilesTransformer extends TransformerAbstract
{
    public function transform( CommunityFile $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'citizen_id'    =>  isset( $data->community_id ) ? (int) $data->community_id : null,
            'file_name'     =>  isset( $data->file_name ) ? $data->file_name : null,
            'path'          =>  isset( $data->path ) ? $data->getOriginal('path') : null,
            'src'           =>  isset( $data->path ) ? $data->path : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}