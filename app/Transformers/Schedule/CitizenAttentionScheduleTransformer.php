<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\CitizenAttention;
use IDRDApp\Entities\Schedule\CitizenAttentionSchedule;
use League\Fractal\TransformerAbstract;

class CitizenAttentionScheduleTransformer extends TransformerAbstract
{
    /**
     * @param CitizenAttentionSchedule $data
     * @return array
     */
    public function transform(CitizenAttentionSchedule $data )
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'schedule'      =>  isset( $data->schedule ) ? $data->schedule : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}