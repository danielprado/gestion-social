<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\CitizenAttention;
use IDRDApp\Entities\Schedule\CitizenAttentionView;
use IDRDApp\Entities\Schedule\CommunityAttention;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class CommunityAttentionTransformer extends TransformerAbstract
{
    /**
     * @param CommunityAttention $data
     * @return array
     */
    public function transform(CommunityAttention $data )
    {

        $initial_hour = isset( $data->initial_hour ) ? substr($data->initial_hour, 0, 5) : null;
        $final_hour   = isset( $data->final_hour ) ? substr($data->final_hour, 0, 5) : null;

        return [
            'id'                =>  isset( $data->id ) ? (int) $data->id : null,
            'color'             => 'info',
            'place'             => 'IDRD - Calle 63 # 59A-06',
            'execution_date'    =>  isset( $data->execution_date ) ? $data->execution_date->format('Y-m-d') : null,
            'date'              =>  isset( $data->execution_date ) ? $data->execution_date->format('Y-m-d') : null,
            'start'             =>  $initial_hour,
            'end'               =>  $final_hour,
            'initial_hour'      =>  $initial_hour,
            'final_hour'        =>  $final_hour,
            'professional'      =>  isset( $data->user_id ) ? (int) $data->user_id : null,
            'professional_name' =>  isset( $data->professional->part_name ) ? $data->professional->part_name : null,
            'files'             =>  isset( $data->files ) ? $this->includeFiles($data) : [],
            'created_at'        =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'        =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'deleted_at'        =>  isset( $data->deleted_at ) ? $data->deleted_at->format('Y-m-d H:i:s') : null,
        ];
    }


    public function includeFiles( CommunityAttention $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->files, new CommunityFilesTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }
}