<?php


namespace IDRDApp\Transformers\Schedule;


use Carbon\Carbon;
use IDRDApp\Entities\Parks\Location;
use IDRDApp\Entities\Parks\Park;
use IDRDApp\Entities\Parks\Upz;
use IDRDApp\Entities\Schedule\ProgrammingCommitment;
use IDRDApp\Entities\Schedule\ProgrammingFile;
use IDRDApp\Entities\Schedule\ProgrammingImage;
use IDRDApp\Entities\Schedule\ProgrammingView;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;

class CommitmentExcelTransformer extends TransformerAbstract
{
    public function transform_(ProgrammingCommitment $data)
    {
        $park = isset( $data->programing->park_id) ? $this->getParkName( $data->programing->park_id) : null;
        $alternative_park_name = isset( $data->programing->park) ? $data->programing->park : null;
        $activities = isset( $data->programing->programming_activities ) ? $data->programing->programming_activities->toArray() : [];
        $activities = collect( $activities )->implode('activity', ', ');
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'park_id'       =>  isset( $data->programing->park_id) ? $data->programing->park_id : null,
            'park_code'     =>  isset( $data->programing->park_code) ? $data->programing->park_code : null,
            'park'          =>  isset($park) ? $park : $alternative_park_name,
            'location_id'   =>  isset( $data->programing->location_id ) ? (int) $data->programing->location_id : null,
            'location'      =>  isset( $data->programing->location_id ) ? $this->getLocation( $data->programing->location_id ) : null,
            'upz_id'        =>  isset( $data->programing->upz_id) ? $data->programing->upz_id : null,
            'upz'           =>  isset( $data->programing->upz) ? $data->programing->upz : null,
            'programming_date'     =>  isset( $data->programing->execution_date) ? $data->programing->execution_date : null,
            'programming_desc'     =>  isset( $data->programing->objective) ? $data->programing->objective : null,
            'date'          =>  isset( $data->date ) ? $data->date->format('Y-m-d') : null,
            'programming_act'     =>  $activities,
            'status'        => isset( $data->date) ? $this->greaterThanOneYear( $data->date ) : null,
            'responsable'   =>  isset( $data->responsable ) ? $data->responsable : null,
            'description'   =>  isset( $data->description ) ? $data->description : null,
            'professional'  => isset( $data->programing->user_id) ? $data->programing->user_id : null,
            'professional_name' => isset( $data->programing->full_name) ? $data->programing->full_name : null,
            'tracings'      =>  isset( $data->tracings )   ? $this->includeTracing( $data ) : [],
            'programming_id'=>  isset($data->programming_id) ? $data->programming_id : null,
            'files'         =>  isset( $data->programing) ? $this->getFiles( $data->programing ) : null,
            'images'         =>  isset( $data->programing) ? $this->getImages( $data->programing ) : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function transform(ProgrammingView $data)
    {
        $park = isset( $data->park_id) ? $this->getParkName( $data->park_id) : null;
        $alternative_park_name = isset( $data->park) ? $data->park : null;
        $activities = isset( $data->programming_activities ) ? $data->programming_activities->toArray() : [];
        $activities = collect( $activities )->implode('activity', ', ');
        $programming =  [
            'id'                =>  isset($data->id) ? $data->id : null,
            'park_id'           =>  isset( $data->park_id) ? $data->park_id : null,
            'park_code'         =>  isset( $data->park_code) ? $data->park_code : null,
            'park'              =>  isset($park) ? $park : $alternative_park_name,
            'location_id'       =>  isset( $data->location_id ) ? (int) $data->location_id : null,
            'location'          =>  isset( $data->location_id ) ? $this->getLocation( $data->location_id ) : null,
            'upz_id'            =>  isset( $data->upz_id) ? $data->upz_id : null,
            'upz'               =>  isset( $data->upz) ? $data->upz : null,
            'programming_date'  =>  isset( $data->execution_date) ? $data->execution_date->format('Y-m-d') : null,
            'programming_desc'  =>  isset( $data->objective) ? $data->objective : null,
            'programming_act'   =>  $activities,
            'professional'      => isset( $data->user_id) ? $data->user_id : null,
            'professional_name' => isset( $data->full_name) ? $data->full_name : null,
            'status'            => isset( $data->execution_date) ? $this->greaterThanOneYear( $data->execution_date ) : null,
            'files'             =>  isset( $data->programming_files ) ? $this->getFiles( $data->programming_files ) : null,
            'images'            =>  isset( $data->programming_images ) ? $this->getImages( $data->programming_images ) : null,
            'commitment_id'     => '',
            'date'              => '',
            'responsable'       => '',
            'description'       => '',
            'tracings'          => [],
            'created_at'        => '',
            'updated_at'        => '',
        ];
        $array = [];

        foreach ( $data->programming_commitments as $commitment ) {
            $array[] = array_merge( $programming, [
                'commitment_id' =>  isset( $commitment->id ) ? (int) $commitment->id : null,
                'date'          =>  isset( $commitment->date ) ? $commitment->date->format('Y-m-d') : null,
                'responsable'   =>  isset( $commitment->responsable ) ? $commitment->responsable : null,
                'description'   =>  isset( $commitment->description ) ? $commitment->description : null,
                'tracings'      =>  isset( $commitment->tracings )   ? $this->includeTracing( $commitment ) : [],
                'created_at'    =>  isset( $commitment->created_at ) ? $commitment->created_at->format('Y-m-d H:i:s') : null,
                'updated_at'    =>  isset( $commitment->updated_at ) ? $commitment->updated_at->format('Y-m-d H:i:s') : null,
            ]);
        }

        return count( $array ) > 0 ? $array : [$programming];
    }

    public function includeTracing( ProgrammingCommitment $data )
    {
        $manager = new Manager();
        $resource = $this->collection( $data->tracings, new TracingTransformer() );
        return $manager->createData( $resource )->toArray()['data'];
    }

    public function getLocation($id = null)
    {
        if (isset($id)) {
            $location = Location::query()->where('Id_Localidad', $id)->first();
            return isset($location->Localidad) ? toUpper( $location->Localidad ) : null;
        }
        return null;
    }

    public function getParkName($id = null)
    {
        if (isset($id)) {
            $park = Park::query()->where('Id', $id)->first();
            return isset($park->Nombre) ? toUpper($park->Nombre) : null;
        }
        return null;
    }

    public function greaterThanOneYear($date)
    {
        return Carbon::parse($date)->age >= 1 ? 'VENCIDO' : 'VIGENTE';
    }

    public function getFiles($files)
    {
        $documents = [];
        foreach ( $files as $programming_file ) {
            $file   = isset( $programming_file->pivot->path ) ? asset( 'public'.ProgrammingFile::FILE_PATH.'/'.$programming_file->pivot->path ) : null;
            if ( $file ) {
                $documents[] = $file;
            }
        }
        return implode(', ', $documents);
    }

    public function getImages($images)
    {
        $documents = [];
        foreach ( $images as $programming_image ) {
            $path = $programming_image->getOriginal('path');
            if ( $path ) {
                $file = asset( 'public'.ProgrammingImage::IMAGE_PATH.'/'.$programming_image->getOriginal('path') );
                $documents[] = $file;
            }
        }
        return implode(', ', $documents);
    }
}