<?php


namespace IDRDApp\Transformers\Schedule;


use IDRDApp\Entities\Schedule\ReunionType;
use League\Fractal\TransformerAbstract;

class ReunionTypeTransformer extends TransformerAbstract
{
    public function transform(ReunionType $data)
    {
        return [
            'id'            =>  isset( $data->id ) ? (int) $data->id : null,
            'name'          =>  isset( $data->reunion_type ) ? $data->reunion_type : null,
            'require_process' =>  isset( $data->require_process ) ? (boolean) $data->require_process : null,
            'created_at'    =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $data->updated_at ) ? $data->updated_at->format('Y-m-d H:i:s') : null,
            'deleted_at'    =>  isset( $data->deleted_at ) ? $data->deleted_at->format('Y-m-d H:i:s') : null,
        ];
    }
}