<?php


namespace IDRDApp\Transformers\Excel;


use IDRDApp\Entities\Schedule\Execution;
use IDRDApp\Entities\Schedule\Process;
use IDRDApp\Entities\Schedule\ProgrammingFile;
use IDRDApp\Entities\Schedule\ProgrammingImage;
use IDRDApp\Entities\Schedule\ProgrammingView;
use IDRDApp\Transformers\Schedule\ExecutionTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class ProgrammingExcelTransformer extends TransformerAbstract
{
    public function transform(ProgrammingView $data)
    {
        $activities = isset(  $data->programming_activities ) ?  $data->programming_activities : [];
        $park_code = isset(  $data->park_code ) ?  $data->park_code : null;

        $program = [
            'id'                             =>  isset( $data->id ) ? (int) $data->id : null,
            'execution_date'                 =>  isset( $data->execution_date ) ? $data->execution_date->format('Y-m-d') : null,
            'start'                          =>  isset( $data->initial_hour ) ? substr($data->initial_hour, 0, 5) : null,
            'end'                            =>  isset( $data->final_hour ) ? substr($data->final_hour, 0, 5) : null,
            'reunion_type'                   =>  isset( $data->reunion_type ) ? $data->reunion_type : null,
            'professional_name'              =>  isset( $data->full_name ) ? $data->full_name : null,
            'park_code'                      =>  isset( $data->park_code ) ?  $data->park_code : null,
            'park'                           =>  isset( $data->park ) ?  $data->park : null,
            'upz'                            =>  isset( $data->upz ) ?  $data->upz : null,
            'process'                        =>  isset( $data->process ) ? $data->process : null,
            'activities'                     =>  isset( $data->programming_activities ) ?  $data->programming_activities->implode('activity', ', ') : '',
            'themes'                         =>  isset( $data->programming_themes ) ?  $data->programming_themes->implode('theme', ', ') : '',
            'other_themes'                   =>  isset( $data->programming_themes ) ? trim( $data->programming_themes->filter(function ($value, $key) {
                return isset( $value->pivot->other );
            })->implode('pivot.other', ', ') )
                : '',
            'who_summons'                    =>  isset( $data->who_summons ) ? $data->who_summons : null,
            'which'                          =>  isset( $data->which ) ? $data->which : null,
            'request'                        =>  isset( $data->request ) ? $data->request : null,
            'objective'                      =>  isset( $data->objective ) ?  $data->objective : null,
            'deleted_at'                     =>  isset( $data->deleted_at ) ? $data->deleted_at : null,
            'who_cancel'                     =>  isset( $data->who_cancel ) ? $data->who_cancel : null,
            'reason_for_cancellation'        =>  isset( $data->reason_for_cancellation ) ? $data->reason_for_cancellation : null,
            'reprogramming'                  =>  isset( $data->reprogrammings ) ? $data->reprogrammings->implode('reason', ', ') : '',
            'is_covenant'                    =>  isCovenant($park_code, $activities)  ? 'PACTOS 2018' : null,
            'is_agreement'                   =>  isAgreement($park_code, $activities) ? 'ACUERDOS 2018' : null,
            'is_work_table'                  =>  isWorkTable($park_code, $activities) ? 'MESAS 2018' : null,
            'created_at'                     =>  isset( $data->created_at ) ? $data->created_at->format('Y-m-d H:i:s') : null,
            'is_executed'                    => $this->isExecuted( $data )

        ];

        return array_merge( $program, $this->concatFiles( $data ), $this->concatImages( $data ), $this->concatCommitments(  $data ), $this->concatExecution(  $data ) );
    }


    public function concatFiles( ProgrammingView $data )
    {
        $types = '';
        $names = '';
        $created_at = '';
        if ( isset( $data->programming_files ) ) {
            foreach ( $data->programming_files as $programming_file ) {
                $path = isset( $programming_file->pivot->path ) ? $programming_file->pivot->path : 'not_found.png';
                $file   = file_exists( public_path(ProgrammingFile::FILE_PATH.'/'.$path ) )
                    ? asset( 'public'.ProgrammingFile::FILE_PATH.'/'.$path )
                    : '';

                $file_id   = isset( $programming_file->id ) ? $programming_file->id : '';
                $file_type = isset( $programming_file->file_type ) ? $programming_file->file_type : '';
                $file_name = $file;
                $file_date = isset( $programming_file->created_at ) ? $programming_file->created_at : '';

                $types.= "({$file_id}) - {$file_type}\n";
                $names.= "({$file_id}) - {$file_name}\n";
                $created_at.= "({$file_id}) - {$file_date}\n";
            }
        }

        return [
            'file_types' =>  $types,
            'file_names' =>  $names,
            'file_dates' =>  $created_at,
        ];
    }

    public function concatImages( ProgrammingView $data )
    {
        $names = '';
        $descriptions = '';
        $created_at = '';
        if ( isset( $data->programming_images ) ) {
            foreach ( $data->programming_images as $programming_image ) {
                $file   = file_exists( public_path(ProgrammingImage::IMAGE_PATH.'/'.$programming_image->getOriginal('path') ) )
                    ? asset( 'public'.ProgrammingImage::IMAGE_PATH.'/'.$programming_image->getOriginal('path') )
                    : '';

                $file_description = isset( $programming_image->description ) ? $programming_image->description : '';
                $file_id   = isset( $programming_image->id ) ? $programming_image->id : '';
                $file_name = $file;
                $file_date = isset( $programming_image->created_at ) ? $programming_image->created_at : '';

                $names.= "({$file_id}) - {$file_name}\n";
                $descriptions.= "({$file_id}) - {$file_description}\n";
                $created_at.= "({$file_id}) - {$file_date}\n";
            }
        }

        return [
            'image_names' =>  $names,
            'image_descriptions' => $descriptions,
            'image_dates' =>  $created_at,
        ];
    }

    public function concatCommitments( ProgrammingView $data )
    {

        $commitment_responsables = '';
        $commitment_descriptions = '';
        $commitment_dates = '';
        $tracings_descriptions = '';
        $tracings_dates = '';
        $commitments_created_at = '';
        if ( isset( $data->programming_commitments ) ) {
            foreach ($data->programming_commitments as $commitment) {

                $commitment_id = isset( $commitment->id ) ? $commitment->id : null;
                $commitment_responsable = isset( $commitment->responsable ) ? $commitment->responsable : null;
                $commitment_description = isset( $commitment->description ) ? $commitment->description : null;
                $commitment_date = isset( $commitment->date ) ? $commitment->date : null;
                $commitment_created_at = isset( $commitment->created_at ) ? $commitment->created_at : null;

                $commitment_responsables .= "({$commitment_id}) - {$commitment_responsable}\n";
                $commitment_descriptions .= "({$commitment_id}) - {$commitment_description}\n";
                $commitment_dates .= "({$commitment_id}) - {$commitment_date}\n";
                $commitments_created_at .= "({$commitment_id}) - {$commitment_created_at}\n";

                if ( isset( $commitment->tracings ) ) {
                    foreach ($commitment->tracings as $tracing) {
                        $tracing_id = isset( $tracing->id ) ?  $tracing->id : null;
                        $tracing_descriptions = isset( $tracing->description ) ?  $tracing->description : null;
                        $tracing_dates = isset( $tracing->date ) ?  $tracing->date : null;

                        $tracings_descriptions .= "({$tracing_id}) - {$tracing_descriptions}\n";
                        $tracings_dates .= "({$tracing_id}) - {$tracing_dates}\n";
                    }
                }
            }
        }

        return [
            'responsable'   =>  $commitment_responsables,
            'description'   =>  $commitment_descriptions,
            'dates'         =>  $commitment_dates,
            'tracing'       =>  $tracings_descriptions,
            'tracing_description'   =>  $tracings_dates,
            'commit_created_at'     =>  $commitments_created_at
        ];
    }

    public function concatExecution( ProgrammingView $programming )
    {
        $executions = Execution::query()->where('programming_id', $programming->id)
            ->select('*')
            ->addSelect( DB::raw('( SUM(f_0_5) + SUM(m_0_5) + SUM(f_6_12) + SUM(m_6_12) + SUM(f_13_17) + SUM(m_13_17) + SUM(f_18_26) + SUM(m_18_26) + SUM(f_27_59) + SUM(m_27_59) + SUM(f_60_more) + SUM(m_60_more) ) as subtotal') )
            ->groupBy('id')->get();

        $entities = '';
        $populations = '';
        $conditions = '';
        $situations = '';
        $f_0_5 = '';
        $m_0_5 = '';
        $f_6_12 = '';
        $m_6_12 = '';
        $f_13_17 = '';
        $m_13_17 = '';
        $f_18_26 = '';
        $m_18_26 = '';
        $f_27_59 = '';
        $m_27_59 = '';
        $f_60 = '';
        $m_60 = '';
        $subtotal = '';
        $observations = '';
        $total = 0;

        foreach ( $executions as $data ) {
            $execution_id = isset( $data->id ) ? $data->id : null;
            $entity = isset($data->entity->entity) ? $data->entity->entity : null;
            $population = isset($data->population->type_of_population) ? $data->population->type_of_population : null;
            $condition = isset($data->condition->condition) ? $data->condition->condition : null;
            $situation = isset($data->situation->situation) ? $data->situation->situation : null;
            $f_0_5_sum = isset( $data->{ 'f_0_5' } ) ? (int) $data->{ 'f_0_5' } : 0;
            $m_0_5_sum = isset( $data->{ 'm_0_5' } ) ? (int) $data->{ 'm_0_5' } : 0;
            $f_6_12_sum = isset( $data->{ 'f_6_12' } ) ? (int) $data->{ 'f_6_12' } : 0;
            $m_6_12_sum = isset( $data->{ 'm_6_12' } ) ? (int) $data->{ 'm_6_12' } : 0;
            $f_13_17_sum = isset( $data->{ 'f_13_17' } ) ? (int) $data->{ 'f_13_17' } : 0;
            $m_13_17_sum = isset( $data->{ 'm_13_17' } ) ? (int) $data->{ 'm_13_17' } : 0;
            $f_18_26_sum = isset( $data->{ 'f_18_26' } ) ? (int) $data->{ 'f_18_26' } : 0;
            $m_18_26_sum = isset( $data->{ 'm_18_26' } ) ? (int) $data->{ 'm_18_26' } : 0;
            $f_27_59_sum = isset( $data->{ 'f_27_59' } ) ? (int) $data->{ 'f_27_59' } : 0;
            $m_27_59_sum = isset( $data->{ 'm_27_59' } ) ? (int) $data->{ 'm_27_59' } : 0;
            $f_60_sum = isset( $data->{ 'f_60_more' } ) ? (int) $data->{ 'f_60_more' } : 0;
            $m_60_sum = isset( $data->{ 'm_60_more' } ) ? (int) $data->{ 'm_60_more' } : 0;
            $observation = isset( $data->{ 'observation' } ) ?  $data->{ 'observation' } : null;
            $sub = isset( $data->subtotal ) ? (int) $data->subtotal : 0;
            $total = $total + $sub;

            $entities.= "({$execution_id}) - {$entity}\n";
            $populations.= "({$execution_id}) - {$population}\n";
            $conditions.= "({$execution_id}) - {$condition}\n";
            $situations.= "({$execution_id}) - {$situation}\n";
            $f_0_5.= "({$execution_id}) - CANT: ( {$f_0_5_sum} )\n";
            $m_0_5.= "({$execution_id}) - CANT: ( {$m_0_5_sum} )\n";
            $f_6_12.= "({$execution_id}) - CANT: ( {$f_6_12_sum} )\n";
            $m_6_12.= "({$execution_id}) - CANT: ( {$m_6_12_sum} )\n";
            $f_13_17.= "({$execution_id}) - CANT: ( {$f_13_17_sum} )\n";
            $m_13_17.= "({$execution_id}) - CANT: ( {$m_13_17_sum} )\n";
            $f_18_26.= "({$execution_id}) - CANT: ( {$f_18_26_sum} )\n";
            $m_18_26.= "({$execution_id}) - CANT: ( {$m_18_26_sum} )\n";
            $f_27_59.= "({$execution_id}) - CANT: ( {$f_27_59_sum} )\n";
            $m_27_59.= "({$execution_id}) - CANT: ( {$m_27_59_sum} )\n";
            $f_60.= "({$execution_id}) - CANT: ( {$f_60_sum} )\n";
            $m_60.= "({$execution_id}) - CANT: ( {$m_60_sum} )\n";
            $subtotal.= "({$execution_id}) - CANT: ( {$sub} )\n";
            $observations.= "({$execution_id}) -  {$observation}\n";
        }

        return [
            'entities'  => $entities,
            'populations'   => $populations,
            'conditions'    => $conditions,
            'situations'    => $situations,
            'f_0_5' => $f_0_5,
            'm_0_5' => $m_0_5,
            'f_6_12'    => $f_6_12,
            'm_6_12'    => $m_6_12,
            'f_13_17'   => $f_13_17,
            'm_13_17'   => $m_13_17,
            'f_18_26'   => $f_18_26,
            'm_18_26'   => $m_18_26,
            'f_27_59'   => $f_27_59,
            'm_27_59'   => $m_27_59,
            'f_60'  => $f_60,
            'm_60'  => $m_60,
            'exec_observations'  => $observations,
            'subtotal'  => $subtotal,
            'total' => $total
        ];
    }

    public function includeHasThemes( $id = null )
    {
        if ( isset( $id ) ) {
            $process = Process::query()->where('process', 'SOSTENIBILIDAD SOCIAL')
                ->where('id', $id)->first();
            return isset( $process->id ) ? true : false;
        }
        return false;
    }
                                      
    public function isExecuted( ProgrammingView $programming )
    {
        if ( ($programming->execution()->count() > 0 || ! (boolean) $programming->include_attendance ) &&
            ($programming->programming_files()->count() > 0  || ! (boolean) $programming->include_files )  &&
            ($programming->programming_images()->count() > 0 || ! (boolean) $programming->include_images)  &&
            ($programming->programming_commitments()->count() > 0 || ! (boolean) $programming->include_commitments)  ) {

            if ( isset( $programming->reunion_type->require_process )  && $programming->reunion_type->require_process ) {
                if ( $programming->programming_activities()->count() > 0 ) {
                    return true;
                }
                return false;
            }

            /*
            if ( $this->includeHasThemes( $programming->process_id ) ) {
                if ( $programming->programming_themes()->count() > 0 ) {
                    return true;
                }
                return false;
            }
            */

            return true;
        } else {
            return false;
        }
    }

}