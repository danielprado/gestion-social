<?php


namespace IDRDApp\Transformers\Stats;


use IDRDApp\Entities\Schedule\ProgrammingView;
use League\Fractal\TransformerAbstract;

class ParksStatsTransformer extends TransformerAbstract
{
    public function transform(ProgrammingView $data)
    {
        return [
            'park_id'       => isset( $data->park_id ) ? (int) $data->park_id : null,
            'upz'           => isset( $data->upz ) ? $data->upz : null,
            'park_code'     => isset( $data->park_code ) ? $data->park_code : null,
            'park'          => isset( $data->park ) ? $data->park : null,
            'total_programmed'    => isset( $data->total_programmed ) ? (int) $data->total_programmed : null,
        ];
    }
}