<?php

namespace IDRDApp\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Oauth2Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'oauth2:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate client_id and secret string for api authentication';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $client_id = sha1( Str::random( 40 ) );
        $client_secret = sha1( Str::random( 40 ) );
        $name = env('APP_NAME', 'Laravel Application');

        DB::table('oauth_clients')->insert(
            [
                'id' => $client_id,
                'secret' => $client_secret,
                'name' => $name,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        );
        $this->line('Creating Client');
        $this->info("Client ID: {$client_id}");
        $this->info("Client Secret: {$client_secret}");
        $this->info("Name: {$name}");
    }
}
