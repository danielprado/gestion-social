<?php

namespace IDRDApp\Http\Requests\Auth;

use IDRDApp\Http\Requests\Request;

class AccessTokenRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'      =>  'required|size:40',
            'client_secret'  =>  'required|size:40',
            'grant_type'     =>  'required|alpha',
            'username'       =>  'required',
            'password'       =>  'required'
        ];
    }
}
