<?php

namespace IDRDApp\Http\Requests;

use IDRDApp\Http\Requests\Request;

class StoreProgrammingCommitmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'responsable'   =>  'required|min:3|max:191',
            'date'          =>  'required|date|date_format:Y-m-d',
            'description'   =>  'required|min:3|max:2500'
        ];
    }
}
