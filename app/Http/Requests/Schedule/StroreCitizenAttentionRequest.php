<?php

namespace IDRDApp\Http\Requests\Schedule;

use IDRDApp\Http\Requests\Request;

class StroreCitizenAttentionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'          =>  'required|date|date_format:Y-m-d|citizen_attention_exist',
            'working_day'   =>  'required|exists:citizen_attention_schedules,id'
        ];
    }
}
