<?php

namespace IDRDApp\Http\Requests\Schedule;

use IDRDApp\Http\Requests\Request;

class StoreCommunityAttentionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'          =>  'required|date|date_format:Y-m-d',
            'initial_hour'  =>  'required',
            'final_hour'    =>  'required',
        ];
    }
}
