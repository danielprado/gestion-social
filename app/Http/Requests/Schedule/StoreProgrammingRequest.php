<?php

namespace IDRDApp\Http\Requests\Schedule;

use IDRDApp\Http\Requests\Request;

class StoreProgrammingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'min:3|max:191',
            'date'          => 'required|date|date_format:Y-m-d|schedule_programming:'.request()->get('initial_hour').','.request()->get('final_hour'),
            'initial_hour'  => 'required|min:5|max:5',
            'final_hour'    => 'required|min:5|max:5',
            'objective'     => 'required|min:3|max:2500',
            'reunion_type_id' => 'required|numeric|exists:reunion_types,id',
            'park_id'       => 'numeric|exists:mysql_parks.parque,Id',
            'park'          => 'required|min:3|max:191',
            'upz_id'        => 'numeric|exists:mysql_parks.upz,cod_upz',
            'upz'           => 'required|min:3|max:80',
            'place'         => 'required|min:3|max:80',
            'process_id'    => 'numeric|exists:processes,id|is_require_process:reunion_type_id,'.request()->get('reunion_type_id'),
            'who_summons'   => 'required|numeric|exists:summoners,id',
            'which'         => 'string|min:3|max:80|is_require_which:'.request()->get('who_summons'),
            'request_type_id' => 'required|numeric|exists:request_types,id'
        ];
    }
}
