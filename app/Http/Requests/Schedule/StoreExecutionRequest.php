<?php

namespace IDRDApp\Http\Requests\Schedule;

use IDRDApp\Http\Requests\Request;

class StoreExecutionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'situation_id'              =>      'required|exists:situations,id',
            'entity_id'                 =>      'required|exists:entities,id',
            'type_of_population_id'     =>      'required|exists:types_of_populations,id',
            'condition_id'              =>      'required|exists:conditions,id',
            'f_0_5'                     =>      'integer|between:0,2000',
            'm_0_5'                     =>      'integer|between:0,2000',
            'f_6_12'                    =>      'integer|between:0,2000',
            'm_6_12'                    =>      'integer|between:0,2000',
            'f_13_17'                   =>      'integer|between:0,2000',
            'm_13_17'                   =>      'integer|between:0,2000',
            'f_18_26'                   =>      'integer|between:0,2000',
            'm_18_26'                   =>      'integer|between:0,2000',
            'f_27_59'                   =>      'integer|between:0,2000',
            'm_27_59'                   =>      'integer|between:0,2000',
            'f_60_more'                 =>      'integer|between:0,2000',
            'm_60_more'                 =>      'integer|between:0,2000',
            'observation'               =>      'min:3|max:2500'
        ];
    }
}
