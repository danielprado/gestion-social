<?php

namespace IDRDApp\Http\Requests\Schedule;

use IDRDApp\Http\Requests\Request;

class UpdateProgrammingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date|date_format:Y-m-d|schedule_programming:'.request()->get('initial_hour').','.request()->get('final_hour').','.$this->route('programming')->id,
            'initial_hour' => 'required|min:5|max:5',
            'final_hour' => 'required|min:5|max:5',
            'reprogramming_reason'        =>  'required|min:3|max:2500'
        ];
    }
}
