<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::any('/', function () {
    return view('welcome');
})->name('welcome');
Route::get('excel', 'Schedule\FollowUpOnCommitmentController@excel');

Route::post('oauth/access-token', 'Auth\AccessTokenController@index')->middleware('api')->name('oauth2-access-token');
Route::post('logout', 'Auth\AccessTokenController@logout')->middleware(['auth', 'oauth', 'api'])->name('logout');
Route::post('lock', 'Auth\AccessTokenController@lock')->middleware(['auth', 'oauth', 'api'])->name('lock');
Route::post('unlock', 'Auth\AccessTokenController@unlock')->middleware(['oauth', 'api'])->name('unlock');

Route::group(['prefix' =>   'api', 'middleware' => ['api']], function () {
    Route::get('summary', 'Reports\SummaryController@index');
    Route::get('parks', 'Park\ParkController@index');

    Route::get('programmings/{programming}/force-destroy', 'Schedule\ProgrammingController@destroy');

    Route::group(['middleware' => ['auth', 'api']], function () {
        Route::get('programmed-parks', 'Stats\ProgrammingStatsController@doublePark');
        Route::get('counters', 'Stats\ProgrammingStatsController@counters');
        Route::get('stats', 'Stats\ProgrammingStatsController@covenantsAgreementsTables');
        Route::get('total', 'Stats\ProgrammingStatsController@total');


        Route::get('programmings/{programming}/excel', 'Excel\ProgrammingExcelController@index');
        Route::post('programmings/general-report', 'Excel\ProgrammingExcelController@general');
        Route::post('attendance/general-report', 'Excel\ProgrammingExcelController@attendance');
        Route::post('community/general-report', 'Excel\ProgrammingExcelController@community');
        Route::post('programmings/by-professionals', 'Excel\ProgrammingExcelController@byProfessional');

    });

    //Auth routes
    Route::group(['middleware' => ['auth', 'oauth', 'api']], function () {
        Route::get('user', 'Security\AuthController@index')->name('auth.user');
        Route::get('professionals', 'Professional\ProfessionalController@index')->name('professionals');
        Route::group(['namespace' => 'Schedule'], function () {
            Route::get('activities/{process}', 'ActivityController@index');
            Route::resource('conditions', 'ConditionController', [
                'only'       =>   ['index'],
                'parameters' => [ 'conditions' => 'condition' ]
            ]);
            Route::resource('situations', 'SituationController', [
                'only'       =>   ['index'],
                'parameters' => [ 'situations' => 'situation' ]
            ]);
            Route::post('follow-up-on-commitments/excel', 'FollowUpOnCommitmentController@excel');
            Route::resource('follow-up-on-commitments', 'FollowUpOnCommitmentController', [
                'only'       =>   ['index', 'store', 'destroy'],
                'parameters' => [ 'follow-up-on-commitments' => 'commitment' ]
            ]);
            Route::resource('follow-up-on-commitments.tracings', 'FollowUpOnCommitmentTracingController', [
                'only'       =>   ['index', 'store', 'destroy'],
                'parameters' => [ 'follow-up-on-commitments' => 'commitment', 'tracings' => 'tracing' ]
            ]);
            Route::resource('entities', 'EntityController', [
                'only'       =>   ['index'],
                'parameters' => [ 'entities' => 'entity' ]
            ]);
            Route::resource('types-of-populations', 'TypeOfPopulationController', [
                'only'       =>   ['index'],
                'parameters' => [ 'types-of-populations' => 'type-of-population' ]
            ]);

            Route::resource('themes', 'ThemeController', [
                'only'       =>   ['index'],
                'parameters' => [ 'themes' => 'theme' ]
            ]);
            Route::resource('processes', 'ProcessController', [
                'only'       =>   ['index'],
                'parameters' => [ 'processes' => 'process' ]
            ]);
            Route::resource('request-types', 'RequestTypeController', [
                'only'       =>   ['index'],
                'parameters' => [ 'request-types' => 'request_type' ]
            ]);
            Route::resource('reunion-types', 'ReunionTypeController', [
                'only'       =>   ['index'],
                'parameters' => [ 'reunion-types' => 'reunion_type' ]
            ]);
            Route::resource('summoners', 'SummonerController', [
                'only'       =>   ['index'],
                'parameters' => [ 'summoners' => 'summoner' ]
            ]);
            Route::resource('file-types', 'FileTypeController', [
                'only'       =>   ['index'],
                'parameters' => [ 'file-types' => 'file_type' ]
            ]);
            Route::resource('programmings.executions', 'ExecutionController', [
                'only'       =>   ['index', 'store', 'destroy'],
                'parameters' => [ 'programmings' => 'programming', 'executions' => 'execution' ]
            ]);
            Route::resource('programmings.files', 'ProgrammingFileController', [
                'only'       =>   ['index', 'store', 'destroy'],
                'parameters' => [ 'programmings' => 'programming', 'files' => 'file' ]
            ]);
            Route::resource('programmings.images', 'ProgrammingImageController', [
                'only'       =>   ['index', 'store', 'destroy'],
                'parameters' => [ 'programmings' => 'programming', 'images' => 'image' ]
            ]);
            Route::resource('programmings.commitments', 'ProgrammingCommitmentController', [
                'only'       =>   ['index', 'store', 'destroy'],
                'parameters' => [ 'programmings' => 'programming', 'commitments' => 'commitment' ]
            ]);
            Route::resource('programmings.commitments.tracings', 'ProgrammingCommitmentTracingController', [
                'only'       =>   ['store', 'destroy'],
                'parameters' => [ 'programmings' => 'programming', 'commitments' => 'commitment', 'tracings' => 'tracing' ]
            ]);


            Route::resource('citizens.files', 'CitizenFileController', [
                'only'       =>   ['index', 'store', 'destroy'],
                'parameters' => [ 'citizens' => 'citizen', 'files' => 'file' ]
            ]);

            Route::get('citizen-attention-schedules', 'CitizenAttentionScheduleController@index');
            Route::get('citizen-attentions/calendar', 'CitizenAttentionController@calendar');
            Route::post('citizen-attentions/{attention}/cancel', 'CitizenAttentionController@cancel');
            Route::resource('citizen-attentions', 'CitizenAttentionController', [
                'except'   =>   ['edit', 'create', 'destroy'],
                'parameters' => [ 'citizen-attentions' => 'attention' ]
            ]);
            Route::get('programmings/calendar', 'ProgrammingController@calendar');
            Route::get('commitments/calendar', 'ProgrammingCommitmentController@calendar');
            Route::post('programmings/{programming}/cancel', 'ProgrammingController@cancel');
            Route::post('programmings/{programming}/details', 'ProgrammingController@details');
            Route::resource('programmings', 'ProgrammingController', [
                'except'   =>   ['edit', 'create', 'destroy'],
                'parameters' => [ 'programmings' => 'programming' ]
            ]);

            Route::get('community-attention-schedules', 'CommunityAttentionController@index');
            Route::get('community-attentions/calendar', 'CommunityAttentionController@calendar');
            Route::resource('community-attentions', 'CommunityAttentionController', [
                'except'   =>   ['edit', 'create',],
                'parameters' => [ 'community-attentions' => 'attention' ]
            ]);

            Route::resource('community.files', 'CommunityAttentionFileController', [
                'only'       =>   ['index', 'store', 'destroy'],
                'parameters' => [ 'community' => 'community', 'files' => 'file' ]
            ]);
        });
    });
});

