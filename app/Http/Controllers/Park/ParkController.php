<?php

namespace IDRDApp\Http\Controllers\Park;

use IDRDApp\Entities\Parks\Park;
use IDRDApp\Transformers\Park\ParkTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ParkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ParkTransformer
     */
    public function index()
    {
        if ( $this->query ) {
            $parks = Park::query()
                ->select( ['Id', 'Id_IDRD', 'Nombre', 'Direccion', 'Upz'] )
                ->where(function ($query) {
                    return $query
                        ->where('Id_IDRD', 'LIKE', "%{$this->query}%")
                        ->orWhere('Nombre', 'LIKE', "%{$this->query}%")
                        ->orWhere('Direccion', 'LIKE', "%{$this->query}%");
                })
                ->where( 'Id', '!=', 1 )
                ->get();

            $resource = new Collection($parks, new ParkTransformer());
            $manager = new Manager();
            $rootScope = $manager->createData($resource);
            return response()->json( $rootScope->toArray(), 200);
        }

        return response()->json( [], 200);
    }
}
