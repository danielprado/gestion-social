<?php

namespace IDRDApp\Http\Controllers\Professional;

use IDRDApp\Entities\Security\Permissions;
use IDRDApp\Entities\Security\Profile;
use IDRDApp\Transformers\Schedule\ProfessionalTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ProfessionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ProfessionalTransformer
     */
    public function index()
    {
        $professionals = Profile::query()->whereHas('permissions', function ($query) {
            return $query->whereHas('activities', function ($query) {
                $query->where('Id_Modulo', env('MODULE_ID') );
            });
        })->get();

        $resource = new Collection($professionals, new ProfessionalTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }
}
