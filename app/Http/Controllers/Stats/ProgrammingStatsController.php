<?php

namespace IDRDApp\Http\Controllers\Stats;

use Carbon\Carbon;
use IDRDApp\Entities\Schedule\ProgrammingActivities;
use IDRDApp\Entities\Schedule\ProgrammingCommitment;
use IDRDApp\Entities\Schedule\ProgrammingView;
use IDRDApp\Http\Controllers\Schedule\ProgrammingController;
use IDRDApp\Transformers\Schedule\ProgrammingViewTransformer;
use IDRDApp\Transformers\Stats\ParksStatsTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ProgrammingStatsController extends Controller
{

    /**
     * The start date.
     *
     * @var string
     */
    private $start_date;

    /**
     * The final date.
     *
     * @var string
     */
    private $final_date;

    /**
     * The professionals filter.
     *
     * @var string
     */
    private $professionals;


    public function __construct()
    {
        $this->start_date = request()->has('start_date') ? request()->get('start_date') : Carbon::now()->startOfMonth()->format('Y-m-d');
        $this->final_date = request()->has('final_date') ? request()->get('final_date') : Carbon::now()->endOfMonth()->format('Y-m-d');
        $this->professionals = ( request()->has('professional') && count( request()->get('professional') ) > 0 ) ? request()->get('professional') : [ auth()->user()->id ];
        parent::__construct();
    }

    public function counters()
    {
        return response()->json([
            'data'  =>  [
                'total'         =>  ProgrammingView::whereBetween('execution_date', [ $this->start_date, $this->final_date  ])
                                                    ->whereIn('user_id', $this->professionals)->count(),
                'active'        =>  ProgrammingView::whereBetween('execution_date', [ $this->start_date, $this->final_date  ])
                                                    ->whereIn('user_id', $this->professionals)->whereNull('deleted_at')->count(),
                'cancelled'     =>  ProgrammingView::whereBetween('execution_date', [ $this->start_date, $this->final_date  ])
                                                    ->whereIn('user_id', $this->professionals)->whereNotNull('deleted_at')->count(),
                'commitments'   =>  ProgrammingCommitment::query()->whereHas('programing', function ($query) {
                                                                                return $query->whereNull('deleted_at')
                                                                                             ->whereIn('user_id', $this->professionals);
                                                                             })->count(),
                'tracings'      =>  ProgrammingCommitment::query()->whereHas('programing', function ($query) {
                                                                        return $query->whereNull('deleted_at')
                                                                                     ->whereIn('user_id', $this->professionals);
                                                                    })->whereHas('tracings', function ($query) {
                                                                        return $query;
                                                                    })->count(),
            ],
            'code'  =>  200
        ], 200);
    }

    public function doublePark()
    {
        $programmings = ProgrammingView::whereBetween('execution_date', [ $this->start_date, $this->final_date  ])
                        ->select( 'park_id', 'upz', 'park_code', 'park', DB::raw('COUNT(park_id) as total_programmed') )
                        ->whereIn('user_id', $this->professionals)
                        ->whereNull('deleted_at')
                        ->where('park_id', "!=", 15627) // DISTRITAL PARK
                        ->groupBy('park_id')
                        ->having('total_programmed', '>', 1)
                        ->get();

        $resource = new Collection($programmings, new ParksStatsTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }

    public function covenantsAgreementsTables()
    {
        $agreements  = DB::table('agreements_2018')->get();
        $covenants   = DB::table('covenant_2018')->get();
        $work_table  = DB::table('work_table_2018')->get();

        $programming_with_agreements = ProgrammingActivities::query()
                                            ->whereIn('activity_id', collect( $agreements )->pluck('activity_id')->toArray() )
                                            ->get()->pluck('programming_id')->toArray();
        $programming_with_covenants  = ProgrammingActivities::query()
                                            ->whereIn('activity_id', collect( $covenants  )->pluck('activity_id')->toArray() )
                                            ->get()->pluck('programming_id')->toArray();
        $programming_with_work_table = ProgrammingActivities::query()
                                            ->whereIn('activity_id', collect( $work_table )->pluck('activity_id')->toArray() )
                                            ->get()->pluck('programming_id')->toArray();

        return response()->json([
            'data'  =>  [
                'agreements'    =>  ProgrammingView::whereBetween('execution_date', [ $this->start_date, $this->final_date  ])
                                                    ->whereIn('park_code', collect( $agreements )->pluck('park_code')->toArray() )
                                                    ->whereIn('user_id', $this->professionals)
                                                    ->whereIn('id', $programming_with_agreements)
                                                    ->count(),
                'covenants'     =>  ProgrammingView::whereBetween('execution_date', [ $this->start_date, $this->final_date  ])
                                                    ->whereIn('park_code', collect( $covenants )->pluck('park_code')->toArray() )
                                                    ->whereIn('user_id', $this->professionals)
                                                    ->whereIn('id', $programming_with_covenants)
                                                    ->count(),
                'work_table'    =>  ProgrammingView::whereBetween('execution_date', [ $this->start_date, $this->final_date  ])
                                                    ->whereIn('park_code', collect( $work_table )->pluck('park_code')->toArray() )
                                                    ->whereIn('user_id', $this->professionals)
                                                    ->whereIn('id', $programming_with_work_table)
                                                    ->count(),
            ],
            'code'  =>  200
        ], 200);
    }

    public function total()
    {
        return ( new ProgrammingController() )->index();
    }
}
