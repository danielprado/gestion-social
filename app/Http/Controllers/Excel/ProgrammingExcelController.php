<?php


namespace IDRDApp\Http\Controllers\Excel;


use Carbon\Carbon;
use IDRDApp\Entities\Schedule\CitizenAttentionView;
use IDRDApp\Entities\Schedule\CommunityAttentionView;
use IDRDApp\Entities\Schedule\Execution;
use IDRDApp\Entities\Schedule\ProgrammingCommitment;
use IDRDApp\Entities\Schedule\ProgrammingFile;
use IDRDApp\Entities\Schedule\ProgrammingImage;
use IDRDApp\Entities\Schedule\ProgrammingView;
use IDRDApp\Entities\Security\Profile;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Transformers\Excel\ProgrammingExcelTransformer;
use IDRDApp\Transformers\Schedule\ExecutionExcelTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Border;

class ProgrammingExcelController extends Controller
{
    public function index (ProgrammingView $programming)
    {
        Excel::load( public_path('excel/PROGRAMACION.xlsx'), function ($file) use ($programming) {
            //Programming
            $file->sheet(0, function ($sheet) use ( $programming ) {
                $sheet->cell("B1", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->id ) ?  "PROGRAMACIÓN #{$programming->id}" : 'PROGRAMACIÓN #0' );
                });
                $sheet->cell("B2", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->reunion_type ) ?  $programming->reunion_type : '' );
                });
                $sheet->cell("B4", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->execution_date ) ?  $programming->execution_date->format('Y-m-d') : '' );
                });
                $sheet->cell("H4", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->initial_hour, $programming->final_hour ) ?  "{$programming->initial_hour} - {$programming->final_hour}" : '' );
                });
                $sheet->cell("B6", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->park_code ) ?  $programming->park_code : '' );
                });
                $sheet->cell("E6", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->park ) ?  $programming->park : '' );
                });
                $sheet->cell("B8", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->upz ) ?  $programming->upz : '' );
                });
                $sheet->cell("H8", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->place ) ?  $programming->place : '' );
                });
                $sheet->cell("B10", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->request ) ?  $programming->request : '' );
                });
                $sheet->cell("H10", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->who_summons ) ?  $programming->who_summons : '' );
                });
                $sheet->cell("B12", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->which ) ?  $programming->which : '' );
                });
                $sheet->cell("B14", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->process ) ?  $programming->process : '' );
                });

                //Activity
                $sheet->cell("B16", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->programming_activities ) ?  $programming->programming_activities->implode('activity', ', ') : '' );
                });
                //Themes
                $sheet->cell("B18", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->programming_themes ) ?  $programming->programming_themes->implode('theme', ', ') : '' );
                });
                //Other Themes
                $sheet->cell("B20", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->programming_themes )
                                     ? trim( $programming->programming_themes->filter(function ($value, $key) {
                                            return isset( $value->pivot->other );
                                        })->implode('pivot.other', ', ') )
                                     : '' );
                });

                $sheet->cell("B22", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->objective ) ?  $programming->objective : '' );
                });

                $sheet->cell("B24", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->full_name ) ?  $programming->full_name : '' );
                });

                $sheet->cell("B27", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->created_at ) ?  $programming->created_at : '' );
                });

                $sheet->cell("I27", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->updated_at ) ?  $programming->updated_at : '' );
                });

                $sheet->cell("B33", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->deleted_at ) ?  $programming->deleted_at : '' );
                });

                $sheet->cell("I33", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->who_cancel ) ?  $programming->who_cancel : '' );
                });

                $sheet->cell("B35", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->reason_for_cancellation ) ?  $programming->reason_for_cancellation : '' );
                });

                $activities = isset(  $programming->programming_activities ) ?  $programming->programming_activities : [];
                $park_code = isset(  $programming->park_code ) ?  $programming->park_code : null;

                $sheet->cell("B38", function($cell) use ($activities, $park_code) {
                    $cell->setValue(isCovenant($park_code, $activities)  ? 'PACTOS 2018' : null );
                });

                $sheet->cell("B39", function($cell) use ($activities, $park_code) {
                    $cell->setValue( isAgreement($park_code, $activities) ? 'ACUERDOS 2018' : null );
                });

                $sheet->cell("B40", function($cell) use ($activities, $park_code) {
                    $cell->setValue( isWorkTable($park_code, $activities) ? 'MESAS 2018' : null );
                });



                //Reprogrammings
                $sheet->cell("B42", function($cell) use ($programming) {
                    $cell->setValue( isset(  $programming->reprogrammings ) ?  "REPROGRAMACIONES ({$programming->reprogrammings->count()})" : 'REPROGRAMACIONES (0)' );
                });

                if ( isset( $programming->reprogrammings ) ) {
                    $row = 44;
                    foreach ( $programming->reprogrammings as $reprogramming) {
                        $sheet->mergeCells("E$row:M$row");
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue("ID");
                        });
                        $sheet->cell("E$row", function($cell) use ($reprogramming) {
                            $cell->setValue( isset($reprogramming->id) ? $reprogramming->id : 0 );
                        });
                        $row++;
                        $sheet->mergeCells("E$row:M$row");
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue("FECHA ANTERIOR");
                        });
                        $sheet->cell("E$row", function($cell) use ($reprogramming) {
                            $cell->setValue( isset($reprogramming->date) ? substr($reprogramming->date, 0, 11) : '' );
                        });
                        $row++;
                        $sheet->mergeCells("E$row:M$row");
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue("JORNADA ANTERIOR");
                        });
                        $sheet->cell("E$row", function($cell) use ($reprogramming) {
                            $cell->setValue( isset(  $reprogramming->initial_hour, $reprogramming->final_hour ) ?  "{$reprogramming->initial_hour} - {$reprogramming->final_hour}" : '' );
                        });
                        $row++;
                        $sheet->mergeCells("E$row:M$row");
                        $sheet->setHeight($row, 220);
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue("MOTIVO");
                        });
                        $sheet->cell("E$row", function($cell) use ($reprogramming) {
                            $cell->setValue( isset($reprogramming->reason) ? $reprogramming->reason : '' );
                        });
                        $row++;
                        $sheet->mergeCells("E$row:M$row");
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue("FECHA DE REGISTRO");
                        });
                        $sheet->cell("E$row", function($cell) use ($reprogramming) {
                            $cell->setValue( isset($reprogramming->created_at) ? $reprogramming->created_at : '' );
                        });
                        $row+=2;
                    }
                }
            });

            //Execution
            $file->sheet(1, function ($sheet) use ($programming) {
                $data = Execution::query()->where('programming_id', $programming->id)
                                ->select('*')
                                ->addSelect( DB::raw('( SUM(f_0_5) + SUM(m_0_5) + SUM(f_6_12) + SUM(m_6_12) + SUM(f_13_17) + SUM(m_13_17) + SUM(f_18_26) + SUM(m_18_26) + SUM(f_27_59) + SUM(m_27_59) + SUM(f_60_more) + SUM(m_60_more) ) as subtotal') )
                                ->groupBy('id')->get();

                $resource = new Collection( $data, new ExecutionExcelTransformer());
                $manager = new Manager();
                $rootScope = $manager->createData($resource);
                if ( isset( $rootScope->toArray()['data'] ) ) {
                    $row = 4;
                    foreach ( $rootScope->toArray()['data'] as $item ) {
                        $sheet->row($row, $item);
                        $row++;
                    }
                }
            });

            //Evidences
            $file->sheet(2, function ($sheet) use ($programming) {
                $documents = [];
                if ( isset( $programming->programming_images ) ) {
                    foreach ( $programming->programming_images as $programming_image ) {
                        $file   = file_exists( public_path(ProgrammingImage::IMAGE_PATH.'/'.$programming_image->getOriginal('path') ) )
                                ? asset( 'public'.ProgrammingImage::IMAGE_PATH.'/'.$programming_image->getOriginal('path') )
                                : '';

                        $documents[] = [
                            'id'            =>  isset( $programming_image->id ) ? (int) $programming_image->id : '',
                            'file_type'     =>  'IMÁGEN',
                            'link'          =>  $file,
                            'description'   =>  isset( $programming_image->description ) ? $programming_image->description : '',
                            'created_at'    =>  isset( $programming_image->created_at ) ? $programming_image->created_at->format('Y-m-d H:i:s') : ''
                        ];
                    }
                }
                if ( isset( $programming->programming_files ) ) {
                    foreach ( $programming->programming_files as $programming_file ) {
                        $path = isset( $programming_file->pivot->path ) ? $programming_file->pivot->path : 'not_found.png';
                        $file   = file_exists( public_path(ProgrammingFile::FILE_PATH.'/'.$path ) )
                                ? asset( 'public'.ProgrammingFile::FILE_PATH.'/'.$path )
                                : '';

                        $documents[] = [
                            'id'            =>  isset( $programming_file->id ) ? (int) $programming_file->id : '',
                            'file_type'     =>  isset( $programming_file->file_type ) ? $programming_file->file_type : '',
                            'link'          =>  $file,
                            'description'   =>  '',
                            'created_at'    =>  isset( $programming_file->pivot->created_at ) ? $programming_file->pivot->created_at : ''
                        ];
                    }
                }
                $row = 4;
                foreach ( $documents as $document ) {
                    $sheet->row($row, $document);
                    $row++;
                }
            });

            //Commitments and Tracings
            $file->sheet(3, function ($sheet) use ($programming) {
                if ( isset( $programming->programming_commitments ) ) {
                    $row = 1;
                    foreach ( $programming->programming_commitments as $commitment ) {
                        $sheet->cell("A$row", function($cell) use ($commitment) {
                            $cell->setValue( isset( $commitment->id ) ? "COMPROMISO #{$commitment->id}" : "COMPROMISO 0" );
                        });
                        $row++;
                        $sheet->cell("A$row", function($cell) use ($commitment) {
                            $cell->setValue( isset( $commitment->responsable ) ? "RESPONSABLE: {$commitment->responsable}" : "RESPONSABLE" );
                        });
                        $row++;
                        $sheet->cell("A$row", function($cell) use ($commitment) {
                            $cell->setValue( isset( $commitment->description ) ? "DESCRIPCIÓN: {$commitment->description}" : "DESCRIPCIÓN" );
                        });
                        $row++;
                        $sheet->cell("A$row", function($cell) use ($commitment) {
                            $cell->setValue( isset( $commitment->date ) ? "FECHA DE VENCIMIENTO: {$commitment->date}" : "FECHA DE VENCIMIENTO" );
                        });
                        $row++;
                        $sheet->cell("A$row", function($cell) use ($commitment) {
                            $cell->setValue( isset( $commitment->created_at ) ? "FECHA DE CREACIÓN: {$commitment->created_at}" : "FECHA DE CREACIÓN" );
                        });
                        $row++;
                        $sheet->setHeight($row, 45);
                        $sheet->cell("A$row", function($cell) {
                            $cell->setValue("");
                        });
                        $row++;
                        $sheet->cell("A$row", function($cell) {
                            $cell->setValue("SEGUIMIENTOS");
                        });
                        $sheet->setHeight($row, 45);
                        $sheet->cell("A$row", function($cell) {
                            $cell->setValue("");
                        });
                        $row++;


                        if ( isset( $commitment->tracings ) ) {
                            foreach ( $commitment->tracings as $tracing ) {
                                $sheet->cell("A$row", function($cell) use ($tracing) {
                                    $cell->setValue( isset( $tracing->id ) ? "SEGUIMIENTO #{$tracing->id}" : "SEGUIMIENTO 0" );
                                });
                                $row++;
                                $sheet->cell("A$row", function($cell) use ($tracing) {
                                    $cell->setValue( isset( $tracing->description ) ? "DESCRIPCIÓN: {$tracing->description}" : "DESCRIPCIÓN" );
                                });
                                $row++;
                                $sheet->cell("A$row", function($cell) use ($tracing) {
                                    $cell->setValue( isset( $tracing->date ) ? "FECHA DE SEGUIMIENTO: {$tracing->date}" : "FECHA DE SEGUIMIENTO" );
                                });
                                $row++;
                                $sheet->cell("A$row", function($cell) use ($tracing) {
                                    $cell->setValue( isset( $tracing->created_at ) ? "FECHA DE CREACIÓN: {$tracing->created_at}" : "FECHA DE CREACIÓN" );
                                });
                                $row++;
                                $sheet->setHeight($row, 45);
                                $sheet->cell("A$row", function($cell) {
                                    $cell->setValue("");
                                });
                                $row++;
                            }
                        }

                        $row+=2;
                    }
                }
            });

        })->export('xlsx');
    }

    public function general(Request $request)
    {
        $initial_date = $request->has('start_day') ? $request->get('start_day') : Carbon::now()->startOfMonth()->format('Y-m-d');
        $final_date   = $request->has('end_day')   ? $request->get('end_day') : Carbon::now()->endOfMonth()->format('Y-m-d');



        $hasExecutionQuery = ( $request->has('entity_id') && count( $request->get('entity_id') ) > 0 ) ||
                             ( $request->has('type_of_population_id') && count( $request->get('type_of_population_id') ) > 0 ) ||
                             ( $request->has('condition_id') && count( $request->get('condition_id') ) > 0 ) ||
                             ( $request->has('situation_id') && count( $request->get('situation_id') ) > 0 );


        $data = ProgrammingView::query()->when( $request->has('park_id'), function ($query) use ( $request ) {
                        return $query->where( 'park_id', $request->get('park_id') );
                     })
                     ->when(  ( $request->has('who_summons') && count( $request->get('who_summons') ) > 0 ) , function ($query) use ( $request ) {
                         return $query->whereIn( 'who_summons_id', $request->get('who_summons') );
                     })
                     ->when( (  $request->has('process_id') && count(  $request->get('process_id') ) > 0 ) , function ($query) use ( $request ) {
                         return $query->whereIn( 'process_id', $request->get('process_id') );
                     })
                     ->when( $hasExecutionQuery, function ($query) use ( $request ) {
                         return $query->whereHas('execution', function ($query) use ( $request ) {
                             return $query->when( ( $request->has('entity_id') && count( $request->get('entity_id') ) > 0 ) , function ($q) use ( $request ) {
                                 return $q->whereIn('entity_id', $request->get('entity_id'));
                             })->when( ( $request->has('type_of_population_id') && count( $request->get('type_of_population_id') ) > 0 ) , function ($q) use ( $request ) {
                                 return $q->whereIn('type_of_population_id', $request->get('type_of_population_id'));
                             })->when( ( $request->has('condition_id') && count( $request->get('condition_id') ) > 0 ) , function ($q) use ( $request ) {
                                 return $q->whereIn('condition_id', $request->get('condition_id'));
                             })->when( ( $request->has('situation_id') && count( $request->get('situation_id') ) > 0 ) , function ($q) use ( $request ) {
                                 return $q->whereIn('situation_id', $request->get('situation_id'));
                             });
                         });
                     })
                     ->when( ( $request->has('request_type_id') && count( $request->get('request_type_id') ) > 0 ) , function ($query) use ( $request ) {
                         return $query->whereIn( 'request_type_id', $request->get('request_type_id') );
                     })
                     ->when( ( $request->has('professionals') && count( $request->get('professionals') ) > 0 ) , function ($query) use ( $request ) {
                         return $query->whereIn( 'user_id', $request->get('professionals') );
                     })->whereBetween('execution_date', [$initial_date, $final_date])->get();

        $data = $data->map( function ( $item ) {
            return ( new ProgrammingExcelTransformer() )->transform( $item );
        })->toArray();


        Excel::load( public_path('excel/REPORTE_GENERAL.xlsx'), function ($file) use ( $data, $initial_date, $final_date ) {

            $file->sheet(0, function ($sheet) use ( $data, $initial_date, $final_date ) {

                $row = 8;

                $sheet->cell("C3", function($cell)  {
                    $cell->setValue( isset(  auth()->user()->profile->full_name ) ?  auth()->user()->profile->full_name : '' );
                });
                $sheet->cell("H3", function($cell) use ($initial_date, $final_date)  {
                    $cell->setValue( "DEL  $initial_date AL $final_date " );
                });
                $sheet->cell("C4", function($cell)  {
                    $cell->setValue( Carbon::now()->format('Y-m-d H:i:s') );
                });


                foreach ( $data as $datum ) {
                    if ( $datum['is_executed'] ) {
                        $sheet->row($row, function ($cell) {
                            $cell->setBackground('#c0cfe8');
                        });
                    }
                    unset( $datum['is_executed'] );
                    $sheet->row($row, $datum);
                    $row++;
                }


                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A8:BD$sum")->applyFromArray($styleArray);
            });

        })->export('xlsx');
    }

    public function attendance( Request $request )
    {
        Excel::load( public_path('excel/ANTENCION_CIUDADANA.xlsx'), function ($file) use ( $request )  {
            $file->sheet(0, function ($sheet) use ($request) {
                $start_of_week = $request->has('start_of_week') ? $request->get('start_of_week') : Carbon::now()->startOfWeek()->format('Y-m-d');
                $end_of_week   = $request->has('end_of_week')   ? $request->get('end_of_week') : Carbon::now()->endOfWeek()->format('Y-m-d');

                $attention = CitizenAttentionView::query()
                                                ->when( ( $request->has('professionals') && count( $request->get('professionals')  ) > 0 ), function ($query) use ($request) {
                                                    return $query->whereIn('user_id',$request->get('professionals'));
                                                })
                                                ->whereBetween('execution_date', [$start_of_week, $end_of_week])
                                                ->whereNull('deleted_at')->get();

                $attentions = $attention->groupBy( 'day_month' )->sortBy('execution_date')->toArray();

                $rowB = 10;
                $rowC = 10;
                $rowD = 10;
                $rowE = 10;
                $rowF = 10;
                $rowG = 10;
                $rowH = 10;

                $sheet->cell("B7", function($cell)  {
                    $cell->setValue( isset(  auth()->user()->profile->full_name ) ?  auth()->user()->profile->full_name : '' );
                });
                $sheet->cell("E7", function($cell) use ($start_of_week, $end_of_week)  {
                    $cell->setValue( "$start_of_week - $end_of_week" );
                });

                foreach ( $attentions as $values ) {
                    foreach ( $values as $data ) {
                        if ( isset( $data['day_of_week'] ) ) {

                            $name = isset( $data['part_name'] ) ? $data['part_name'] : "";
                            $date  = isset( $data['execution_date'] ) ? trim(substr($data['execution_date'], 0, 11)) : "";
                            $schedule  = isset( $data['citizen_attention_schedule'] ) ? $data['citizen_attention_schedule'] : "";

                            $text = "{$name}\n{$date}\n{$schedule}";

                            switch ( $data['day_of_week'] ) {
                                case 0:
                                    //Sunday 0
                                    $sheet->cell("B$rowB", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowB++;
                                    break;
                                case 1:
                                    //Monday 1
                                    $sheet->cell("C$rowC", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowC++;
                                    break;
                                case 2:
                                    //Tuesday 2
                                    $sheet->cell("D$rowD", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowD++;
                                    break;
                                case 3:
                                    //Wednesday 3
                                    $sheet->cell("E$rowE", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowE++;
                                    break;
                                case 4:
                                    //Thursday 4
                                    $sheet->cell("F$rowF", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowF++;
                                    break;
                                case 5:
                                    //Friday 5
                                    $sheet->cell("G$rowG", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowG++;
                                    break;
                                case 6:
                                    //Saturday 6
                                    $sheet->cell("H$rowH", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowH++;
                                    break;
                            }
                        }
                    }
                }
            });
        })->export('xlsx');
    }

    public function byProfessional(Request $request)
    {
        Excel::load( public_path('excel/REPORTE_MENSUAL_PROFESIONAL.xlsx'), function ($file) use ( $request )  {
            $file->sheet(0, function ($sheet) use ( $request ) {
                $initial_date = $request->has('start_day') ? $request->get('start_day') : Carbon::now()->startOfMonth()->format('Y-m-d');
                $final_date   = $request->has('end_day')   ? $request->get('end_day') : Carbon::now()->endOfMonth()->format('Y-m-d');
                $user_id = $request->has('professionals')   ? $request->get('professionals') : auth()->user()->id;

                $programmings = ProgrammingView::query()->where('user_id', $user_id)
                                               ->whereBetween('execution_date', [$initial_date, $final_date])->get();

                $profile = Profile::query()->where('Id_Persona', $user_id)->first();

                $total_programming = 0;
                $total_reprogramming = 0;
                $total_cancelled = 0;
                $total_executed = 0;
                $total_not_executed = 0;
                $total_commitments = 0;
                $total_tracings = 0;
                $covenants = 0;
                $agreements = 0;
                $works = 0;


                //Professional
                $sheet->cell("C5", function($cell) use ($profile) {
                    $cell->setValue( isset( $profile->full_name ) ? $profile->full_name : null );
                });

                //Period
                $sheet->cell("C6", function($cell) use ($initial_date, $final_date) {
                    $cell->setValue("{$initial_date} - {$final_date}");
                });



                //Details

                $row = 25;
                foreach ( $programmings as $programming ) {
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setFontColor('#0BD0D9');
                        $cell->setFontSize(28);
                        $cell->setFontWeight('bold');
                        $cell->setValue( "PROGRAMACIÓN" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setFontColor('#0BD0D9');
                        $cell->setFontSize(28);
                        $cell->setFontWeight('bold');
                        $cell->setValue( isset(  $programming->id ) ?  "ID {$programming->id}" : 0 );
                    });
                    $row+=2;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "TIPO DE REUNIÓN" );
                    });

                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->reunion_type ) ?  $programming->reunion_type : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "PROCESO" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->process ) ?  $programming->process : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "ACTIVIDADES" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->programming_activities ) ?  $programming->programming_activities->implode('activity', ', ') : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "TEMAS" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->programming_themes ) ?  $programming->programming_themes->implode('theme', ', ') : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "TEMAS" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->programming_themes ) ?  $programming->programming_themes->implode('theme', ', ') : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "OTROS TEMAS" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->programming_themes )
                            ? trim( $programming->programming_themes->filter(function ($value, $key) {
                                return isset( $value->pivot->other );
                            })->implode('pivot.other', ', ') )
                            : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "FECHA DE EJECUCIÓN" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->execution_date ) ?  $programming->execution_date->format('Y-m-d') : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "JORNADA" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->initial_hour, $programming->final_hour ) ?  "{$programming->initial_hour} - {$programming->final_hour}" : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "CÓDIGO DEL PARQUE" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->park_code ) ?  $programming->park_code : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "PARQUE" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->park ) ?  $programming->park : '' );
                    });

                    $activities = isset(  $programming->programming_activities ) ?  $programming->programming_activities : [];
                    $park_code = isset(  $programming->park_code ) ?  $programming->park_code : null;

                    if ( isCovenant($park_code, $activities) ) {
                        $covenants++;
                    }

                    if ( isAgreement($park_code, $activities) ) {
                        $agreements++;
                    }

                    if ( isWorkTable($park_code, $activities) ) {
                        $works++;
                    }

                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "PACTO 2018" );
                    });
                    $sheet->cell("C$row", function($cell) use ($activities, $park_code) {
                        $cell->setValue(isCovenant($park_code, $activities)  ? 'SI' : 'NO' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "ACUERDO 2018" );
                    });
                    $sheet->cell("C$row", function($cell) use ($activities, $park_code) {
                        $cell->setValue( isAgreement($park_code, $activities) ? 'SI' : 'NO' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "MESA 2018" );
                    });
                    $sheet->cell("C$row", function($cell) use ($activities, $park_code) {
                        $cell->setValue( isWorkTable($park_code, $activities) ? 'SI' : 'NO' );
                    });



                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "UPZ" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->upz ) ?  $programming->upz : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "LUGAR" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->place ) ?  $programming->place : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "¿QUIÉN CONVOCA?" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->who_summons ) ?  $programming->who_summons : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "¿CUÁL?" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->which ) ?  $programming->which : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "TIPO DE SOLICITUD" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->request ) ?  $programming->request : '' );
                    });
                    $row++;
                    $sheet->setHeight($row, 120);
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "OBJETIVO" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->objective ) ?  $programming->objective : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "CANTIDAD DE REPROGRAMACIONES" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->reprogrammings ) ?  $programming->reprogrammings->count() : 0 );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "FECHA DE CREACIÓN" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->created_at ) ?  $programming->created_at : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "FECHA DE ACTUALIZACIÓN" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->updated_at ) ?  $programming->updated_at : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "FECHA DE CANCELACIÓN" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->deleted_at ) ?  $programming->deleted_at : '' );
                    });
                    $row++;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "¿QUIÉN CANCELA?" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->who_cancel ) ?  $programming->who_cancel : '' );
                    });
                    $row++;
                    $sheet->setHeight($row, 120);
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setValue( "MOTIVO" );
                    });
                    $sheet->cell("C$row", function($cell) use ($programming) {
                        $cell->setValue( isset(  $programming->reason_for_cancellation ) ?  $programming->reason_for_cancellation : '' );
                    });
                    $row+=3;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setFontColor('#0BD0D9');
                        $cell->setFontSize(18);
                        $cell->setFontWeight('bold');
                        $cell->setValue( "REPROGRAMACIONES" );
                    });
                    $row+=3;

                    if ( isset( $programming->reprogrammings ) ) {

                        foreach ( $programming->reprogrammings as $reprogramming) {
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue("FECHA ANTERIOR");
                            });
                            $sheet->cell("C$row", function($cell) use ($reprogramming) {
                                $cell->setValue( isset($reprogramming->date) ? substr($reprogramming->date, 0, 11) : '' );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue("JORNADA ANTERIOR");
                            });
                            $sheet->cell("C$row", function($cell) use ($reprogramming) {
                                $cell->setValue( isset(  $reprogramming->initial_hour, $reprogramming->final_hour ) ?  "{$reprogramming->initial_hour} - {$reprogramming->final_hour}" : '' );
                            });
                            $row++;
                            $sheet->setHeight($row, 220);
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue("MOTIVO");
                            });
                            $sheet->cell("C$row", function($cell) use ($reprogramming) {
                                $cell->setValue( isset($reprogramming->reason) ? $reprogramming->reason : '' );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue("FECHA DE REGISTRO");
                            });
                            $sheet->cell("C$row", function($cell) use ($reprogramming) {
                                $cell->setValue( isset($reprogramming->created_at) ? $reprogramming->created_at : '' );
                            });
                            $row+=2;
                            $total_reprogramming++;
                        }

                    }

                    $row+=3;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setFontColor('#0BD0D9');
                        $cell->setFontSize(18);
                        $cell->setFontWeight('bold');
                        $cell->setValue( "EJECUCIÓN" );
                    });
                    $row+=3;

                    $executions = Execution::query()->where('programming_id', $programming->id)
                        ->select('*')
                        ->addSelect( DB::raw('( SUM(f_0_5) + SUM(m_0_5) + SUM(f_6_12) + SUM(m_6_12) + SUM(f_13_17) + SUM(m_13_17) + SUM(f_18_26) + SUM(m_18_26) + SUM(f_27_59) + SUM(m_27_59) + SUM(f_60_more) + SUM(m_60_more) ) as subtotal') )
                        ->groupBy('id')->get();

                    $resource = new Collection( $executions, new ExecutionExcelTransformer());
                    $manager = new Manager();
                    $rootScope = $manager->createData($resource);
                    if ( isset( $rootScope->toArray()['data'] ) ) {
                        foreach ( $rootScope->toArray()['data'] as $item ) {
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "ID" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['id'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "ENTIDAD" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['entity'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "TIPO DE POBLACIÓN" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['type_of_population'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "CONDICIÓN" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['condition'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "SITUACIÓN" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['situation'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "FEMENINO 0 A 5 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['f_0_5'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "MASCULINO 0 A 5 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['m_0_5'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "FEMENINO 6 A 12 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['f_6_12'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "MASCULINO 6 A 12 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['m_6_12'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "FEMENINO 13 A 17 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['f_13_17'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "MASCULINO 13 A 17 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['m_13_17'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "FEMENINO 18 A 26 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['f_18_26'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "MASCULINO 18 A 26 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['m_18_26'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "FEMENINO 27 A 59 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['f_27_59'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "MASCULINO 27 A 59 AÑOS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['m_27_59'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "FEMENINO 60 AÑOS O MÁS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['f_60_more'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "MASCULINO 60 AÑOS O MÁS" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['m_60_more'] );
                            });


                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "SUBTOTAL" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['subtotal'] );
                            });
                            $row++;
                            $sheet->setHeight($row, 120);
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "OBSERVACIÓN" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['observation'] );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setValue( "FECHA DE CREACIÓN" );
                            });
                            $sheet->cell("C$row", function($cell) use ($item) {
                                $cell->setValue( $item['created_at'] );
                            });
                            $row++;
                        }
                    }

                    $row+=3;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setFontColor('#0BD0D9');
                        $cell->setFontSize(18);
                        $cell->setFontWeight('bold');
                        $cell->setValue( "ARCHIVOS E IMÁGENES" );
                    });
                    $row+=3;

                    $documents = [];
                    if ( isset( $programming->programming_images ) ) {
                        foreach ( $programming->programming_images as $programming_image ) {
                            $file   = file_exists( public_path(ProgrammingImage::IMAGE_PATH.'/'.$programming_image->getOriginal('path') ) )
                                ? asset( 'public'.ProgrammingImage::IMAGE_PATH.'/'.$programming_image->getOriginal('path') )
                                : '';

                            $documents[] = [
                                'id'            =>  isset( $programming_image->id ) ? (int) $programming_image->id : '',
                                'file_type'     =>  'IMÁGEN',
                                'link'          =>  $file,
                                'description'   =>  isset( $programming_image->description ) ? $programming_image->description : '',
                                'created_at'    =>  isset( $programming_image->created_at ) ? $programming_image->created_at->format('Y-m-d H:i:s') : ''
                            ];
                        }
                    }
                    if ( isset( $programming->programming_files ) ) {
                        foreach ( $programming->programming_files as $programming_file ) {
                            $path = isset( $programming_file->pivot->path ) ? $programming_file->pivot->path : 'not_found.png';
                            $file   = file_exists( public_path(ProgrammingFile::FILE_PATH.'/'.$path ) )
                                ? asset( 'public'.ProgrammingFile::FILE_PATH.'/'.$path )
                                : '';

                            $documents[] = [
                                'id'            =>  isset( $programming_file->id ) ? (int) $programming_file->id : '',
                                'file_type'     =>  isset( $programming_file->file_type ) ? $programming_file->file_type : '',
                                'link'          =>  $file,
                                'description'   =>  '',
                                'created_at'    =>  isset( $programming_file->pivot->created_at ) ? $programming_file->pivot->created_at : ''
                            ];
                        }
                    }

                    foreach ( $documents as $document ) {
                        $row++;
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue( "ID" );
                        });
                        $sheet->cell("C$row", function($cell) use ($document) {
                            $cell->setValue( $document['id'] );
                        });
                        $row++;
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue( "TIPO DE ARCHIVO" );
                        });
                        $sheet->cell("C$row", function($cell) use ($document) {
                            $cell->setValue( $document['file_type'] );
                        });
                        $row++;
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue( "LINK DEL ARCHIVO" );
                        });
                        $sheet->cell("C$row", function($cell) use ($document) {
                            $cell->setValue( $document['link'] );
                        });
                        $row++;
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue( "DESCRIPCIÓN" );
                        });
                        $sheet->cell("C$row", function($cell) use ($document) {
                            $cell->setValue( $document['description'] );
                        });
                        $row++;
                        $sheet->cell("B$row", function($cell) {
                            $cell->setValue( "FECHA DE CREACIÓN" );
                        });
                        $sheet->cell("C$row", function($cell) use ($document) {
                            $cell->setValue( $document['created_at'] );
                        });
                        $row++;
                    }

                    $row+=3;
                    $sheet->cell("B$row", function($cell) use ($programming) {
                        $cell->setFontColor('#0BD0D9');
                        $cell->setFontSize(18);
                        $cell->setFontWeight('bold');
                        $cell->setValue( "COMPROMISOS" );
                    });
                    $row+=3;

                    if ( isset( $programming->programming_commitments ) ) {
                        foreach ( $programming->programming_commitments as $commitment ) {
                            $sheet->cell("B$row", function($cell) use ($programming) {
                                $cell->setValue( "ID" );
                            });
                            $sheet->cell("C$row", function($cell) use ($commitment) {
                                $cell->setValue( isset( $commitment->id ) ? $commitment->id : "" );
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) use ($programming) {
                                $cell->setValue( "RESPONSABLE" );
                            });
                            $sheet->cell("C$row", function($cell) use ($commitment) {
                                $cell->setValue( isset( $commitment->responsable ) ? $commitment->responsable : "");
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) use ($programming) {
                                $cell->setValue( "DESCRIPCIÓN" );
                            });
                            $sheet->cell("C$row", function($cell) use ($commitment) {
                                $cell->setValue( isset( $commitment->description ) ? $commitment->description : "");
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) use ($programming) {
                                $cell->setValue( "FECHA DE VENCIMIENTO" );
                            });
                            $sheet->cell("C$row", function($cell) use ($commitment) {
                                $cell->setValue( isset( $commitment->date ) ? $commitment->date : "");
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) use ($programming) {
                                $cell->setValue( "FECHA DE CREACIÓN" );
                            });
                            $sheet->cell("C$row", function($cell) use ($commitment) {
                                $cell->setValue( isset( $commitment->created_at ) ? $commitment->created_at : "");
                            });
                            $row++;
                            $sheet->cell("C$row", function($cell) {
                                $cell->setValue("");
                            });
                            $row++;
                            $sheet->cell("B$row", function($cell) {
                                $cell->setFontColor('#0BD0D9');
                                $cell->setFontSize(18);
                                $cell->setFontWeight('bold');
                                $cell->setValue("SEGUIMIENTOS");
                            });
                            $row++;
                            $sheet->cell("C$row", function($cell) {
                                $cell->setValue("");
                            });
                            $row++;


                            if ( isset( $commitment->tracings ) ) {
                                foreach ( $commitment->tracings as $tracing ) {
                                    $sheet->cell("B$row", function($cell) {
                                        $cell->setValue("ID");
                                    });
                                    $sheet->cell("C$row", function($cell) use ($tracing) {
                                        $cell->setValue( isset( $tracing->id ) ? $tracing->id : "");
                                    });
                                    $row++;
                                    $sheet->cell("B$row", function($cell) {
                                        $cell->setValue("DESCRIPCIÓN");
                                    });
                                    $sheet->cell("C$row", function($cell) use ($tracing) {
                                        $cell->setValue( isset( $tracing->description ) ? $tracing->description : "");
                                    });
                                    $row++;
                                    $sheet->cell("B$row", function($cell) {
                                        $cell->setValue("FECHA DE SEGUIMIENTO");
                                    });
                                    $sheet->cell("C$row", function($cell) use ($tracing) {
                                        $cell->setValue( isset( $tracing->date ) ? $tracing->date : "");
                                    });
                                    $row++;
                                    $sheet->cell("B$row", function($cell) {
                                        $cell->setValue("FECHA DE CREACIÓN");
                                    });
                                    $sheet->cell("C$row", function($cell) use ($tracing) {
                                        $cell->setValue( isset( $tracing->created_at ) ? $tracing->created_at : "");
                                    });
                                    $row++;
                                    $sheet->cell("B$row", function($cell) {
                                        $cell->setValue("");
                                    });
                                    $sheet->cell("C$row", function($cell) {
                                        $cell->setValue("");
                                    });
                                    $row++;
                                    $total_tracings++;
                                }
                            }

                            $row+=2;

                            $total_commitments++;
                        }
                    }

                    $total_programming++;

                    if ( isset( $programming->deleted_at ) ) {
                        $total_cancelled++;
                    }

                    if (  $programming->execution()->count() > 0  &&
                        $programming->programming_files()->count() > 0   &&
                        $programming->programming_images()->count() > 0  &&
                        $programming->programming_commitments()->count() > 0   ) {
                        $total_executed++;
                    } else {
                        $total_not_executed++;
                    }


                }


                //Total programming
                $sheet->cell("C9", function($cell) use ( $total_programming ) {
                    $cell->setValue($total_programming );
                });

                //Total reprogramming
                $sheet->cell("C10", function($cell) use ( $total_reprogramming ) {
                    $cell->setValue($total_reprogramming );
                });

                //Total cancelled
                $sheet->cell("C11", function($cell) use ( $total_cancelled ) {
                    $cell->setValue( $total_cancelled);
                });

                //Total executed
                $sheet->cell("C12", function($cell) use ( $total_executed ) {
                    $cell->setValue( $total_executed);
                });

                //Total not executed
                $sheet->cell("C13", function($cell) use ( $total_not_executed ) {
                    $cell->setValue($total_not_executed );
                });


                //Total commitments
                $sheet->cell("C14", function($cell) use ( $total_commitments ) {
                    $cell->setValue( $total_commitments );
                });


                //Total tracings
                $sheet->cell("C15", function($cell) use ( $total_tracings )  {
                    $cell->setValue( $total_tracings );
                });



                //Total agreements
                $sheet->cell("C16", function($cell) use ( $agreements )  {
                    $cell->setValue( $agreements );
                });



                //Total covenants
                $sheet->cell("C17", function($cell) use ( $covenants )  {
                    $cell->setValue( $covenants );
                });



                //Total works
                $sheet->cell("C18", function($cell) use ( $works )  {
                    $cell->setValue( $works );
                });

            });
        })->export('xlsx');
    }

    public function community( Request $request )
    {
        Excel::load( public_path('excel/ANTENCION_COMUNIDAD.xlsx'), function ($file) use ( $request )  {
            $file->sheet(0, function ($sheet) use ($request) {
                $start_of_week = $request->has('start_of_week') ? $request->get('start_of_week') : Carbon::now()->startOfWeek()->format('Y-m-d');
                $end_of_week   = $request->has('end_of_week')   ? $request->get('end_of_week') : Carbon::now()->endOfWeek()->format('Y-m-d');

                $attention = CommunityAttentionView::query()
                    ->when( ( $request->has('professionals') && count( $request->get('professionals')  ) > 0 ), function ($query) use ($request) {
                        return $query->whereIn('user_id',$request->get('professionals'));
                    })
                    ->whereBetween('execution_date', [$start_of_week, $end_of_week])
                    ->whereNull('deleted_at')->get();

                $attentions = $attention->groupBy( 'day_month' )->sortByDesc('execution_date')->toArray();

                $rowB = 10;
                $rowC = 10;
                $rowD = 10;
                $rowE = 10;
                $rowF = 10;
                $rowG = 10;
                $rowH = 10;

                $sheet->cell("B7", function($cell)  {
                    $cell->setValue( isset(  auth()->user()->profile->full_name ) ?  auth()->user()->profile->full_name : '' );
                });
                $sheet->cell("E7", function($cell) use ($start_of_week, $end_of_week)  {
                    $cell->setValue( "$start_of_week - $end_of_week" );
                });

                foreach ( $attentions as $values ) {
                    foreach ( $values as $data ) {
                        if ( isset( $data['day_of_week'] ) ) {

                            $name = isset( $data['part_name'] ) ? $data['part_name'] : "";
                            $date  = isset( $data['execution_date'] ) ? trim(substr($data['execution_date'], 0, 11)) : "";
                            $from  = isset( $data['initial_hour'] ) ? $data['initial_hour'] : "";
                            $to  = isset( $data['final_hour'] ) ? $data['final_hour'] : "";
                            $schedule = "$from - $to";

                            $text = "{$name}\n{$date}\n{$schedule}";

                            switch ( $data['day_of_week'] ) {
                                case 0:
                                    //Sunday 0
                                    $sheet->cell("B$rowB", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowB++;
                                    break;
                                case 1:
                                    //Monday 1
                                    $sheet->cell("C$rowC", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowC++;
                                    break;
                                case 2:
                                    //Tuesday 2
                                    $sheet->cell("D$rowD", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowD++;
                                    break;
                                case 3:
                                    //Wednesday 3
                                    $sheet->cell("E$rowE", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowE++;
                                    break;
                                case 4:
                                    //Thursday 4
                                    $sheet->cell("F$rowF", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowF++;
                                    break;
                                case 5:
                                    //Friday 5
                                    $sheet->cell("G$rowG", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowG++;
                                    break;
                                case 6:
                                    //Saturday 6
                                    $sheet->cell("H$rowH", function($cell) use ($text ) {
                                        $cell->setValue( $text );
                                    });
                                    $rowH++;
                                    break;
                            }
                        }
                    }
                }
            });
        })->export('xlsx');
    }
}