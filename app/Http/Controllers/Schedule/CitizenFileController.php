<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\CitizenAttention;
use IDRDApp\Entities\Schedule\CitizenFile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CitizenFileController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param CitizenAttention $citizen
     * @return JsonResponse
     */
    public function store(Request $request, CitizenAttention $citizen)
    {
        $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('file')->getClientOriginalExtension();
        if ( $request->file('file')->move( public_path(CitizenFile::FILE_PATH ), $file_name  ) ) {
            $citizen->files()->create([
                'citizen_id'    => $citizen->id,
                'file_name' => $request->file('file')->getClientOriginalName(),
                'path' => $file_name,
            ]);
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
        return $this->error_response( trans('validation.handler.unexpected_failure') );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param CitizenAttention $citizen
     * @param CitizenFile $file
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(CitizenAttention $citizen, CitizenFile $file)
    {
        $real_file_path = public_path( CitizenFile::FILE_PATH.'/'.$file->getOriginal('path') );
        if ( file_exists( $real_file_path ) ) {
            unlink( $real_file_path );
        }
        if ( $file->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
        return $this->error_response( trans('validation.handler.unexpected_failure') );
    }
}
