<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\Activity;
use IDRDApp\Entities\Schedule\Process;
use IDRDApp\Transformers\Schedule\ActivityTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Process $process
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Process $process)
    {
        $resource = new Collection(Activity::query()->where('process_id', $process->id)->get(), new ActivityTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }
}
