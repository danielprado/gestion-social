<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\Condition;
use IDRDApp\Transformers\Schedule\ConditionTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $resource = new Collection(Condition::query()->whereNotNull('condition')->get(), new ConditionTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }
}
