<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\Execution;
use IDRDApp\Entities\Schedule\Programming;
use IDRDApp\Http\Requests\Schedule\StoreExecutionRequest;
use IDRDApp\Transformers\Schedule\ExecutionTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ExecutionController extends Controller
{
    public function index(Programming $programming)
    {
        $data = Execution::query()->where('programming_id', $programming->id)
                                  ->select('*')
                                  ->addSelect( DB::raw('( SUM(f_0_5) + SUM(m_0_5) + SUM(f_6_12) + SUM(m_6_12) + SUM(f_13_17) + SUM(m_13_17) + SUM(f_18_26) + SUM(m_18_26) + SUM(f_27_59) + SUM(m_27_59) + SUM(f_60_more) + SUM(m_60_more) ) as subtotal') )
                                  ->groupBy('id')->get();

        $resource = new Collection( $data, new ExecutionTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }

    /**
     * @param StoreExecutionRequest $request
     * @param Programming $programming
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreExecutionRequest $request, Programming $programming)
    {
        $programming->execution()->create([
            'programming_id'            =>  $programming->id,
            'entity_id'                 =>  $request->get('entity_id'),
            'type_of_population_id'     =>  $request->get('type_of_population_id'),
            'condition_id'              =>  $request->get('condition_id'),
            'situation_id'              =>  $request->get('situation_id'),
            'f_0_5'                     =>  $request->get('f_0_5'),
            'm_0_5'                     =>  $request->get('m_0_5'),
            'f_6_12'                    =>  $request->get('f_6_12'),
            'm_6_12'                    =>  $request->get('m_6_12'),
            'f_13_17'                   =>  $request->get('f_13_17'),
            'm_13_17'                   =>  $request->get('m_13_17'),
            'f_18_26'                   =>  $request->get('f_18_26'),
            'm_18_26'                   =>  $request->get('m_18_26'),
            'f_27_59'                   =>  $request->get('f_27_59'),
            'm_27_59'                   =>  $request->get('m_27_59'),
            'f_60_more'                 =>  $request->get('f_60_more'),
            'm_60_more'                 =>  $request->get('m_60_more'),
            'observation'               =>  $request->get('observation'),
        ]);

        return response()->json([
            'data'  =>  trans('validation.handler.success'),
            'code'  =>  200
        ], 200);
    }

    /**
     * @param Programming $programming
     * @param Execution $execution
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Programming $programming, Execution $execution )
    {
        if ( $execution->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }
}
