<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\Programming;
use IDRDApp\Entities\Schedule\ProgrammingFile;
use IDRDApp\Entities\Schedule\ProgrammingImage;
use IDRDApp\Http\Requests\Schedule\StoreFileRequest;
use IDRDApp\Http\Requests\Schedule\StoreImageRequest;
use IDRDApp\Transformers\Schedule\ProgrammingFileTransformer;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Transformers\Schedule\ProgrammingImageTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ProgrammingImageController extends Controller
{
    public function index(Programming $programming)
    {
        $data = ProgrammingImage::query()->where('programming_id', $programming->id)->get();

        $resource = new Collection( $data, new ProgrammingImageTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }

    /**
     * @param StoreImageRequest $request
     * @param Programming $programming
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreImageRequest $request, Programming $programming)
    {
        if ( $request->hasFile('image') ) {
            $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('image')->getClientOriginalExtension();
            if ( $request->file('image')->move( public_path(ProgrammingImage::IMAGE_PATH ), $file_name  ) ) {
                $programming->programming_images()->create([
                    'programming_id'    =>  $programming->id,
                    'file_name'         =>  $request->file('image')->getClientOriginalName(),
                    'confirmation'      =>  (boolean) $request->get('confirmation'),
                    'description'       =>  toUpper( $request->get('description') ),
                    'path' => $file_name,
                ]);
                return response()->json([
                    'data'  =>  trans('validation.handler.success'),
                    'code'  =>  200
                ], 200);
            }
        } else {
            $programming->programming_images()->create([
                'programming_id'    =>  $programming->id,
                'confirmation'      =>  false,
                'description'       =>  toUpper( $request->get('description') ),
            ]);
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
        return $this->error_response( trans('validation.handler.unexpected_failure') );
    }


    public function destroy(Programming $programming, ProgrammingImage $image)
    {
        $real_file_path = public_path( ProgrammingImage::IMAGE_PATH.'/'.$image->getOriginal('path') );
        if ( file_exists( $real_file_path ) ) {
            unlink( $real_file_path );
        }
        if ( $image->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }
}
