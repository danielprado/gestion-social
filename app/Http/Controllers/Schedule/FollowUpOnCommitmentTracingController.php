<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\FollowUpOnCommitment;
use IDRDApp\Entities\Schedule\FollowUpOnCommitmentTracing;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;

class FollowUpOnCommitmentTracingController extends Controller
{
    public function store(Requests\Schedule\FollowUpOnCommitmentTracingRequest $request, FollowUpOnCommitment $commitment)
    {
        $commitment->tracings()->create([
            'follow_commitment_id' =>  $commitment->id,
            'date'              =>  $request->get('date'),
            'description'       =>  $request->get('description')
        ]);
        return response()->json([
            'data'  =>  trans('validation.handler.success'),
            'code'  =>  200
        ], 200);
    }

    public function destroy(FollowUpOnCommitment $commitment, FollowUpOnCommitmentTracing $tracing)
    {
        if ( $tracing->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }

}
