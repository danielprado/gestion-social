<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\Process;
use IDRDApp\Transformers\Schedule\ProcessTransformer;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ProcessController extends Controller
{
    public function index()
    {
        $resource = new Collection(Process::query()->whereNotNull('process')->get(), new ProcessTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }
}
