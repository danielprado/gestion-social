<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\RequestType;
use IDRDApp\Transformers\Schedule\RequestTypeTransformer;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class RequestTypeController extends Controller
{
    public function index()
    {
        $resource = new Collection(RequestType::query()->whereNotNull('request_type')->get(), new RequestTypeTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }
}
