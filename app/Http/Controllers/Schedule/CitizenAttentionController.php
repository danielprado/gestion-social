<?php

namespace IDRDApp\Http\Controllers\Schedule;

use Carbon\Carbon;
use IDRDApp\Entities\Schedule\CitizenAttention;
use IDRDApp\Entities\Schedule\CitizenAttentionView;
use IDRDApp\Transformers\Schedule\CitizenAttentionTransformer;
use IDRDApp\Transformers\Schedule\CitizenAttentionViewTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CitizenAttentionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user_id = request()->has( 'professional' ) ? request()->get('professional') : auth()->user()->id;
        $records = $this->table( new CitizenAttentionView(), new CitizenAttentionViewTransformer(), false);
        $records = $records->where('user_id', $user_id);
        $records = $this->paginateCollection( $records, new CitizenAttentionViewTransformer() );
        return response()->json([
            'data'  =>  $records,
            'code'  =>  200
        ], 200);
    }

    /**
     * Display a listing of the resource to the calendar.
     *
     * @return Response
     */
    public function calendar(Request $request)
    {
        $start   = $request->has( 'start' ) ? $request->get('start') : Carbon::now()->startOfMonth();
        $end     = $request->has( 'end' )   ? $request->get('end')   : Carbon::now()->endOfMonth();
        $user_id = $request->has( 'professional' )   ? $request->get('professional') : auth()->user()->id;
        $is_general = $request->has('general') ? $request->get('general') : false;

        $data = CitizenAttentionView::query()
                    ->whereBetween( 'execution_date', [$start, $end] )
                    ->whereNull('deleted_at')
                    ->when( ! $is_general, function ($query) use ($user_id) {
                        return $query->where('user_id', $user_id );
                    })
                    ->get();

        $resource = new Collection( $data, new CitizenAttentionViewTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\Schedule\StroreCitizenAttentionRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(Requests\Schedule\StroreCitizenAttentionRequest $request)
    {
        $resource = new CitizenAttention();
        $resource->execution_date   = $request->get('date');
        $resource->citizen_attention_schedule_id    = $request->get('working_day');
        $resource->user_id      = auth()->user()->id;
        if ( $resource->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  =>  201
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param CitizenAttentionView $attention
     * @return JsonResponse
     */
    public function show(CitizenAttentionView $attention)
    {
        $resource = new Item($attention, new CitizenAttentionViewTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\Schedule\UpdateCitizenAttentionRequest $request
     * @param CitizenAttention $attention
     * @return JsonResponse
     * @throws \Throwable
     */
    public function update(Requests\Schedule\UpdateCitizenAttentionRequest $request, CitizenAttention $attention)
    {
        $attention->reprogramming()->create([
            'citizen_attention_id'           => $attention->id,
            'date'                           => $attention->execution_date,
            'schedule_id'                    => $attention->citizen_attention_schedule_id,
            'reason'                         => $request->get('reprogramming_reason'),
        ]);

        $attention->execution_date                   = $request->get('date');
        $attention->citizen_attention_schedule_id    = $request->get('working_day');

        if ( $attention->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Requests\Schedule\CitizenAttentionCancelRequest $request
     * @param CitizenAttention $attention
     * @return Response
     * @throws \Throwable
     */
    public function cancel(Requests\Schedule\CitizenAttentionCancelRequest $request, CitizenAttention $attention)
    {
        $attention->who_cancel               = $request->get('who_cancel');
        $attention->reason_for_cancellation  = $request->get('reason');
        $attention->deleted_at               = Carbon::now();
        if ( $attention->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }
}
