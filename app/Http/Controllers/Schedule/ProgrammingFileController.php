<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\Programming;
use IDRDApp\Entities\Schedule\ProgrammingFile;
use IDRDApp\Http\Requests\Schedule\StoreFileRequest;
use IDRDApp\Transformers\Schedule\ProgrammingFileTransformer;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ProgrammingFileController extends Controller
{
    public function index(Programming $programming)
    {
        $data = ProgrammingFile::query()->where('programming_id', $programming->id)->get();

        $resource = new Collection( $data, new ProgrammingFileTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }

    /**
     * @param StoreFileRequest $request
     * @param Programming $programming
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreFileRequest $request, Programming $programming)
    {
        $file_name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('file')->getClientOriginalExtension();
        if ( $request->file('file')->move( public_path(ProgrammingFile::FILE_PATH ), $file_name  ) ) {
            $programming->programming_files()->attach($programming->id, [
                'file_type_id' => $request->get('file_type_id'),
                'file_name' => $request->file('file')->getClientOriginalName(),
                'path' => $file_name,
            ]);
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
        return $this->error_response( trans('validation.handler.unexpected_failure') );
    }


    public function destroy(Programming $programming, ProgrammingFile $file)
    {
        $real_file_path = public_path( ProgrammingFile::FILE_PATH.'/'.$file->getOriginal('path') );
        if ( file_exists( $real_file_path ) ) {
            unlink( $real_file_path );
        }
        if ( $file->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }
}
