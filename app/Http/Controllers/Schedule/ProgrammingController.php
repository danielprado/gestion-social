<?php

namespace IDRDApp\Http\Controllers\Schedule;

use Carbon\Carbon;
use IDRDApp\Entities\Schedule\Programming;
use IDRDApp\Entities\Schedule\ProgrammingThemes;
use IDRDApp\Entities\Schedule\ProgrammingView;
use IDRDApp\Entities\Schedule\Theme;
use IDRDApp\Http\Requests\Schedule\CancelProgrammingRequest;
use IDRDApp\Http\Requests\Schedule\DetailsProgrammingRequest;
use IDRDApp\Http\Requests\Schedule\StoreProgrammingRequest;
use IDRDApp\Http\Requests\Schedule\UpdateProgrammingRequest;
use IDRDApp\Transformers\Schedule\ProgrammingTransformer;
use IDRDApp\Transformers\Schedule\ProgrammingViewTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ProgrammingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $start_date = request()->has('start_date') ? request()->get('start_date') : null;
        $final_date = request()->has('final_date') ? request()->get('final_date') : null;
        $user_id = ( request()->has( 'professional' ) && count(request()->get('professional')) > 0 ) ? request()->get('professional') :[ auth()->user()->id ];
        $trashed = ( request()->has('trashed') && request()->get('trashed') == 'true' ) ? true : false;
        $executed = ( request()->has('executed') && request()->get('executed') == 'true' ) ? true : false;

        $onlyActive = ( request()->has('only_active') && request()->get('only_active') == 'true' ) ? true : false;

        $records = $this->table( new ProgrammingView(), new ProgrammingViewTransformer(), false);
        $records = $records->whereIn('user_id', $user_id)
                           ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                               return $query->whereBetween( 'execution_date', [$start_date, $final_date] );
                           })
                           ->when( (!$onlyActive && $trashed), function ($query) {
                               return $query->whereNotNull( 'deleted_at');
                           })
                           ->when( ($onlyActive && !$trashed), function ($query) {
                               return $query->whereNull( 'deleted_at');
                           })
                           ->when( $executed, function ($query) {
                               return $query->has('programming_activities')->has('programming_files')->has('programming_images');
                           });
        $records = $this->paginateCollection( $records, new ProgrammingViewTransformer() );
        return response()->json([
            'data'  =>  $records,
            'code'  =>  200
        ], 200);
    }

    /**
     * Display a listing of the resource to the calendar.
     *
     * @param Request $request
     * @return Response
     */
    public function calendar(Request $request)
    {
        $start   = $request->has( 'start' ) ? $request->get('start') : Carbon::now()->startOfMonth();
        $end     = $request->has( 'end' )   ? $request->get('end')   : Carbon::now()->endOfMonth();
        $user_id = $request->has( 'professional' )   ? $request->get('professional') : auth()->user()->id;
        $is_general = $request->has('general') ? $request->get('general') : false;

        $data = ProgrammingView::query()
            ->whereBetween( 'execution_date', [$start, $end] )
            ->whereNull('deleted_at')
            ->when( ! $is_general, function ($query) use ($user_id) {
                return $query->where('user_id', $user_id );
            })
            ->get();

        $resource = new Collection( $data, new ProgrammingViewTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param ProgrammingView $programming
     * @return Response
     */
    public function show(ProgrammingView $programming)
    {
        $resource = new Item($programming, new ProgrammingViewTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProgrammingRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable 4347190003120753 *700
     */
    public function store(StoreProgrammingRequest $request)
    {
        $resource = new Programming();
        if ( $request->has('title') ) {
            $resource->title            = $request->get('title');
        }
        $resource->execution_date   = $request->get('date');
        $resource->initial_hour     = $request->get('initial_hour');
        $resource->final_hour       = $request->get('final_hour');
        $resource->reunion_type_id  = $request->get('reunion_type_id');
        $resource->park             = $request->get('park');
        $resource->upz              = $request->get('upz');
        $resource->upz_id           = $request->get('upz_id');
        $resource->place            = $request->get('place');
        $resource->process_id       = $request->get('process_id');
        $resource->park_id          = $request->get('park_id');
        $resource->who_summons_id   = $request->get('who_summons');
        $resource->which            = $request->get('which');
        $resource->request_type_id  = $request->get('request_type_id');
        $resource->objective        = $request->get('objective');

        $resource->include_attendance = (boolean) $request->get('include_attendance');
        $resource->include_files      = (boolean) $request->get('include_files');
        $resource->include_images     = (boolean) $request->get('include_images');
        $resource->include_commitments= (boolean) $request->get('include_commitments');

        $resource->user_id          = auth()->user()->id;
        if ( $resource->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  =>  201
            ], 201);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProgrammingRequest $request
     * @param Programming $programming
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function update(UpdateProgrammingRequest $request, Programming $programming)
    {

        $programming->reprogrammings()->create([
            'programming_id'                => $programming->id,
            'date'                          => $programming->execution_date,
            'initial_hour'                  => $programming->initial_hour,
            'final_hour'                    => $programming->final_hour,
            'reason'                        => $request->get('reprogramming_reason'),
        ]);

        $programming->execution_date   = $request->get('date');
        $programming->initial_hour     = $request->get('initial_hour');
        $programming->final_hour       = $request->get('final_hour');
        if ( $programming->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CancelProgrammingRequest $request
     * @param Programming $programming
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function cancel(CancelProgrammingRequest $request, Programming $programming)
    {

        if (  $programming->execution()->count() > 0  ||
              $programming->programming_files()->count() > 0   ||
              $programming->programming_images()->count() > 0  ||
              $programming->programming_commitments()->count() > 0   ) {
            return $this->error_response( trans('validation.handler.executed') );
        }
        $programming->who_cancel               = $request->get('who_cancel');
        $programming->reason_for_cancellation  = $request->get('reason');
        $programming->deleted_at               = Carbon::now();
        if ( $programming->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }

    /**
     * Store a newly created activities and themes resource in storage.
     *
     * @param DetailsProgrammingRequest $request
     * @param Programming $programming
     * @return \Illuminate\Http\JsonResponse
     */
    public function details( DetailsProgrammingRequest $request, Programming $programming )
    {
        foreach ($request->get('activities') as $activity ) {
            $programming->programming_activities()->attach($programming->id, [
                'activity_id'   =>  $activity,
            ]);
        }

        $other_theme = Theme::query()->where('theme', 'OTROS')->first();
        $theme_id = isset( $other_theme->id ) ? $other_theme->id : null;

        foreach ($request->get('themes') as $theme ) {
            if ( $theme_id == $theme && $request->has('other')) {
                $programming->programming_themes()->attach($programming->id, [
                    'theme_id'   =>  $theme,
                    'other'      =>  toUpper( $request->get('other') )
                ]);
            } else {
                $programming->programming_themes()->attach($programming->id, [
                    'theme_id'   =>  $theme
                ]);
            }
        }

        return response()->json([
            'data'  =>  trans('validation.handler.success'),
            'code'  =>  200
        ], 200);
    }

    public function destroy( Programming $programming )
    {
        $programming->programming_files()->chunk(10, function ($files) {
            foreach ( $files as $file ) {
                if ( isset( $file->pivot->path ) ) {
                    if ( file_exists( public_path('storage/files/'.$file->pivot->path) ) ) {
                        unlink( public_path('storage/files/'.$file->pivot->path) );
                    }
                }
            }
        });
        $programming->programming_images()->chunk(10, function ($images) {
            foreach ( $images as $image ) {
                $path = $image->getOriginal('path');
                if ( isset( $path ) ) {
                    if ( file_exists( public_path('storage/images/'.$image->getOriginal('path')) ) ) {
                        unlink(  public_path('storage/images/'.$image->getOriginal('path')) );
                    }
                }
            }
        });

        $programming->programming_commitments()->chunk( 20, function ($commitments) {
            foreach ( $commitments as $commitment ) {
                $commitment->tracings()->delete();
            }
        });


        $programming->programming_themes()->detach();
        $programming->programming_activities()->detach();
        $programming->execution()->forceDelete();


        $programming->programming_files()->detach();
        $programming->programming_images()->forceDelete();

        $programming->programming_commitments()->forceDelete();
        $programming->reprogrammings()->forceDelete();


        $programming->forceDelete();

        return \response()->json([
            'data'      =>  "Programming { $programming->id } was force deleted successfully",
            'details'   =>  $programming,
            'code'      =>  204
        ], 200);
    }
}
