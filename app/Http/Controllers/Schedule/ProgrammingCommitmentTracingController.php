<?php


namespace IDRDApp\Http\Controllers\Schedule;


use IDRDApp\Entities\Schedule\Programming;
use IDRDApp\Entities\Schedule\ProgrammingCommitment;
use IDRDApp\Entities\Schedule\ProgrammingCommitmentTracing;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Http\Requests\Schedule\StoreProgrammingCommitmentTracingRequest;

class ProgrammingCommitmentTracingController extends Controller
{
    /**
     * @param StoreProgrammingCommitmentTracingRequest $request
     * @param Programming $programming
     * @param ProgrammingCommitment $commitment
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProgrammingCommitmentTracingRequest $request, Programming $programming, ProgrammingCommitment $commitment)
    {
        $commitment->tracings()->create([
            'commitment_id'     =>  $commitment->id,
            'date'              =>  $request->get('date'),
            'description'       =>  $request->get('description')
        ]);

        return response()->json([
            'data'  =>  trans('validation.handler.success'),
            'code'  =>  200
        ], 200);
    }


    public function destroy(Programming $programming, ProgrammingCommitment $commitment, ProgrammingCommitmentTracing $tracing)
    {
        if ( $tracing->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }
}