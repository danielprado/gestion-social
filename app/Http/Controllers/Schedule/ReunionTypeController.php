<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\ReunionType;
use IDRDApp\Transformers\Schedule\ReunionTypeTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ReunionTypeController extends Controller
{
    public function index()
    {
        $resource = new Collection(ReunionType::query()->whereNotNull('reunion_type')->get(), new ReunionTypeTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }
}
