<?php

namespace IDRDApp\Http\Controllers\Schedule;

use Carbon\Carbon;
use IDRDApp\Entities\Schedule\FollowUpOnCommitment;
use IDRDApp\Entities\Schedule\ProgrammingActivities;
use IDRDApp\Entities\Schedule\ProgrammingCommitment;
use IDRDApp\Entities\Schedule\ProgrammingView;
use IDRDApp\Transformers\Schedule\CommitmentExcelTransformer;
use IDRDApp\Transformers\Schedule\CommitmentTransformer;
use IDRDApp\Transformers\Schedule\FollowingUpOnCommitmentTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Border;

class FollowUpOnCommitmentController extends Controller
{
    public function index()
    {
        $start_date = request()->has('start_date') ? request()->get('start_date') : null;
        $final_date = request()->has('final_date') ? request()->get('final_date') : null;
        $park = request()->has('park_id') ? request()->get('park_id') : null;
        $user_id = ( request()->has( 'professional' ) && count(request()->get('professional')) > 0 ) ? request()->get('professional') : [];
        $types = \request()->has('type_id') ? request()->get('type_id') : [
            16, // Mesas de trabajo
            17, // Acuerdos ciudadanos
            18, // Pactos ciudadanos
            31, // Firmas de acuerdos ciudadanos
        ];

        $programmings = ProgrammingActivities::query()->whereIn('activity_id', $types)->get(['programming_id'])->toArray();
        // $records = $this->table( new ProgrammingCommitment(), new CommitmentExcelTransformer(), false);
        $records = $this->table( new ProgrammingView(), new CommitmentExcelTransformer(), false);
        $records = $records->whereIn('id', $programmings)
                            ->when( count($user_id) > 0, function ($query) use ( $user_id ) {
                                return $query->whereIn('user_id', $user_id);
                            })
                            ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                                return $query->whereDate( 'execution_date', '>=', $start_date)
                                    ->whereDate( 'execution_date', '<=', $final_date);
                            })
                            ->when( isset($park), function ($query) use ( $park ) {
                                return $query->where( 'park_id', $park);
                            });
        /*
        $records = $records->whereIn('programming_id', $programmings)
            ->when( count($user_id) > 0, function ($query) use ( $user_id ) {
                return $query->whereHas('programing', function ($q) use ($user_id) {
                    return $q->whereIn('user_id', $user_id);
                });
            })
            ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                return $query->whereBetween( 'date', [$start_date, $final_date] );
            })
            ->when( isset($park), function ($query) use ( $park ) {
                return $query->whereHas('programing', function ($q) use ($park) {
                    return $q->where( 'park_id', $park);
                });
            });
        */
        $records = $this->paginateCollection( $records, new CommitmentExcelTransformer() );


        $query_data_2 = $records->getCollection()->toArray();

        $resource = [];

        $flag = false;
        foreach ($query_data_2 as $datam) {
            foreach ( $datam as $datum ) {
                if ( isset( $datam['programming_date'] ) && Carbon::parse( $datam['programming_date'] )->year == 2019 ) {
                    $flag = true;
                }
                $resource[] = $datum;
            }
        }
        $count_latest = 0;
        if ( $flag ) {
            $latest_records = $this->table( new FollowUpOnCommitment(), new FollowingUpOnCommitmentTransformer(), false);
            $latest_records = $latest_records->when( count($user_id) > 0, function ($query) use ( $user_id ) {
                return $query->whereIn('user_id', $user_id);
            })
                ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                    return $query->whereBetween( 'date', [$start_date, $final_date] );
                })
                ->when( isset($park), function ($query) use ( $park ) {
                    return $query->where( 'park_id', $park );
                });
            $latest_records = $this->paginateCollection( $latest_records, new FollowingUpOnCommitmentTransformer() );
            $query_data_1 = $latest_records->getCollection()->toArray();

            foreach ($query_data_1 as $datam) {
                foreach ( $datam as $datum ) {
                    $resource[] = $datum;
                }
            }

            $count_latest = $latest_records->perPage();
        }


        return response()->json([
            'data'  => new LengthAwarePaginator(
                        $resource,
                        (int) $records->total() + (int) FollowUpOnCommitment::count(),
                (int) $records->perPage() + $count_latest,
                (int) $records->currentPage(),
                [
                    'path' => request()->url(),
                    'query' => [
                        'page' => (int) $records->currentPage()
                    ]
                ]
            ),
            'code'  =>  200
        ], 200);
    }

    public function store(Requests\Schedule\FollowUpOnCommitmentRequest $request)
    {
        $data = new FollowUpOnCommitment();
        $data->fill( $request->all() );
        $data->user_id = auth()->user()->id;
        $data->save();

        return response()->json([
            'data'  =>  trans('validation.handler.success'),
            'code'  =>  200
        ], 200);
    }

    public function destroy(FollowUpOnCommitment $commitment)
    {
        $commitment->tracings()->forceDelete();
        if ( $commitment->forceDelete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }

    public function excel()
    {
        Excel::load( public_path('excel/SEGUIMIENTOS.xlsx'), function ($file) {
            $file->sheet(0, function ($sheet) {
                $data = $this->getRecords();
                $row = 7;
                $sheet->cell("C2", function($cell)  {
                    $start_date = request()->has('start_date') ? request()->get('start_date') : Carbon::now()->startOfMonth()->format('Y-m-d');
                    $final_date = request()->has('final_date') ? request()->get('final_date') : Carbon::now()->endOfMonth()->format('Y-m-d');
                    if ( isset($start_date, $final_date) ) {
                        $cell->setValue( "DEL $start_date AL $final_date" );
                    } else {
                        $cell->setValue( "TODO" );
                    }
                });
                $sheet->cell("C3", function($cell)  {
                    $cell->setValue( Carbon::now()->format('Y-m-d H:i:s') );
                });
                $sheet->cell("C4", function($cell)  {
                    $cell->setValue( isset(  auth()->user()->profile->full_name ) ?  auth()->user()->profile->full_name : '' );
                });

                foreach ( $data as $datum ) {
                    $tracings = $datum['tracings'];
                    $files  = $datum['files'];
                    $images = $datum['images'];
                    unset($datum['tracings']);
                    unset($datum['files']);
                    unset($datum['images']);
                    $sheet->row($row, $datum);
                    $sheet->cell("T$row", function($cell) use($files){
                        $cell->setValue( $files );
                    });
                    $sheet->cell("U$row", function($cell) use($images){
                        $cell->setValue( $images );
                    });
                    foreach ($tracings as $tracing) {
                        $sheet->cell("P$row", function($cell) use($tracing){
                            $cell->setValue( isset($tracing['id']) ? $tracing['id'] : '' );
                        });
                        $sheet->cell("Q$row", function($cell) use($tracing){
                            $cell->setValue( isset($tracing['date']) ? $tracing['date'] : '' );
                        });
                        $sheet->cell("R$row", function($cell) use($tracing){
                            $cell->setValue( isset($tracing['description']) ? $tracing['description'] : '' );
                        });
                        $sheet->cell("S$row", function($cell) use($tracing){
                            $cell->setValue( isset($tracing['created_at']) ? $tracing['created_at'] : '' );
                        });
                        $row++;
                    }
                    if ( count($tracings) > 0 ) {
                        $row = $row - 1;
                    }
                    $row++;
                }

                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A6:U$sum")->applyFromArray($styleArray);
            });
        })->export('xlsx');
    }

    public function getRecords2()
    {
        $start_date = request()->has('start_date') ? request()->get('start_date') : Carbon::now()->startOfMonth()->format('Y-m-d');
        $final_date = request()->has('final_date') ? request()->get('final_date') : Carbon::now()->endOfMonth()->format('Y-m-d');
        $park = request()->has('park_id') ? request()->get('park_id') : null;
        $user_id = ( request()->has( 'professional' ) && count(request()->get('professional')) > 0 ) ? request()->get('professional') :[];
        $types = \request()->has('type_id') ? request()->get('type_id') : [
            16, // Mesas de trabajo
            17, // Acuerdos ciudadanos
            18, // Pactos ciudadanos
            31, // Firmas de acuerdos ciudadanos
        ];

        $data = FollowUpOnCommitment::when( count($user_id) > 0, function ($query) use ( $user_id ) {
                return $query->whereIn('user_id', $user_id);
            })
            ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                return $query->whereBetween( 'date', [$start_date, $final_date] );
            })
            ->when( isset($park), function ($query) use ( $park ) {
                return $query->where( 'park_id', $park );
            })->get();
        $data = $data->map( function ( $item ) {
            return ( new FollowingUpOnCommitmentTransformer() )->transform( $item );
        })->toArray();

        $programmings = ProgrammingActivities::query()->whereIn('activity_id', $types)->get(['programming_id'])->toArray();
        $merge = ProgrammingCommitment::whereIn('programming_id', $programmings)
            ->when( count($user_id) > 0, function ($query) use ( $user_id ) {
                return $query->whereHas('programing', function ($q) use ($user_id) {
                    return $q->whereIn('user_id', $user_id);
                });
            })
            ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                //return $query->whereBetween( 'date', [$start_date, $final_date] );
                return $query->whereHas('programing', function ($q) use ($start_date, $final_date) {
                    return $q->whereDate( 'execution_date', '>=', $start_date)
                              ->whereDate( 'execution_date', '<=', $final_date);
                });
            })
            ->when( isset($park), function ($query) use ( $park ) {
                return $query->whereHas('programing', function ($q) use ($park) {
                    return $q->where( 'park_id', $park);
                });
            })->get();
        $merge = $merge->map( function ( $item ) {
            return ( new CommitmentExcelTransformer() )->transform( $item );
        })->toArray();

        $merged = array_merge($merge, $data);
        $merged = collect( $merged )->sortBy('program_date')->reverse()->toArray();
        $array = [];
        foreach ($merged as $datum) {
            $array[] = [
                'id'            =>  $datum['id'],
                'park'          =>  toUpper($datum['park']),
                'park_code'     =>  toUpper($datum['park_code']),
                'location'      =>  toUpper($datum['location']),
                'upz'           =>  toUpper($datum['upz']),
                'program_date'  =>  isset( $datum['programming_date'] ) ? $datum['programming_date'] : null,
                'program_desc'  =>  isset( $datum['programming_desc'] ) ? $datum['programming_desc'] : null,
                'type'          =>  isset( $datum['programming_act'] ) ? $datum['programming_act'] : null,
                'commitment_id' =>  isset( $datum['commitment_id'] ) ? $datum['commitment_id'] : null,
                'date'          =>  isset( $datum['date'] ) ? $datum['date'] : null,
                'status'        =>  toUpper($datum['status']),
                'professional_name' => toUpper($datum['professional_name']),
                'description'   =>  toUpper($datum['description']),
                'responsable'   =>  toUpper($datum['responsable']),
                'created_at'    =>  $datum['created_at'],
                'tracings'      =>  $datum['tracings'],
                'files'         =>  isset( $datum['files'] ) ? $datum['files'] : null,
                'images'        =>  isset( $datum['images'] ) ? $datum['images'] : null,
            ];
        }
        return $array;
    }

    public function getRecords()
    {
        $start_date = request()->has('start_date') ? request()->get('start_date') : Carbon::now()->subMonth()->startOfMonth()->format('Y-m-d');
        $final_date = request()->has('final_date') ? request()->get('final_date') : Carbon::now()->subMonth()->endOfMonth()->format('Y-m-d');
        $park = request()->has('park_id') ? request()->get('park_id') : null;
        $user_id = ( request()->has( 'professional' ) && count(request()->get('professional')) > 0 ) ? request()->get('professional') :[];
        $types = \request()->has('type_id') ? request()->get('type_id') : [
            16, // Mesas de trabajo
            17, // Acuerdos ciudadanos
            18, // Pactos ciudadanos
            31, // Firmas de acuerdos ciudadanos
        ];

        $data = FollowUpOnCommitment::when( count($user_id) > 0, function ($query) use ( $user_id ) {
            return $query->whereIn('user_id', $user_id);
        })
            ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                return $query->whereBetween( 'date', [$start_date, $final_date] );
            })
            ->when( isset($park), function ($query) use ( $park ) {
                return $query->where( 'park_id', $park );
            })->get();
        $data = $data->map( function ( $item ) {
            return ( new FollowingUpOnCommitmentTransformer() )->transform( $item );
        })->toArray();

        $programmings = ProgrammingActivities::query()->whereIn('activity_id', $types)->get(['programming_id'])->toArray();
        $merge = ProgrammingView::whereIn('id', $programmings)
            ->when( count($user_id) > 0, function ($query) use ( $user_id ) {
                return $query->whereIn('user_id', $user_id);
            })
            ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                return $query->whereDate( 'execution_date', '>=', $start_date)
                    ->whereDate( 'execution_date', '<=', $final_date);
            })
            ->when( isset($park), function ($query) use ( $park ) {
                return $query->where( 'park_id', $park);
            })->get();

        $merge = $merge->map( function ( $item ) {
            return ( new CommitmentExcelTransformer() )->transform( $item );
        })->toArray();

        $merged = array_merge($merge, $data);
        $merged = collect( $merged )->sortBy('program_date')->reverse()->toArray();
        $array = [];

        foreach ($merged as $datam) {
            foreach ( $datam as $datum ) {
                $array[] = [
                    'id'            =>  $datum['id'],
                    'park'          =>  toUpper($datum['park']),
                    'park_code'     =>  toUpper($datum['park_code']),
                    'location'      =>  toUpper($datum['location']),
                    'upz'           =>  toUpper($datum['upz']),
                    'program_date'  =>  isset( $datum['programming_date'] ) ? $datum['programming_date'] : null,
                    'program_desc'  =>  isset( $datum['programming_desc'] ) ? $datum['programming_desc'] : null,
                    'type'          =>  isset( $datum['programming_act'] ) ? $datum['programming_act'] : null,
                    'commitment_id' =>  isset( $datum['commitment_id'] ) ? $datum['commitment_id'] : null,
                    'date'          =>  isset( $datum['date'] ) ? $datum['date'] : null,
                    'status'        =>  toUpper($datum['status']),
                    'professional_name' => toUpper($datum['professional_name']),
                    'description'   =>  toUpper($datum['description']),
                    'responsable'   =>  toUpper($datum['responsable']),
                    'created_at'    =>  $datum['created_at'],
                    'tracings'      =>  $datum['tracings'],
                    'files'         =>  isset( $datum['files'] ) ? $datum['files'] : null,
                    'images'        =>  isset( $datum['images'] ) ? $datum['images'] : null,
                ];
            }
        }
        return $array;
    }
}
