<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\CitizenAttentionSchedule;
use IDRDApp\Transformers\Schedule\CitizenAttentionScheduleTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class CitizenAttentionScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resource = new Collection( CitizenAttentionSchedule::all(), new CitizenAttentionScheduleTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }
}
