<?php

namespace IDRDApp\Http\Controllers\Schedule;

use IDRDApp\Entities\Schedule\Summoner;
use IDRDApp\Transformers\Schedule\SummonerTransformer;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class SummonerController extends Controller
{
    public function index()
    {
        $resource = new Collection(Summoner::query()->whereNotNull('summoner')->get(), new SummonerTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }
}
