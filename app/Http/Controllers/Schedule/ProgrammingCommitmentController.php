<?php


namespace IDRDApp\Http\Controllers\Schedule;


use Carbon\Carbon;
use IDRDApp\Entities\Schedule\Programming;
use IDRDApp\Entities\Schedule\ProgrammingCommitment;
use IDRDApp\Http\Controllers\Controller;
use IDRDApp\Http\Requests\StoreProgrammingCommitmentRequest;
use IDRDApp\Transformers\Schedule\CommitmentTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ProgrammingCommitmentController extends Controller
{
    public function index(Programming $programming)
    {
        $data = ProgrammingCommitment::query()->where('programming_id', $programming->id)->get();

        $resource = new Collection( $data, new CommitmentTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200);
    }

    /**
     * Display a listing of the resource to the calendar.
     *
     * @param Request $request
     * @return Response
     */
    public function calendar(Request $request)
    {
        $start   = $request->has( 'start' ) ? $request->get('start') : Carbon::now()->startOfMonth();
        $end     = $request->has( 'end' )   ? $request->get('end')   : Carbon::now()->endOfMonth();
        $user_id = $request->has( 'professional' )   ? $request->get('professional') : auth()->user()->id;
        $is_general = $request->has('general') ? $request->get('general') : false;

        $data = ProgrammingCommitment::query()->whereHas('programing', function ($query) use ($user_id, $is_general) {
            return $query->when( ! $is_general, function ($query) use ($user_id) {
                return $query->where('user_id', $user_id );
            });
        })->whereBetween( 'date', [$start, $end] )->get();

        $resource = new Collection( $data, new CommitmentTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }

    /**
     * @param StoreFileRequest $request
     * @param Programming $programming
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProgrammingCommitmentRequest $request, Programming $programming)
    {
        $programming->programming_commitments()->create([
            'programming_id'    =>  $programming->id,
            'responsable'       =>  $request->get('responsable'),
            'date'              =>  $request->get('date'),
            'description'       =>  $request->get('description')
        ]);

        return response()->json([
            'data'  =>  trans('validation.handler.success'),
            'code'  =>  200
        ], 200);
    }


    public function destroy(Programming $programming, ProgrammingCommitment $commitment)
    {
        if ( $commitment->delete() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
    }
}