<?php

namespace IDRDApp\Http\Controllers\Schedule;

use Carbon\Carbon;
use IDRDApp\Entities\Schedule\CommunityAttention;
use IDRDApp\Entities\Schedule\CommunityAttentionView;
use IDRDApp\Entities\Schedule\CommunityFile;
use IDRDApp\Http\Requests\Schedule\StoreCommunityAttentionRequest;
use IDRDApp\Transformers\Schedule\CommunityAttentionViewTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class CommunityAttentionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $user_id = request()->has( 'professional' ) ? request()->get('professional') : auth()->user()->id;
        $records = $this->table( new CommunityAttentionView(), new CommunityAttentionViewTransformer(), false);
        $records = $records->where('user_id', $user_id);
        $records = $this->paginateCollection( $records, new CommunityAttentionViewTransformer() );
        return response()->json([
            'data'  =>  $records,
            'code'  =>  200
        ], 200);
    }

    /**
     * Display a listing of the resource to the calendar.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function calendar(Request $request)
    {
        $start   = $request->has( 'start' ) ? $request->get('start') : Carbon::now()->startOfMonth();
        $end     = $request->has( 'end' )   ? $request->get('end')   : Carbon::now()->endOfMonth();
        $user_id = $request->has( 'professional' )   ? $request->get('professional') : auth()->user()->id;
        $is_general = $request->has('general') ? $request->get('general') : false;

        $data = CommunityAttentionView::query()
            ->whereBetween( 'execution_date', [$start, $end] )
            ->whereNull('deleted_at')
            ->when( ! $is_general, function ($query) use ($user_id) {
                return $query->where('user_id', $user_id );
            })
            ->get();

        $resource = new Collection( $data, new CommunityAttentionViewTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCommunityAttentionRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(StoreCommunityAttentionRequest $request)
    {
        $resource = new CommunityAttention();
        $resource->execution_date   = $request->get('date');
        $resource->initial_hour     = $request->get('initial_hour');
        $resource->final_hour       = $request->get('final_hour');
        $resource->user_id          = auth()->user()->id;
        if ( $resource->saveOrFail() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  =>  201
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param CommunityAttentionView $attention
     * @return JsonResponse
     */
    public function show(CommunityAttentionView $attention)
    {
        $resource = new Item($attention, new CommunityAttentionViewTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray(), 200 );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param CommunityAttention $attention
     * @return JsonResponse
     */
    public function destroy(CommunityAttention $attention)
    {
        $attention->files()->chunk(10, function ($files) {
            foreach ( $files as $file ) {
                $path = $file->getOriginal('path');
                if ( isset( $path ) ) {
                    if ( file_exists( public_path(CommunityFile::FILE_PATH."/".$file->getOriginal('path')) ) ) {
                        unlink(  public_path(CommunityFile::FILE_PATH."/".$file->getOriginal('path')) );
                    }
                }
            }
        });

        $attention->files()->forceDelete();
        $attention->forceDelete();
        return \response()->json([
            'data'      =>  "Programming { $attention->id } was force deleted successfully",
            'details'   =>  $attention,
            'code'      =>  204
        ], 200);
    }
}
