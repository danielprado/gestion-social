<?php

namespace IDRDApp\Http\Controllers\Reports;

use IDRDApp\Entities\Schedule\ProgrammingView;
use IDRDApp\Transformers\Schedule\ProgrammingViewTransformer;
use IDRDApp\Transformers\Schedule\SummaryProgrammingViewTransformer;
use Illuminate\Http\Request;

use IDRDApp\Http\Requests;
use IDRDApp\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class SummaryController extends Controller
{
    public function index()
    {
        $park_id    = request()->has('park_id') ? request()->get('park_id') : null;
        $park = request()->has('park') ? request()->get('park') : null;
        $start_date = request()->has('initial_date') ? request()->get('initial_date') : null;
        $final_date = request()->has('final_date') ? request()->get('final_date') : null;
        $professional = request()->has('professional') ? request()->get('professional') : null;
        $trashed    = ( request()->has('trashed') && request()->get('trashed') == 'true' ) ? true : false;

        $records = $this->table( new ProgrammingView(), new SummaryProgrammingViewTransformer(), false);
        $records = $records
                        ->when( $professional, function ($query) use ($professional) {
                            return $query->where('user_id', $professional);
                        })
                        ->when( $park_id, function ($query) use ($park_id) {
                            return $query->where('park_id', $park_id);
                        })
                        ->when( $park, function ($query) use ($park) {
                            return $query->where('park', 'LIKE', "%$park%")->whereNull('park_id');
                        })
                        ->when( isset($start_date, $final_date), function ($query) use ( $start_date, $final_date ) {
                            return $query->whereBetween( 'execution_date', [$start_date, $final_date] );
                        })
                        ->when( $trashed, function ($query) {
                            return $query->whereNotNull( 'deleted_at');
                        });

        $records = $this->paginateCollection( $records, new SummaryProgrammingViewTransformer() );
        return response()->json([
            'data'  =>  $records,
            'code'  =>  200
        ], 200);
    }
}
