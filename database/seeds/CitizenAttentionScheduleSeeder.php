<?php

use IDRDApp\Entities\Schedule\CitizenAttentionSchedule;
use Illuminate\Database\Seeder;

class CitizenAttentionScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schedules = [
            ['schedule' =>  '7:00 - 16:30'],
            ['schedule' =>  '7:00 - 12:00'],
            ['schedule' =>  '12:00 - 16:30']
        ];

        foreach ($schedules as $schedule) {
            CitizenAttentionSchedule::create( $schedule );
        }
    }
}
