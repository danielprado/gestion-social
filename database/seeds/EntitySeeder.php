<?php

use IDRDApp\Entities\Schedule\Entity;
use Illuminate\Database\Seeder;

class EntitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entities = [
            ['entity'   =>   'ENTIDAD DISTRITAL'],
            ['entity'   =>   'ENTIDAD PRIVADA'],
            ['entity'   =>   'JAC'],
            ['entity'   =>   'ESCUELA O CLUB DEPORTIVO'],
            ['entity'   =>   'COMUNIDAD'],
            ['entity'   =>   'PLANTEL EDUCATIVO'],
            ['entity'   =>   'POLICÍA'],
            ['entity'   =>   'FUNCIONARIO IDRD'],
            ['entity'   =>   'FUNCIONARIO OTRAS INSTITUCIONES'],
            ['entity'   =>   'OTRO'],
            ['entity'   =>   'N/A']
        ];

        foreach ($entities as $entity) {
            Entity::create($entity);
        }
    }
}
