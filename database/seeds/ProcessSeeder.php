<?php

use IDRDApp\Entities\Schedule\Process;
use Illuminate\Database\Seeder;

class ProcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $processes = [
            ['process' =>   'CAMPAÑA CULTURA CIUDADANA'],
            ['process' =>   'SOSTENIBILIDAD FISICA'],
            ['process' =>   'SOSTENIBILIDAD SOCIAL'],
            ['process' =>   'MAPA DE RIESGOS'],
            ['process' =>   'OTROS'],
        ];
        foreach ( $processes as $process ) {
            Process::create( $process );
        }
    }
}
