<?php

use IDRDApp\Entities\Schedule\TypeOfPopulation;
use Illuminate\Database\Seeder;

class TypeOfPopulationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['type_of_population'   =>   'AFRODECENDIENTES'],
            ['type_of_population'   =>   'ARTESANOS'],
            ['type_of_population'   =>   'COMUNIDAD GENERAL'],
            ['type_of_population'   =>   'COMUNIDAD INDÍGENA'],
            ['type_of_population'   =>   'DOCENTES']
        ];
        foreach ($types as $type) {
            TypeOfPopulation::create( $type );
        }
    }
}
