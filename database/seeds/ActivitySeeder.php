<?php

use IDRDApp\Entities\Schedule\Activity;
use IDRDApp\Entities\Schedule\Process;
use Illuminate\Database\Seeder;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activities = [
            //--------------- CAMPAÑA CULTURA CIUDADANA --------------//
            [
                'activity'  =>  'EVALUACION DE SATISFACIÓN',
                'require_text'  =>  true,
                'process'   =>  'CAMPAÑA CULTURA CIUDADANA'
            ],
            [
                'activity'  =>  'ZONAS CANINAS',
                'require_text'  =>  false,
                'process'   =>  'CAMPAÑA CULTURA CIUDADANA'
            ],
            [
                'activity'  =>  'LECTURA AL PARQUE',
                'require_text'  =>  false,
                'process'   =>  'CAMPAÑA CULTURA CIUDADANA'
            ],
            [
                'activity'  =>  'VENDEDORES DE MÓDULOS CONSTRUIDOS',
                'require_text'  =>  false,
                'process'   =>  'CAMPAÑA CULTURA CIUDADANA'
            ],
            [
                'activity'  =>  'JORNADA DE EMBELLECIMIENTO',
                'require_text'  =>  false,
                'process'   =>  'CAMPAÑA CULTURA CIUDADANA'
            ],
            [
                'activity'  =>  'BECAS EN PARQUES',
                'require_text'  =>  false,
                'process'   =>  'CAMPAÑA CULTURA CIUDADANA'
            ],

            //--------------- SOSTENIBILIDAD FÍSICA --------------//
            [
                'activity'  =>  'CANCHAS INICIO',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],
            [
                'activity'  =>  'CANCHAS AVANCE',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],
            [
                'activity'  =>  'CANCHAS FINALIZACION',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],
            [
                'activity'  =>  'CANCHAS COMITÉ DE PARTICIPACIÓN CIUDADANA',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],
            [
                'activity'  =>  'MANTENIEMIENTO COMBOS INICIO',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],
            [
                'activity'  =>  'MANTENIMIENTO COMBOS AVANCE',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],
            [
                'activity'  =>  'MANTENIMIENTO COMBOS FINALIZACIÓN',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],
            [
                'activity'  =>  'MANTENIMIENTO COMITÉ DE PARTICIPACIÓN',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],
            [
                'activity'  =>  'EVALUACION DE SATISFACCIÓN',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD FISICA'
            ],

            //------- SOSTENIBILIDAD SOCIAL ---------//
            [
                'activity'  =>  'MESA DE TRABAJO',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD SOCIAL'
            ],
            [
                'activity'  =>  'ACUERDOS CIUDADANOS',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD SOCIAL'
            ],
            [
                'activity'  =>  'PACTOS CIUDADANOS',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD SOCIAL'
            ],
            [
                'activity'  =>  'DIÁLOGOS CIUDADANOS',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD SOCIAL'
            ],
            [
                'activity'  =>  'JORNADAS DE EMBELLECIMIENTO',
                'require_text'  =>  false,
                'process'   =>  'SOSTENIBILIDAD SOCIAL'
            ],

            //------- MAPA DE RIESGOS ---------//
            [
                'activity'  =>  'PREVENCIÓN DE RIESGOS',
                'require_text'  =>  false,
                'process'   =>  'MAPA DE RIESGOS'
            ],

            //------- OTROS ---------//

            [
                'activity'  =>  'CONCEPTO SOCIAL DE USO TEMPORAL',
                'require_text'  =>  false,
                'process'   =>  'OTROS'
            ],
            [
                'activity'  =>  'DIAGNÓSTICO',
                'require_text'  =>  false,
                'process'   =>  'OTROS'
            ],
            [
                'activity'  =>  'CITACIONES ORFEO',
                'require_text'  =>  false,
                'process'   =>  'OTROS'
            ],
        ];
        foreach ($activities as $activity) {
            $process = Process::query()->where('process', $activity['process'])->first();
            Activity::create([
                'activity'      =>  $activity['activity'],
                'require_text'  =>  $activity['require_text'],
                'process_id'    =>  $process->id
            ]);
        }
    }
}
