<?php

use IDRDApp\Entities\Schedule\RequestType;
use Illuminate\Database\Seeder;

class RequestTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request_types = [
            ['request_type' =>  'VERBAL'],
            ['request_type' =>  'ESCRITA'],
            ['request_type' =>  'EMAIL'],
            ['request_type' =>  'CITACIONES DE ORFEO'],
            ['request_type' =>  'WHATSAPP']
        ];
        foreach ( $request_types as $request_type ) {
            RequestType::create( $request_type );
        }
    }
}
