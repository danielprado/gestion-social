<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call( ProcessSeeder::class );
        // $this->call( RequestTypeSeeder::class );
        // $this->call( SummonerSeeder::class );
        // $this->call( CitizenAttentionScheduleSeeder::class );
        // $this->call( ReunionTypeSeeder::class );
        // $this->call( ActivitySeeder::class );
        // $this->call( ThemeSeeder::class );
        // $this->call(EntitySeeder::class);
        // $this->call(ConditionSeeder::class);
        // $this->call(SituationSeeder::class);
        // $this->call(TypeOfPopulationSeeder::class);
        // $this->call(FileTypeSeeder::class);
    }
}
