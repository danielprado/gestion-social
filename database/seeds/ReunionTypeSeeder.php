<?php

use IDRDApp\Entities\Schedule\ReunionType;
use Illuminate\Database\Seeder;

class ReunionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [ 'reunion_type'    =>  'REUNIÓN INTRAINSTITUCIONAL', 'require_process' =>  false ],
            [ 'reunion_type'    =>  'REUNIÓN INTERINSTITUCIONAL', 'require_process' =>  true ],
            [ 'reunion_type'    =>  'REUNIÓN COMUNITARIA', 'require_process' =>  true ],
        ];

        foreach ( $types as $type ) {
            ReunionType::create( $type );
        }
    }
}
