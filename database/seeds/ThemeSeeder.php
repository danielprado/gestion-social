<?php

use IDRDApp\Entities\Schedule\Theme;
use Illuminate\Database\Seeder;

class ThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $themes = [
            ['theme'  =>  'SEGURIDAD'],
            ['theme'  =>  'ESTABLECIMIENTOS EDUCATIVOS'],
            ['theme'  =>  'MANEJO DE RESIDUOS'],
            ['theme'  =>  'CONCERTACIÓN DE HORARIOS'],
            ['theme'  =>  'DONACIÓN'],
            ['theme'  =>  'HABITANTES DE CALLE'],
            ['theme'  =>  'ARBOLADO'],
            ['theme'  =>  'ANIMALES DE COMPAÑÍA'],
            ['theme'  =>  'RESOLUCIÓN DE CONFLICTOS'],
            ['theme'  =>  'ANIMALES SILVESTRES'],
            ['theme'  =>  'CONSUMOS SPA'],
            ['theme'  =>  'OTROS']
        ];

        foreach ($themes as $theme) {
            Theme::create( $theme );
        }
    }
}
