<?php

use IDRDApp\Entities\Schedule\Situation;
use Illuminate\Database\Seeder;

class SituationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $situations = [
            ['situation'   =>  'DESMOVILIZADOS'],
            ['situation'   =>  'DESPLAZADOS'],
            ['situation'   =>  'HABITANTES DE CALLE'],
            ['situation'   =>  'MENOR TRABAJADOR'],
            ['situation'   =>  'NIÑOS DESESCOLARIZADOS'],
            ['situation'   =>  'NIÑOS EN PROTECCIÓN'],
            ['situation'   =>  'REINCORPORADOS'],
            ['situation'   =>  'REUBICADOS'],
            ['situation'   =>  'VÍCTIMAS DE CONFLICTO'],
            ['situation'   =>  'N/A']
        ];

        foreach ($situations as $situation) {
            Situation::create($situation);
        }
    }
}
