<?php

use IDRDApp\Entities\Schedule\Summoner;
use Illuminate\Database\Seeder;

class SummonerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $summoners = [
            [
                'summoner' =>  'COMUNIDAD',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'JAL',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'ESCUELA',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'ADMINISTRADOR',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'AUXILIAR',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'ENTIDAD DISTRITAL',
                'additional_input'  =>  1
            ],
            [
                'summoner' =>  'ENTIDAD NACIONAL',
                'additional_input'  =>  1
            ],
            [
                'summoner' =>  'SUPERVISORA',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'GERENTES DE ZONA',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'ASUNTOS LOCALES',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'PROFESIONAL SOCIAL',
                'additional_input'  =>  0
            ],
            [
                'summoner' =>  'OTRO',
                'additional_input'  =>  1
            ],
        ];
        foreach ( $summoners as $summoner ) {
            Summoner::create( $summoner );
        }
    }
}
