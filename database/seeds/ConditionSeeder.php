<?php

use IDRDApp\Entities\Schedule\Condition;
use Illuminate\Database\Seeder;

class ConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conditions = [
            ['condition'  =>  'LIMITACIÓN AUDITIVA'],
            ['condition'  =>  'LIMITACIÓN COGNITIVA'],
            ['condition'  =>  'LIMITACIÓN FÍSICA'],
            ['condition'  =>  'LIMITACIÓN MENTAL'],
            ['condition'  =>  'LIMITACIÓN MULTIPLE'],
            ['condition'  =>  'LIMITACIÓN VISUAL'],
            ['condition'  =>  'LIMITACIÓN AUDITIVA Y VISUAL'],
            ['condition'  =>  'N/A'],
        ];

        foreach ($conditions as $condition) {
            Condition::create($condition);
        }
    }
}
