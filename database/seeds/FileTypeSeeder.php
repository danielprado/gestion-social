<?php

use IDRDApp\Entities\Schedule\FileType;
use Illuminate\Database\Seeder;

class FileTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $files = [
            ['file_type'    =>    'ACTA DE REUNIÓN'],
            ['file_type'    =>    'ACUERDO CIUDADANO'],
            ['file_type'    =>    'OTRO']
        ];
        foreach ($files as $file) {
            FileType::create( $file );
        }
    }
}
