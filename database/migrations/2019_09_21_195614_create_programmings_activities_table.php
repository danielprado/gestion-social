<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammingsActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmings_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('programming_id');
            $table->unsignedInteger('activity_id');
            $table->string('description', 191)->nullable();
            $table->timestamps();
            $table->foreign('programming_id')->references('id')->on('programmings');
            $table->foreign('activity_id')->references('id')->on('activities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programmings_activities');
    }
}
