<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCitizenAttentionViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $db_name = env('DB_DATABASE');
        DB::statement("
        CREATE VIEW `{$db_name}`.`tbl_citizen_attention_view` AS SELECT
        {$db_name}.tbl_citizen_attentions.id,
        {$db_name}.tbl_citizen_attentions.execution_date,
        {$db_name}.tbl_citizen_attentions.citizen_attention_schedule_id,
        {$db_name}.tbl_citizen_attention_schedules.`schedule` AS citizen_attention_schedule,
        {$db_name}.tbl_citizen_attentions.user_id,
        {$db_name}.tbl_citizen_attentions.who_cancel,
        {$db_name}.tbl_citizen_attentions.reason_for_cancellation,
        {$db_name}.tbl_citizen_attentions.created_at,
        {$db_name}.tbl_citizen_attentions.updated_at,
        {$db_name}.tbl_citizen_attentions.deleted_at,
        UCASE( CONCAT(  COALESCE(idrdgov_simgeneral.persona.Primer_Nombre, ''), ' ', COALESCE(idrdgov_simgeneral.persona.Segundo_Nombre, ''), ' ', COALESCE(idrdgov_simgeneral.persona.Primer_Apellido, ''), ' ', COALESCE(idrdgov_simgeneral.persona.Segundo_Apellido, '')  ) ) AS full_name,
        idrdgov_simgeneral.persona.Primer_Nombre AS `name`,
        idrdgov_simgeneral.persona.Segundo_Nombre AS middle_name,
        idrdgov_simgeneral.persona.Primer_Apellido AS last_name,
        idrdgov_simgeneral.persona.Segundo_Apellido AS second_last_name,
        idrdgov_simgeneral.persona.Cedula AS document
        FROM
        {$db_name}.tbl_citizen_attentions
        JOIN {$db_name}.tbl_citizen_attention_schedules
        ON {$db_name}.tbl_citizen_attentions.citizen_attention_schedule_id = {$db_name}.tbl_citizen_attention_schedules.id 
        JOIN idrdgov_simgeneral.persona
        ON {$db_name}.tbl_citizen_attentions.user_id = idrdgov_simgeneral.persona.Id_Persona
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $db_name = env('DB_DATABASE');
        DB::statement("DROP VIEW `{$db_name}`.`tbl_citizen_attention_view`");
    }
}
