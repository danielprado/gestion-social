<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammingsCommitmentsTracingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmings_commitments_tracings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('commitment_id');
            $table->date('date');
            $table->text('description');
            $table->timestamps();
            $table->foreign('commitment_id')->references('id')->on('programmings_commitments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programmings_commitments_tracings');
    }
}
