<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowUpOnCommitmentsTracingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follow_up_on_commitments_tracing', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->text('description');
            $table->unsignedInteger('follow_commitment_id');
            $table->timestamps();
            $table->foreign('follow_commitment_id')->references('id')->on('follow_up_on_commitments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('follow_up_on_commitments_tracing');
    }
}
