<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('programming_id');
            $table->unsignedInteger('entity_id');
            $table->unsignedInteger('type_of_population_id');
            $table->unsignedInteger('condition_id');
            $table->unsignedInteger('situation_id');
            $table->unsignedInteger('f_0_5');
            $table->unsignedInteger('m_0_5');
            $table->unsignedInteger('f_6_12');
            $table->unsignedInteger('m_6_12');
            $table->unsignedInteger('f_13_17');
            $table->unsignedInteger('m_13_17');
            $table->unsignedInteger('f_18_26');
            $table->unsignedInteger('m_18_26');
            $table->unsignedInteger('f_27_59');
            $table->unsignedInteger('m_27_59');
            $table->unsignedInteger('f_60_more');
            $table->unsignedInteger('m_60_more');
            $table->text('observation')->nullable();
            $table->timestamps();
            $table->foreign('programming_id')->references('id')->on('programmings');
            $table->foreign('entity_id')->references('id')->on('entities');
            $table->foreign('type_of_population_id')->references('id')->on('types_of_populations');
            $table->foreign('condition_id')->references('id')->on('conditions');
            $table->foreign('situation_id')->references('id')->on('situations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('executions');
    }
}
