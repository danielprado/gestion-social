<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammingsThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmings_themes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('programming_id');
            $table->unsignedInteger('theme_id');
            $table->text('other')->nullable();
            $table->timestamps();

            $table->foreign('programming_id')->references('id')->on('programmings');
            $table->foreign('theme_id')->references('id')->on('themes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programmings_themes');
    }
}
