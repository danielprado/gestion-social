<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammingImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programming_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('programming_id');
            $table->string('file_name', 191)->nullable();
            $table->text('path')->nullable();
            $table->boolean('confirmation')->default( false );
            $table->text('description')->nullable();
            $table->timestamps();
            $table->foreign('programming_id')->references('id')->on('programmings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programming_images');
    }
}
