<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenAttentionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizen_attentions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('execution_date');
            $table->unsignedInteger('citizen_attention_schedule_id');
            $table->unsignedInteger('user_id');
            $table->string('who_cancel', 191)->nullable();
            $table->text('reason_for_cancellation')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('citizen_attention_schedule_id')
                        ->references('id')
                        ->on('citizen_attention_schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('citizen_attentions');
    }
}
