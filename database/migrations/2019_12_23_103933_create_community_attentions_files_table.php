<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityAttentionsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('community_id');
            $table->string('file_name', 191);
            $table->text('path');
            $table->timestamps();
            $table->foreign('community_id')->references('id')->on('community_attentions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('community_files');
    }
}
