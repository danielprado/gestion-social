<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammingsCommitmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmings_commitments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('programming_id');
            $table->date('date');
            $table->string('responsable', 191);
            $table->text('description');
            $table->timestamps();
            $table->foreign('programming_id')->references('id')->on('programmings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programmings_commitments');
    }
}
