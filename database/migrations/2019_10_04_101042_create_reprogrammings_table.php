<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReprogrammingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reprogrammings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->time('initial_hour');
            $table->time('final_hour');
            $table->text('reason')->nullable();
            $table->unsignedBigInteger('programming_id');
            $table->timestamps();
            $table->foreign('programming_id')->references('id')->on('programmings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reprogrammings');
    }
}
