<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReprogrammingCitizenAttentionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reprogramming_citizen_attentions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->text('reason')->nullable();
            $table->unsignedBigInteger('citizen_attention_id');
            $table->unsignedInteger('schedule_id');
            $table->timestamps();
            $table->foreign('citizen_attention_id')->references('id')->on('citizen_attentions');
            $table->foreign('schedule_id')
                ->references('id')
                ->on('citizen_attention_schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reprogramming_citizen_attentions');
    }
}
