<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowUpOnCommitmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follow_up_on_commitments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('park_id')->nullable();
            $table->date('date');
            $table->string('responsable', 191);
            $table->text('description');
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('follow_up_on_commitments');
    }
}
