<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 191);
            $table->date('execution_date');
            $table->time('initial_hour');
            $table->time('final_hour');
            $table->unsignedInteger('reunion_type_id');
            $table->string('park', 191)->nullabble();
            $table->string('upz', 191)->nullable();
            $table->unsignedInteger('upz_id')->nullable();
            $table->string('place', 191);
            $table->unsignedInteger('process_id')->nullable();
            $table->unsignedInteger('park_id')->nullable();
            $table->unsignedInteger('who_summons_id');
            $table->string('which', 191)->nullable();
            $table->unsignedInteger('request_type_id');
            $table->text('objective');
            $table->string('who_cancel', 191)->nullable();
            $table->text('reason_for_cancellation')->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('process_id')
                  ->references('id')->on('processes');
            $table->foreign('who_summons_id')
                  ->references('id')->on('summoners');
            $table->foreign('request_type_id')
                  ->references('id')->on('request_types');
            $table->foreign('reunion_type_id')
                  ->references('id')->on('reunion_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programmings');
    }
}
