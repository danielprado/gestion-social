<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenAttentionFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizen_files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('citizen_id');
            $table->string('file_name', 191);
            $table->text('path');
            $table->timestamps();
            $table->foreign('citizen_id')->references('id')->on('citizen_attentions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('citizen_files');
    }
}
