<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityAttentionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_attentions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('execution_date');
            $table->time('initial_hour');
            $table->time('final_hour');
            $table->string('who_cancel', 191)->nullable();
            $table->text('reason_for_cancellation')->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('community_attentions');
    }
}
