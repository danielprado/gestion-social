<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammingFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programming_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('file_type_id');
            $table->unsignedBigInteger('programming_id');
            $table->string('file_name', 191);
            $table->text('path');
            $table->timestamps();
            $table->foreign('file_type_id')->references('id')->on('file_types');
            $table->foreign('programming_id')->references('id')->on('programmings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programming_files');
    }
}
