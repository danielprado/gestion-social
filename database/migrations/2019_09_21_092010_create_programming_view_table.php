<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateProgrammingViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $db_name = env('DB_DATABASE');
        DB::statement("
        CREATE VIEW `{$db_name}`.`tbl_programming_view` AS SELECT
        {$db_name}.tbl_programmings.id,
        {$db_name}.tbl_programmings.title,
        {$db_name}.tbl_programmings.execution_date,
        {$db_name}.tbl_programmings.initial_hour,
        {$db_name}.tbl_programmings.final_hour,
        {$db_name}.tbl_programmings.upz,
        {$db_name}.tbl_programmings.upz_id,
        {$db_name}.tbl_programmings.place,
        {$db_name}.tbl_programmings.process_id,
        COALESCE( (SELECT {$db_name}.tbl_processes.process FROM {$db_name}.tbl_processes WHERE {$db_name}.tbl_processes.id = {$db_name}.tbl_programmings.process_id LIMIT 1), '' ) AS `process`,
        {$db_name}.tbl_programmings.park_id,
        COALESCE( (SELECT idrdgov_sim_ParqueIDRD.parque.Id_IDRD FROM idrdgov_sim_ParqueIDRD.parque WHERE idrdgov_sim_ParqueIDRD.parque.Id = {$db_name}.tbl_programmings.park_id LIMIT 1), '' ) AS `park_code`,
        {$db_name}.tbl_programmings.park,
        {$db_name}.tbl_programmings.who_summons_id,
        COALESCE( (SELECT {$db_name}.tbl_summoners.summoner FROM {$db_name}.tbl_summoners WHERE {$db_name}.tbl_summoners.id = {$db_name}.tbl_programmings.who_summons_id LIMIT 1), '' ) AS `who_summons`,
        {$db_name}.tbl_programmings.which,
        {$db_name}.tbl_programmings.user_id,
        UCASE( CONCAT(  COALESCE(idrdgov_simgeneral.persona.Primer_Nombre, ''), ' ', COALESCE(idrdgov_simgeneral.persona.Segundo_Nombre, ''), ' ', COALESCE(idrdgov_simgeneral.persona.Primer_Apellido, ''), ' ', COALESCE(idrdgov_simgeneral.persona.Segundo_Apellido, '')  ) ) AS full_name,
        idrdgov_simgeneral.persona.Primer_Nombre AS `name`,
        idrdgov_simgeneral.persona.Segundo_Nombre AS middle_name,
        idrdgov_simgeneral.persona.Primer_Apellido AS last_name,
        idrdgov_simgeneral.persona.Segundo_Apellido AS second_last_name,
        idrdgov_simgeneral.persona.Cedula AS document,
        {$db_name}.tbl_programmings.request_type_id,
        COALESCE( (SELECT {$db_name}.tbl_request_types.request_type FROM {$db_name}.tbl_request_types WHERE {$db_name}.tbl_request_types.id = {$db_name}.tbl_programmings.request_type_id LIMIT 1), '' ) AS `request`,
        {$db_name}.`tbl_programmings`.`reunion_type_id` AS `reunion_type_id`,
        COALESCE ( ( SELECT {$db_name}.`tbl_reunion_types`.`reunion_type` FROM {$db_name}.`tbl_reunion_types` WHERE ( {$db_name}.`tbl_reunion_types`.`id` = {$db_name}.`tbl_programmings`.`reunion_type_id` ) LIMIT 1 ), '' ) AS `reunion_type`,
        {$db_name}.tbl_programmings.objective,
        {$db_name}.tbl_programmings.who_cancel,
        {$db_name}.tbl_programmings.reason_for_cancellation,
        {$db_name}.tbl_programmings.created_at,
        {$db_name}.tbl_programmings.updated_at,
        {$db_name}.tbl_programmings.deleted_at
        FROM
        {$db_name}.tbl_programmings
        JOIN idrdgov_simgeneral.persona
        ON {$db_name}.tbl_programmings.user_id = idrdgov_simgeneral.persona.Id_Persona;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $db_name = env('DB_DATABASE');
        DB::statement("DROP VIEW `{$db_name}`.`tbl_programming_view`");
    }
}
