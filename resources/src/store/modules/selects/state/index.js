export default {
  processes: [],
  requests: [],
  summoners: [],
  reunion_types: [],
  entities: [],
  population_types: [],
  conditions: [],
  situations: [],
  schedules: [],
  file_types: [],
  themes: [],
  professionals: []
}