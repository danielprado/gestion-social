import {Process} from "@/models/services/Process";
import {RequestType} from "@/models/services/RequestType";
import {Summoner} from "@/models/services/Summoner";
import {Entity} from "@/models/services/Entity";
import {TypeOfPopulation} from "@/models/services/TypeOfPopulation";
import {Condition} from "@/models/services/Condition";
import {Situation} from "@/models/services/Situation";
import {CitizenAttentionSchedule} from "@/models/services/CitizenAttentionSchedule";
import {FileType} from "@/models/services/FileType";
import {Theme} from "@/models/services/Theme";
import {Professional} from "@/models/services/Professional";
import {ReunionType} from "@/models/services/ReunionType";
const actions = {
  //Programming
  
  processes: ({ commit }) => {
    return new Promise( (resolve, reject) => {
      (new Process()).index()
        .then(response => {
          commit('STORE_PROCESSES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  request_types: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new RequestType()).index()
        .then(response => {
          commit('STORE_REQUESTS', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  summoners: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Summoner()).index()
        .then(response => {
          commit('STORE_SUMMONERS', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  reunion_types: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new ReunionType()).index()
        .then(response => {
          commit('STORE_REUNION_TYPES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  //Citizen
  
  entities: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Entity()).index()
        .then(response => {
          commit('STORE_ENTITIES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  population_types: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new TypeOfPopulation()).index()
        .then(response => {
          commit('STORE_POPULATION_TYPES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  conditions: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Condition()).index()
        .then(response => {
          commit('STORE_CONDITIONS', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  situations: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Situation()).index()
        .then(response => {
          commit('STORE_SITUATIONS', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  schedules: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new CitizenAttentionSchedule()).index()
        .then(response => {
          commit('STORE_SCHEDULES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  file_types: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new FileType()).index()
        .then(response => {
          commit('STORE_FILE_TYPES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  
  themes: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Theme()).index()
        .then(response => {
          commit('STORE_THEMES', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  
  professionals: ({commit}) => {
    return new Promise( (resolve, reject) => {
      (new Professional()).index()
        .then(response => {
          commit('STORE_PROFESSIONALS', response.data);
          resolve( response.data );
        })
        .catch(error => reject( error ))
    })
  },
  
  clear_all: ({commit}) => {
    return new Promise((resolve, reject) => {
      commit('CLEAR_ALL');
      resolve();
    })
  }
  
};

export default actions;