const mutations = {
  STORE_PROCESSES(state, response) {
    state.processes = response
  },
  STORE_REQUESTS(state, response) {
    state.requests = response
  },
  STORE_SUMMONERS(state, response) {
    state.summoners = response
  },
  STORE_REUNION_TYPES(state, response) {
    state.reunion_types = response
  },
  
  STORE_ENTITIES(state, response) {
    state.entities = response
  },
  STORE_POPULATION_TYPES(state, response) {
    state.population_types = response
  },
  STORE_CONDITIONS(state, response) {
    state.conditions = response
  },
  STORE_SITUATIONS(state, response) {
    state.situations = response
  },
  
  STORE_SCHEDULES(state, response) {
    state.schedules = response
  },
  
  STORE_FILE_TYPES(state, response) {
    state.file_types = response
  },
  
  STORE_THEMES(state, response) {
    state.themes = response
  },
  
  STORE_PROFESSIONALS(state, response) {
    state.professionals = response
  },
  
  CLEAR_ALL( state ) {
    state.processes = [];
    state.requests = [];
    state.summoners = [];
    state.reunion_types = [];
    state.entities = [];
    state.population_types = [];
    state.conditions = [];
    state.situations = [];
    state.schedules = [];
    state.file_types = [];
    state.themes = [];
    state.professionals = [];
  }
};

export default mutations