const getters = {
  getProcesses: (state) => state.processes,
  getRequests: (state) => state.requests,
  getSummoners: (state) => state.summoners,
  getReunionTypes: (state) => state.reunion_types,

  getEntities: (state) => state.entities,
  getPopulationTypes: (state) => state.population_types,
  getConditions: (state) => state.conditions,
  getSituations: (state) => state.situations,
  
  getSchedules: (state) => state.schedules,
  getFileTypes: (state) => state.file_types,
  getThemes: (state) => state.themes,
  getProfessionals: (state) => state.professionals,
};

export default getters;