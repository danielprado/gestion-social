import {vm} from "@/main"
const actions = {
  
  login: ({ commit }, data) => {
    return new Promise((resolve, reject) => {
      commit('LOGIN', data);
      resolve();
    })
  },

  logout: ({commit}) => {
      return new Promise((resolve, reject) => {
          commit('LOGOUT');
          delete vm.axios.defaults.headers.common['Authorization'];
          resolve();
      })
  }
  
};

export default actions;
