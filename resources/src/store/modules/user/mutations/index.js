const mutations = {
  LOGIN(state, response) {
    state.access_token = response.access_token;
    state.token_type = response.token_type;
    state.expires_in = response.expires_in;
    state.refresh_token = response.refresh_token;
    state.id = response.id;
    state.document = response.document;
    state.document_type_id = response.document_type_id;
    state.document_type =response.document_type;
    state.document_type_description = response.document_type_description;
    state.full_name = response.full_name;
    state.part_name = response.part_name;
    state.lastname = response.lastname;
    state.second_lastname = response.second_lastname;
    state.first_name = response.first_name;
    state.second_name = response.second_name;
    state.birthday = response.birthday;
    state.country_id = response.country_id;
    state.city = response.city;
    state.gender_id = response.gender_id;
    state.ethnicity_id = response.ethnicity_id;
    state.department_id = response.department_id;
    state.gender = response.gender;
    state.country = response.country;
    state.ethnicity = response.ethnicity;
    state.username = response.username;
    state.permission = response.permission;
  },
  LOGOUT(state) {
      state.access_token = null;
      state.token_type = null;
      state.expires_in = null;
      state.refresh_token = null;
      state.id = null;
      state.document = null;
      state.document_type_id = null;
      state.document_type =null;
      state.document_type_description = null;
      state.full_name = null;
      state.part_name = null;
      state.lastname = null;
      state.second_lastname = null;
      state.first_name = null;
      state.second_name = null;
      state.birthday = null;
      state.country_id = null;
      state.city = null;
      state.gender_id = null;
      state.ethnicity_id = null;
      state.department_id = null;
      state.gender = null;
      state.country = null;
      state.ethnicity = null;
      state.username = null;
      state.permission = [];
  }
};

export default mutations;
