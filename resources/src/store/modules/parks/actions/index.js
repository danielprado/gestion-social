import {Park} from "@/models/services/Park";
const actions = {
  store_parks: ({ commit }) => {
    return new Promise( (resolve, reject) => {
      (new Park()).index()
        .then( response => {
          commit('STORE_PARKS', response.data)
          resolve( response.data )
        })
        .catch( error => reject( error ))
    })
  }
};

export default actions;