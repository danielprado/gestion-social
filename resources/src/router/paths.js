/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
// mdi-view-dashboard
export default [
    {
        path: '/home',
        name: 'Home',
        view: 'Home',
        icon: 'mdi-calendar',
        can: true,
        meta: {
            hideFooter: true,
            isFullCalendar: true
        }
    },
    {
        path: '/general-calendar',
        name: 'General',
        view: 'General',
        icon: 'mdi-calendar-month-outline',
        can: true,
        meta: {
            hideFooter: true,
            isFullCalendar: true
        }
    },
    {
        path: '/citizen-attention',
        name: 'CitizenAttention',
        view: 'CitizenAttention',
        icon: 'mdi-nature-people',
        can: true
    },
    {
        path: '/citizen-attention/:id/details',
        name: 'CitizenAttentionDetails',
        view: 'CitizenAttentionDetails',
        icon: 'mdi-nature-people',
        hideInMenu: true,
        props: true,
        can: true
    },
    {
        path: '/community-attention',
        name: 'CommunityAttention',
        view: 'CommunityAttention',
        icon: 'mdi-account-multiple',
        can: true
    },
    {
        path: '/community-attention/:id/details',
        name: 'CommunityAttentionDetails',
        view: 'CommunityAttentionDetails',
        icon: 'mdi-account-multiple',
        hideInMenu: true,
        props: true,
        can: true
    },
    {
        path: '/programmings',
        name: 'Programming',
        view: 'Programming',
        icon: 'mdi-format-list-bulleted',
        can: true
    },
    {
        path: '/programmings/:id/details',
        name: 'ProgrammingDetails',
        view: 'ProgrammingDetails',
        icon: 'mdi-format-list-bulleted',
        hideInMenu: true,
        props: true,
        can: true
    },
    {
        path: '/follow-up-on-commitments',
        name: 'FollowingCommitment',
        view: 'FollowingCommitment',
        icon: 'mdi-format-list-bulleted',
        can: true
    },
    {
        path: '/reports',
        name: 'GeneralReport',
        icon: 'mdi-file-document-outline',
        props: true,
        can: true,
        children: [
            {
                path: 'programmings',
                name: 'GeneralReportProgramming',
                view: 'GeneralReport',
                icon: 'mdi-file-document-outline',
                props: true,
                can: true,
            },
            {
                path: 'citizen-attention',
                name: 'CalendarAttention',
                view: 'CalendarAttention',
                icon: 'mdi-file-document-outline',
                props: true,
                can: true,
            },
            {
                path: 'community-attention',
                name: 'CalendarCommunity',
                view: 'CalendarCommunity',
                icon: 'mdi-file-document-outline',
                props: true,
                can: true,
            },
            {
                path: 'by-professionals',
                name: 'ByProfessionals',
                view: 'ByProfessionals',
                icon: 'mdi-account',
                props: true,
                can: true,
            },
            {
                path: 'summary',
                name: 'Summary',
                view: 'Summary',
                icon: 'mdi-account',
                props: true,
                can: true,
            },
        ]
    },
    {
        path: '/programmings/:id/execution',
        name: 'Execution',
        view: 'Execution',
        icon: 'mdi-format-list-bulleted',
        hideInMenu: true,
        props: true,
        can: true
    },
    {
        path: '/stats',
        name: 'Stats',
        view: 'Stats',
        icon: 'mdi-chart-bar',
        can: true,
        meta: {
            hideFooter: true,
            isFullCalendar: true
        }
    },
]
