import {mapMutations, mapState} from "vuex";

/**
 * Mixin for change locale
 */

const AppLocaleMixin = {
    install(Vue) {
        Vue.mixin({
            methods: {
                ...mapMutations('app', ['setLang']),
                onClickLang ( lang ) {
                    this.setLang( lang );
                    this.setLocales();
                },
                setLocales: function () {
                    this.$i18n.locale = this.lang;
                    if ( this.$validator ) {
                        this.$validator.localize( this.lang );
                    }
                },
            },
            computed: {
                ...mapState('app', ['lang']),
                appLanguage () {
                    return this.$store.state.app.lang
                },
            },
            mounted () {
                if ( this.$store ) {
                    this.setLocales();
                }
            },
        });
    }
};

export default AppLocaleMixin;
