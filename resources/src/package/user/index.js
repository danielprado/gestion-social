import store from "@/store";

export default function(Vue) {
  Object.defineProperties(Vue.prototype, {
    $user: {
      get() {
        return store.getters['user/getUserData'];
      }
    }
  });
}