export default {
  required: {
    required: true,
  },
  text_required: {
    required: true,
    min: 3,
    max: 2500
  },
  excel_file_required: {
    required: true,
    mimes: ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
  },
  any_doc_files_required: {
    required: true,
    mimes: ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf']
  },
  text: {
    min: 3,
    max: 2500
  },
  input_text_required: {
    required: true,
    min: 3,
    max: 80
  },
  input_text: {
    min: 3,
    max: 80
  },
  input_number_required: {
    required: true,
    numeric: true,
  },
  input_number: {
    numeric: true,
  },
  input_phone_required: {
    required: true,
    numeric: true,
    digits: 7,
  },
  input_phone: {
    numeric: true,
    digits: 7,
  },
  input_mobile_required: {
    required: true,
    numeric: true,
    digits: 10,
  },
  input_mobile: {
    numeric: true,
    digits: 10,
  },
  input_document_required: {
    required: true,
    numeric: true,
    min: 3,
    max: 12
  },
  input_document: {
    numeric: true,
    min: 3,
    max: 12
  },
  input_date_required: {
    required: true,
    date_format: 'yyyy-MM-dd'
  },
  input_date: {
    date_format: 'yyyy-MM-dd'
  },
  input_time_required: {
    required: true,
    date_format: 'HH:mm'
  },
  input_time: {
    date_format: 'HH:mm'
  },
  input_datetime_required: {
    required: true,
    date_format: 'yyyy-MM-dd HH:mm:ss',
  },
  input_datetime: {
    date_format: 'yyyy-MM-dd HH:mm:ss',
  },
  input_datetime_after_required: ( target ) => ({
    required: true,
    date_format: 'yyyy-MM-dd HH:mm:ss',
    after: target
  }),
  input_datetime_before_required: ( target ) => ({
    required: true,
    date_format: 'yyyy-MM-dd HH:mm:ss',
    before: target
  }),
  input_number_required_between: (min, max) => ({
    required: true,
    numeric: true,
    between: [min, max]
  }),
  input_number_between: (min, max) => ({
    numeric: true,
    between: [min, max]
  }),
}