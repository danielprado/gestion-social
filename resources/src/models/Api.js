export const Api = {
  END_POINTS: {
    //Project
    REUNION_TYPES: () => `/api/reunion-types`,
    REQUEST_TYPES: () => `/api/request-types`,
    PROCESSES: () => `/api/processes`,
    SUMMONERS: () => `/api/summoners`,
    PROGRAMMING: () => `/api/programmings`,
    PROGRAMMING_SUMMARY: () => `/api/summary`,
    SCHEDULES: () => `/api/citizen-attention-schedules`,
    CITIZEN: () => `/api/citizen-attentions`,
    COMMUNITY: () => `/api/community-attentions`,
    PARKS: () => `/api/parks`,
    PROFESSIONALS: () => `/api/professionals`,
    THEMES: () => `/api/themes`,
    CONDITIONS: () => `/api/conditions`,
    SITUATIONS: () => `/api/situations`,
    ENTITIES: () => `/api/entities`,
    FILE_TYPES: () => `/api/file-types`,
    TYPES_OF_POPULATIONS: () => `/api/types-of-populations`,
    ACTIVITIES: (id)  => `/api/activities/${id}`,
    EXECUTIONS: (id)  => `/api/programmings/${id}/executions`,
    PROGRAMMINGS_FILES: (id)  => `/api/programmings/${id}/files`,
    PROGRAMMINGS_IMAGE: (id)  => `/api/programmings/${id}/images`,
    PROGRAMMINGS_COMMITMENTS: (id) => `/api/programmings/${id}/commitments`,
    PROGRAMMINGS_COMMITMENTS_TRACINGS: (programming_id, commitment_id) => `/api/programmings/${programming_id}/commitments/${commitment_id}/tracings`,
    COMMITMENTS_CALENDAR: () => `/api/commitments/calendar`,
    GENERAL_REPORT: () => `/api/programmings/general-report`,
    REPORT_BY_PROFESSIONALS: () => `/api/programmings/by-professionals`,
    ATTENTION_REPORT: () => `/api/attendance/general-report`,
    COMMUNITY_REPORT: () => `/api/community/general-report`,
    CITIZEN_FILES: (id)  => `/api/citizens/${id}/files`,
    COMMUNITY_FILES: (id)  => `/api/community/${id}/files`,

    FOLLOW_UP_ON_COMMITMENT: () => `/api/follow-up-on-commitments`,
    FOLLOW_UP_ON_COMMITMENT_EXCEL: () => `/api/follow-up-on-commitments/excel`,
    FOLLOW_UP_ON_COMMITMENT_TRACINGS: (commitment_id) => `/api/follow-up-on-commitments/${commitment_id}/tracings`,

    
    PROGRAMMED_PARKS: () => `/api/programmed-parks`,
    PROGRAMMING_COUNT: () => `/api/counters`,
    STATS: () => `/api/stats`,
    
    

    // Auth Routes
    GET_PROFILE: () => `/api/user`,
    LOGIN: () => `/oauth/access-token`,
    LOGOUT: () => `/logout`,
    LOCK: () => `/lock`,
    UNLOCK: () => `/unlock`
  }
};
