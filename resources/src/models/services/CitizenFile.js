import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class CitizenFile extends Model {
  constructor(id, data = {}) {
    super(Api.END_POINTS.CITIZEN_FILES(id), data);
  }
}