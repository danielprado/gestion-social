import {Model} from "@/models/Model";
import {Api} from "@/models/Api";
import {vm} from "@/main";

export class ProgrammingImage extends Model {
  constructor(id, data = {
    image: null,
    description: null,
    confirmation: false,
  }) {
    super(Api.END_POINTS.PROGRAMMINGS_IMAGE( id ), data);
  }

  store( image ) {
    return new Promise( (resolve, reject) => {
      let config = {
        header : {
          'Content-Type' : 'multipart/form-data'
        }
      };
      let form_data = new FormData();
      let confirmation = this.confirmation ? 1 : 0;
      form_data.append( 'description', this.description );
      form_data.append( 'confirmation', confirmation );
      if ( image.hasImage() ) {
        form_data.append( 'image', this.image, image.getChosenFile().name );
      }

      vm.axios.post( this.url, form_data, config )
        .then((response) => resolve( response.data ))
        .then(() => this.reset() )
        .then(() => {
          if (image.hasImage()) {
            image.remove()
          }
        })
        .then(() => vm.$validator.reset())
        .catch((error) => {
          if ( error.response && error.response.data ) {
            reject( error.response.data );
          } else {
            reject( error );
          }
        })

    })
  }
}