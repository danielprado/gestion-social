import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class CommunityAttention extends Model {
  constructor(data = {
    date: null,
    initial_hour: null,
    final_hour: null,
    who_cancel: null,
    reason: null,
  }) {
    super(Api.END_POINTS.COMMUNITY(), data);
  }
  
  calendar( options = {} ) {
    return this.get( `${this.url}/calendar`, options );
  }
  
  cancel( id, options = {} ) {
    return this.post( `${this.url}/${id}/cancel`, options );
  }
}