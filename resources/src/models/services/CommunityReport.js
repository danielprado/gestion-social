import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class CommunityReport extends Model {
  constructor(data = {
    start_of_week: null,
    end_of_week: null,
    professionals: []
  }) {
    super(Api.END_POINTS.COMMUNITY_REPORT(), data);
  }
}