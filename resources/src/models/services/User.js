import { Model } from "@/models/Model";
import { Api } from "@/models/Api";

export class User extends Model {
  constructor(data) {
    super(Api.END_POINTS.GET_PROFILE(), data);
  }

  login() {
    return this.post(Api.END_POINTS.LOGIN(), this.data());
  }

  logout() {
    return this.post( Api.END_POINTS.LOGOUT() );
  }
  
  lock() {
    return this.post(Api.END_POINTS.LOCK());
  }
  
  unlock() {
    return this.post( Api.END_POINTS.UNLOCK(), this.data() );
  }
}
