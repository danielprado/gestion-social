import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class FollowingUpOnCommitment extends Model {
  constructor(data = {
    date: null,
    responsable: null,
    description: null,
    park_id: null,
  }) {
    super(Api.END_POINTS.FOLLOW_UP_ON_COMMITMENT(), data);
  }

  excel(options = {}) {
    return this.post( Api.END_POINTS.FOLLOW_UP_ON_COMMITMENT_EXCEL(), options );
  }
}