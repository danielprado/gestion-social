import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class CommunityFile extends Model {
  constructor(id, data = {}) {
    super(Api.END_POINTS.COMMUNITY_FILES(id), data);
  }
}