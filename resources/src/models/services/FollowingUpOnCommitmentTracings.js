import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class FollowingUpOnCommitmentTracings extends Model {
  constructor(id, data = {
    date: null,
    description: null,
  }) {
    super(Api.END_POINTS.FOLLOW_UP_ON_COMMITMENT_TRACINGS(id), data);
  }
}