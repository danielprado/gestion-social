import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Professional extends Model {
  constructor(data = {}) {
    super(Api.END_POINTS.PROFESSIONALS(), data );
  }
}