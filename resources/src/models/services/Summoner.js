import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Summoner extends Model {
  constructor() {
    super(Api.END_POINTS.SUMMONERS(), {});
  }

}