import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Tracing extends Model {
  constructor(programming_id, commitment_id, data = {
    date: null,
    description: null
  }) {
    super(Api.END_POINTS.PROGRAMMINGS_COMMITMENTS_TRACINGS(programming_id, commitment_id), data );
  }
}