import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class ProgrammingSummary extends Model {
  constructor(data = {}) {
    super(Api.END_POINTS.PROGRAMMING_SUMMARY(), data);
  }
}